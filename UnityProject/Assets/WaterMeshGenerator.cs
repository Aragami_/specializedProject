using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Mesh Generation: https://www.youtube.com/watch?v=3MoHJtBnn2U
public class WaterMeshGenerator : MonoBehaviour
{
    [SerializeField] private float m_size = 1;
    [SerializeField] private int m_gridSize = 16;

    public void GenerateMesh()
    {
        MeshFilter filter = GetComponent<MeshFilter>();

        Mesh m = new Mesh();

        List<Vector3> vertices = new List<Vector3>();
        List<Vector3> normals = new List<Vector3>();
        List<Vector2> uvs = new List<Vector2>();

        for (int x = 0; x < m_gridSize + 1; x++)
        {
            for (int y = 0; y < m_gridSize + 1; y++)
            {
                vertices.Add(new Vector3(-m_size * 0.5f + m_size * (x / ((float)m_gridSize)), 0, -m_size * 0.5f  + m_size * (y / ((float)m_gridSize))));
                normals.Add(Vector3.up);
                uvs.Add(new Vector2(x /(float)m_gridSize, y / (float)m_gridSize));
            }
        }

        List<int> triangles = new List<int>();
        int vertCount = m_gridSize + 1;
        for (int i = 0; i < vertCount * vertCount - vertCount; i++)
        {
            if ((i + 1) % vertCount == 0)
            {
                continue;
            }
            triangles.AddRange(new List<int>()
            {
                i+1+vertCount, i+vertCount, i, i, i+1, i+vertCount+1
            });
        }

        m.SetVertices(vertices);
        m.SetNormals(normals);
        m.SetUVs(0, uvs);
        m.SetTriangles(triangles, 0);

        filter.mesh = m;
    }
}
