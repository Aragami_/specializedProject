using FMODUnity;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

[RequireComponent(typeof(Rigidbody))]
[DisallowMultipleComponent]
public class PlayerController : MonoBehaviour
{
    #region Variables

    #region SerializeField
    [Header("Base")]
    [SerializeField]
    [Tooltip("The rigidbody of the player.")]
    private Rigidbody m_myRigidbody = null;
    [SerializeField]
    [Tooltip("The camera from the palyer.")]
    private Camera m_myCamera = null;
    [SerializeField]
    [Tooltip("The transform of the object beneath the character for the ground check.")]
    private Transform m_groundCheck = null;

    [Header("Freezing Effekt")]
    [SerializeField]
    [Tooltip("Freezing Postprocessing")]
    private Volume m_freezingPP;
    [SerializeField]
    [Tooltip("Vignette size")]
    [Range(0, 1f)]
    private float m_vignetteSize = 0;
    [SerializeField]
    [Range(1f, 4f)]
    [Tooltip("Freezing Modifier - Interval multiplier")]
    private float m_freezingMultiplier = 1;
    [SerializeField]
    [Tooltip("Character is freezing")]
    private bool m_freezing = false;

    [Header("Dying Effekt")]
    [SerializeField]
    [Tooltip("The multiplier for the saturation of the dying PP.")]
    private float m_dyingIntensityMultiplier = 0.0f;
    [SerializeField]
    [Tooltip("When to start the dying effect.")]
    private int m_dying = 0;

    [Header("Ground Check")]
    [SerializeField]
    [Tooltip("The layers the ground raycast can hit.")]
    private LayerMask m_groundLayers = 0;
    [SerializeField]
    [Tooltip("The the distance to ground at which the player will be counted as grounded..")]
    private float m_distToGround = 0.0f;
    [SerializeField]
    [Tooltip("The distance between the center ray and the other rays.")]
    private float m_distToCenter = 0.0f;
    [SerializeField]
    [Tooltip("The distance the ray can travel.")]
    private float m_groundRayLength = 0.0f;

    [Header("Movement")]
    [SerializeField]
    [Tooltip("The speed with which the player moves.")]
    private float m_movementSpeed = 0.0f;
    [SerializeField]
    [Tooltip("The speed with which the player runs.")]
    private float m_runningSpeed = 0.0f;
    [SerializeField]
    [Tooltip("The force with which the player jumps.")]
    private float m_jumpForce = 0.0f;
    [SerializeField]
    [Tooltip("The rotaion speed of the camera.")]
    private float m_rotationSpeed = 0.0f;

    [Header("Interaction")]
    [SerializeField]
    [Tooltip("The length of the ray.")]
    private float m_rayLength = 0.0f;

    [Header("Camera adjustments")]
    [SerializeField]
    [Tooltip("Min angle of the x rotation for the camera.")]
    private float m_minX = 0.0f;
    [SerializeField]
    [Tooltip("Max angle of the x rotation for the camera.")]
    private float m_maxX = 0.0f;

    [Header("Player stats")]
    [SerializeField]
    [Range(0, 1000000)]
    [Tooltip("The player max health. 0 >= empty.")]
    private int m_maxHealth = 0;
    [SerializeField]
    [Range(0, 1000000)]
    [Tooltip("The player max hunger. 0 >= empty.")]
    private int m_maxHunger = 0;
    [SerializeField]
    [Range(0, 1000000)]
    [Tooltip("The player max hirst. 0 >= empty.")]
    private int m_maxThirst = 0;
    [SerializeField]
    [Range(0, float.MaxValue)]
    [Tooltip("How fast should the player lose hunger? Default = 0.01f. Higher number means faster.")]
    private float m_hungerMultiplier = 0.01f;
    [SerializeField]
    [Range(0, float.MaxValue)]
    [Tooltip("How fast should the player lose thirst? Default = 0.01f. Higher number means faster.")]
    private float m_thirstMultiplier = 0.01f;
    [SerializeField]
    [Range(0, float.MaxValue)]
    [Tooltip("The interval between reducing hunger. Default = 1.0f.")]
    private float m_hungerInterval = 1.0f;
    [SerializeField]
    [Range(0, float.MaxValue)]
    [Tooltip("The interval between reducing thirst. Default = 1.0f.")]
    private float m_thirstInterval = 1.0f;
    [SerializeField]
    [Range(0, 1000000)]
    [Tooltip("How much hunger will be reduce per interval? Default = 1.")]
    private int m_hungerAmount = 1;
    [SerializeField]
    [Range(0, 1000000)]
    [Tooltip("How much thirst will be reduce per interval? Default = 1.")]
    private int m_thirstAmount = 1;
    [SerializeField]
    [Range(0, 1000000)]
    [Tooltip("At this state of hunger or thirst the player will get damage.")]
    private int m_hungerThirstState = 0;
    [SerializeField]
    [Range(0, 1000000)]
    [Tooltip("The amount of damage to take when hungry or thirsty. Default  = 1.")]
    private int m_hungerThirstDamage = 5;
    [SerializeField]
    [Range(0, float.MaxValue)]
    [Tooltip("How long does it take to revive? Default = 5.0f.")]
    private float m_reviveTimer = 5.0f;
    [SerializeField]
    [Range(0.01f, 1.0f)]
    [Tooltip("Revive the player with x% of his health, hunger and thirst. Default = 0.5f / 50%.")]
    private float m_revivePercentage = 0.5f;
    [SerializeField, Range(0, 10), Tooltip("Damage taken without an axe!")]
    private int m_timberDamage = 5;

    [Header("Natural regeneration")]
    [SerializeField]
    [Range(0, 1000000)]
    [Tooltip("At this state of hunger and thirst the player will regenerate health.")]
    private int m_healState = 0;
    [SerializeField]
    [Range(0, 1000000)]
    [Tooltip("The amount the player gets healed when hunger and thirst are above m_healState.")]
    private int m_regenerationAmount = 0;
    [SerializeField]
    [Range(0, float.MaxValue)]
    [Tooltip("The interval between regeneration of health. Default = 1.0f.")]
    private float m_regenerationInterval = 1.0f;
    [SerializeField]
    [Range(0, float.MaxValue)]
    [Tooltip("How fast should the player regenrate health? Default = 0.01f. Higher number means faster.")]
    private float m_regenrationMultiplier = 0.01f;

    [Header("First Time Events")]
    [SerializeField]
    [Tooltip("When should the thirst event message be displayed. Only happens once. This is for a FirstTimeEvent.")]
    private int m_thirstFirstTimeEvent = 0;
    [SerializeField]
    [TextArea]
    [Tooltip("The event message for the FirstTimeEvent for thirst.")]
    private string m_thirstEventMessage = null;

    [Header("Seeds")]
    [SerializeField]
    [Tooltip("Key to place Tree seed")]
    private List<ThrowableSeed> m_throwableSeeds;


    #endregion

    #region Private
    [Tooltip("Is the player alive? Default = true;")]
    private bool m_isAlive = true;
    [Tooltip("The players current health.")]
    private int m_currentHealth = 0;
    [Tooltip("The players current hunger state.")]
    private int m_currentHunger = 0;
    [Tooltip("The players current thirst state.")]
    private int m_currentThirst = 0;
    [Tooltip("Current hunger interval timer.")]
    private float m_hungerTimer = 0.0f;
    [Tooltip("Current thirst interval timer.")]
    private float m_thirstTimer = 0.0f;
    [Tooltip("Current time until review.")]
    private float m_currentReviveTimer = 0.0f;
    [Tooltip("Current regenration interval timer.")]
    private float m_regenrationTimer = 0.0f;

    [Tooltip("The ray which is used for the interaction.")]
    private Ray m_interactionRay = new Ray();

    [Tooltip("The current rotation on the x axis.")]
    private float m_rotX = 0.0f;
    [Tooltip("The current rotation on the y axis.")]
    private float m_rotY = 0.0f;

    [Tooltip("Is the character on the Ground?")]
    private bool m_isGrounded = true;
    [Tooltip("When the player is grounded, is he on Terrain?")]
    private bool m_isOnTerrain = true;
    [Tooltip("Can the player move? Default = true.")]
    private bool m_canMove = true;
    [Tooltip("Can the player rotate? Default = true.")]
    private bool m_canRotate = true;

    [Tooltip("Vignette effect - Freezing")]
    private Vignette m_freezingVignette;

    [Tooltip("The color adjustment for dying.")]
    private ColorAdjustments m_dyingColorAdjustments = null;

    #endregion

    #region Static

    public static PlayerController Instance { get; private set; } = null;

    #endregion

    #region Property
    public bool IsAlive
    {
        get => m_isAlive;
        private set => m_isAlive = value;
    }

    public bool IsGrounded => m_isGrounded;

    public bool IsOnTerrain => m_isOnTerrain;

    public bool CanMove
    {
        get => m_canMove;
        set
        {
            m_canMove = value;
        }
    }

    public bool CanRotate
    {
        get => m_canRotate;
        set
        {
            m_canRotate = value;
        }
    }

    public int CurrentHealth
    {
        get => m_currentHealth;
        private set
        {
            m_currentHealth = Mathf.Clamp(value, 0, m_maxHealth);

            if (!SoundManager.Instance.HungerEmitter.IsPlaying())
            {
                SoundManager.Instance.HungerEmitter.Play();
            }

            SoundManager.Instance.MusicEmitter.SetParameter("Danger", Mathf.Abs(m_currentHealth - 100));

            if (m_currentHealth <= SoundManager.Instance.HeartbeatSound)
            {
                if (!SoundManager.Instance.HeartbeatEmitter.IsPlaying())
                {
                    SoundManager.Instance.HeartbeatEmitter.Play();
                }
            }
            else
            {
                if (SoundManager.Instance.HeartbeatEmitter.IsPlaying())
                {
                    SoundManager.Instance.HeartbeatEmitter.Stop();
                }
            }

            if (m_currentHealth <= 0)
            {
                IsAlive = false;
                if (SoundManager.Instance.HeartbeatEmitter.IsPlaying())
                {
                    SoundManager.Instance.HeartbeatEmitter.Stop();
                }
                SoundManager.Instance.PlaySoundEffect(gameObject, SoundManager.Instance.DeathNotificationEmitter);
                SoundManager.Instance.HungerEmitter.Stop();
                GameManager.Instance.EventMessage("You left Wilson alone... You monster!");
            }
            UIManager.Instance.UpdateHealthParameterUI(m_currentHealth, m_currentHunger, m_currentThirst);
        }
    }

    public int CurrentHunger
    {
        get => m_currentHunger;
        private set
        {
            m_currentHunger = Mathf.Clamp(value, 0, m_maxHunger);
            SoundManager.Instance.HungerEmitter.SetParameter("Hunger", Mathf.Abs(m_currentHunger - 100));

            UIManager.Instance.UpdateHealthParameterUI(m_currentHealth, m_currentHunger, m_currentThirst);
        }
    }

    public int CurrentThirst
    {
        get => m_currentThirst;
        private set
        {
            m_currentThirst = Mathf.Clamp(value, 0, m_maxThirst);

            if (m_currentThirst <= m_thirstFirstTimeEvent)
            {
                if (GameManager.Instance.MyFTSSO.MyFirstTimeValues.ContainsKey(EFirstTimeEvent.FindWater))
                {
                    if (GameManager.Instance.MyFTSSO.MyFirstTimeValues.TryGetValue(EFirstTimeEvent.FindWater, out bool ftssoValue))
                    {
                        if (ftssoValue)
                        {
                            GameManager.Instance.EventMessage(m_thirstEventMessage);
                            GameManager.Instance.MyFTSSO.MyFirstTimeValues[EFirstTimeEvent.FindWater] = false;
                        }
                    }
                }
            }

            UIManager.Instance.UpdateHealthParameterUI(m_currentHealth, m_currentHunger, m_currentThirst);
        }
    }

    public int RegenrationAmount
    {
        get => m_regenerationAmount;
        private set
        {
            m_regenerationAmount = value;
        }
    }

    public int MaxHunger { get => m_maxHunger; set => m_maxHunger = value; }
    public int MaxThirst { get => m_maxThirst; set => m_maxThirst = value; }
    public int MaxHealth { get => m_maxHealth; set => m_maxHealth = value; }

    public Vignette FreezingVignette
    {
        get => m_freezingVignette;
    }

    #endregion

    #endregion

    #region Awake | Start | Update | FixedUpdate

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this.gameObject);
            return;
        }
        Instance = this;

        m_myRigidbody = GetComponent<Rigidbody>();
        m_myCamera = Camera.main;

        m_freezingPP.profile.TryGet<Vignette>(out m_freezingVignette);
        m_freezingPP.profile.TryGet<ColorAdjustments>(out m_dyingColorAdjustments);
    }

    private void Start()
    {
        CurrentHealth = m_maxHealth;
        CurrentHunger = m_maxHunger;
        CurrentThirst = m_maxThirst;
    }

    // Update is called once per frame
    private void Update()
    {
        if (GameManager.Instance)
        {
            if (GameManager.Instance.DbHandler.LoggedIn)
            {
                if (UIManager.Instance.OpenedUI)
                    return;

                if (IsAlive)
                {
                    if (CurrentHealth <= m_dying)
                    {
                        m_dyingColorAdjustments.saturation.value -= Time.deltaTime * m_dyingIntensityMultiplier;
                    }
                    else
                    {
                        m_dyingColorAdjustments.saturation.value += Time.deltaTime * m_dyingIntensityMultiplier;
                        m_dyingColorAdjustments.saturation.value = Mathf.Clamp(m_dyingColorAdjustments.saturation.value, -100, 0);
                    }

                    if (!GameManager.Instance.GodMode)
                    {
                        ReduceHungerThirst();
                    }

                    // Only happens if the player doesn't have full health.
                    if (CurrentHealth < m_maxHealth)
                    {
                        RegenerateHealth();
                    }
                    // Otherwise set the timer back to 0.0f. So the interval starts from the beginning if the player start regenerating health.
                    else
                    {
                        m_regenrationTimer = 0.0f;
                    }
                }
                else
                {
                    m_currentReviveTimer += Time.deltaTime;
                    if (m_currentReviveTimer >= m_reviveTimer)
                    {
                        RevivePlayer();
                    }
                }
                GroundCheck();

                // Blocks everthing thats inside as long as the console is active. Mean no movements etc.
                if (!DeveloperConsoleBehaviour.Instance.IsConsoleActive)
                {
                    if (IsAlive)
                    {
                        if (CanRotate)
                        {
                            Rotation();
                        }
                        if (CanMove)
                        {
                            Jump();
                        }
                        Interaction();
                    }
                }
                ClearInventory();

                if (!GameManager.Instance.DayTime)
                {
                    // check in 5 second interval
                    if ((int)Time.time % 5 == 1)
                    {
                        if (Physics.Raycast(transform.position, transform.forward, float.PositiveInfinity, BuildingSystem.Instance.BuildingLayer))
                        {
                            if (Physics.Raycast(transform.position, transform.right, float.PositiveInfinity, BuildingSystem.Instance.BuildingLayer))
                            {

                                if (Physics.Raycast(transform.position, -transform.right, float.PositiveInfinity, BuildingSystem.Instance.BuildingLayer))
                                {
                                    if (Physics.Raycast(transform.position, -transform.forward, float.PositiveInfinity, BuildingSystem.Instance.BuildingLayer))
                                    {
                                        m_freezing = false;
                                        return;
                                    }
                                }
                            }
                        }
                        m_freezing = true;
                    }
                }
                else
                {
                    m_freezing = false;
                }
            }
        }
    }

    private void FixedUpdate()
    {
        if (GameManager.Instance.DbHandler.LoggedIn)
        {
            // Blocks everthing thats inside as long as the console is active. Mean no movemnts etc.
            if (!DeveloperConsoleBehaviour.Instance.IsConsoleActive)
            {
                if (IsAlive)
                {
                    if (CanMove)
                    {
                        Movement();
                    }
                }
            }
        }
    }
    #endregion

    private void ClearInventory()
    {
        //clear Inventory
        if (Input.GetKeyDown(KeyCode.F4))
            GameManager.Instance.DbHandler.ResetInventory();
    }

    /// <summary>
    /// Move the player character.
    /// </summary>
    private void Movement()
    {
        Vector3 dir = this.transform.right * Input.GetAxisRaw("Horizontal") + this.transform.forward * Input.GetAxisRaw("Vertical");
        // When shift is pressed the player will run.
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            dir = dir.normalized * m_runningSpeed;
        }
        // Otherwise walk.
        else
        {
            dir = dir.normalized * m_movementSpeed;
        }
        dir.y = m_myRigidbody.velocity.y;

        m_myRigidbody.velocity = dir;
    }

    /// <summary>
    /// Let the player jump.
    /// </summary>
    private void Jump()
    {
        if (m_isGrounded && Input.GetKeyDown(KeyCode.Space))
        {
            m_myRigidbody.AddForce(Vector3.up * m_jumpForce, ForceMode.Impulse);
        }
    }

    /// <summary>
    /// Rotate the player model and the camera.
    /// </summary>
    private void Rotation()
    {
        m_rotX += Input.GetAxisRaw("Mouse Y") * m_rotationSpeed * Time.deltaTime;
        m_rotY += Input.GetAxisRaw("Mouse X") * m_rotationSpeed * Time.deltaTime;
        m_rotX = Mathf.Clamp(m_rotX, m_minX, m_maxX);

        m_myRigidbody.transform.localEulerAngles = new Vector3(0, m_rotY, 0);
        m_myCamera.transform.localEulerAngles = new Vector3(-m_rotX, 0, 0);
    }

    /// <summary>
    /// Let the player interact with something.
    /// </summary>
    public void Interaction()
    {
        // Interact
        if (Input.GetKeyDown(KeyCode.E)) // Only check if E pressed, might want to remove this later to enable features like highlighting...
        {
            m_interactionRay = new Ray(m_myCamera.transform.position, m_myCamera.transform.forward);

            if (Physics.Raycast(m_interactionRay, out RaycastHit hit, m_rayLength))
            {
                hit.collider.GetComponent<ILootable>()?.Loot();
            }
        }

        // Attack/Chop
        if (Input.GetKeyDown(KeyCode.F)) // Only check if F pressed, might want to remove this later to enable features like highlighting...
        {
            m_interactionRay = new Ray(m_myCamera.transform.position, m_myCamera.transform.forward);

            if (Physics.Raycast(m_interactionRay, out RaycastHit hit, m_rayLength))
            {
                IDamageable damageable = hit.collider.GetComponent<IDamageable>();
                if (damageable != null && !damageable.Damage(1))
                {
                    if (hit.collider.TryGetComponent<Plant>(out Plant plant))
                        if (plant.m_plantType == PlantType.TREE)
                            if (!SoundManager.Instance.PlayTimberSound())
                                TakeDamage(m_timberDamage);
                }
            }
        }

        // Plant Seed
        foreach (ThrowableSeed seed in m_throwableSeeds)
        {
            if (Input.GetKeyDown(seed.GetKey))
            {
                if (InventoryManager.Instance.GetItemAmount((int)seed.GetResourceType) >= 1)
                {
                    InventoryManager.Instance.SubtractItem((int)seed.GetResourceType, 1);
                    Instantiate(seed.GetPrefab, this.transform.position, Quaternion.identity);
                }
            }
        }
    }

    /// <summary>
    /// Checks if the character is on the ground.
    /// </summary>
    protected void GroundCheck()
    {
        bool grounded = false;
        bool onTerrain = false;

        #region Ground check

        // Shots five raycasts to check if the player is grounded.
        Physics.Raycast(m_groundCheck.position, Vector3.down, out RaycastHit hit, m_groundRayLength, m_groundLayers);
        float distanceToGroundMiddle = Vector3.Distance(hit.point, m_groundCheck.position);
        if (hit.transform != null && hit.transform.CompareTag("Ground")) onTerrain = true;

        Physics.Raycast(m_groundCheck.position + m_groundCheck.right * (-m_distToCenter), Vector3.down, out hit, m_groundRayLength, m_groundLayers);
        float distanceToGroundLeft = Vector3.Distance(hit.point, m_groundCheck.position);
        if (!onTerrain && hit.transform != null && hit.transform.CompareTag("Ground")) onTerrain = true;

        Physics.Raycast(m_groundCheck.position + m_groundCheck.right * m_distToCenter, Vector3.down, out hit, m_groundRayLength, m_groundLayers);
        float distanceToGroundRight = Vector3.Distance(hit.point, m_groundCheck.position);
        if (!onTerrain && hit.transform != null && hit.transform.CompareTag("Ground")) onTerrain = true;

        Physics.Raycast(m_groundCheck.position + m_groundCheck.forward * (-m_distToCenter), Vector3.down, out hit, m_groundRayLength, m_groundLayers);
        float distanceToGroundBack = Vector3.Distance(hit.point, m_groundCheck.position);
        if (!onTerrain && hit.transform != null && hit.transform.CompareTag("Ground")) onTerrain = true;

        Physics.Raycast(m_groundCheck.position + m_groundCheck.forward * m_distToCenter, Vector3.down, out hit, m_groundRayLength, m_groundLayers);
        float distanceToGroundFront = Vector3.Distance(hit.point, m_groundCheck.position);
        if (!onTerrain && hit.transform != null && hit.transform.CompareTag("Ground")) onTerrain = true;

        // If the float is smaller or the same as the given value then the player is grounded.
        if (m_distToGround >= distanceToGroundMiddle)
        {
            grounded = true;
        }
        else if (m_distToGround >= distanceToGroundLeft)
        {
            grounded = true;
        }
        else if (m_distToGround >= distanceToGroundRight)
        {
            grounded = true;
        }
        else if (m_distToGround >= distanceToGroundBack)
        {
            grounded = true;
        }
        else if (m_distToGround >= distanceToGroundFront)
        {
            grounded = true;
        }

        #endregion

        m_isGrounded = grounded;
        m_isOnTerrain = grounded && onTerrain;
    }

    /// <summary>
    /// Reduces the hunger and thirst depending on the setting.
    /// </summary>
    private void ReduceHungerThirst()
    {
        // reduce player parameters multiplied by freezing if it's night and the player has no shelter around
        if (m_freezing)
        {
            // build freezing effect
            if (m_freezingVignette && m_freezingVignette.intensity.value <= m_vignetteSize)
            {
                m_freezingVignette.intensity.value += Time.deltaTime / 12;
            }
            // set new hunger and thirst value times multiplier
            m_hungerTimer += Time.deltaTime * m_hungerMultiplier * m_freezingMultiplier;
            m_thirstTimer += Time.deltaTime * m_thirstMultiplier * m_freezingMultiplier;

            // show event message that player is freezing
            if ((int)Time.time % 10 == 1)
                GameManager.Instance.EventMessage("It's freezing cold out here!");
        }
        else if (!m_freezing)
        {
            // reduce player parameters without freezing multiplier
            m_hungerTimer += Time.deltaTime * m_hungerMultiplier;
            m_thirstTimer += Time.deltaTime * m_thirstMultiplier;

            // clear freezing effect
            if (m_freezingVignette)
                m_freezingVignette.intensity.value -= Time.deltaTime / 12;

        }

        if (m_hungerTimer >= m_hungerInterval)
        {
            CurrentHunger -= m_hungerAmount;
            m_hungerTimer = 0.0f;

            if (m_currentHunger < m_hungerThirstState)
            {
                TakeDamage(m_hungerThirstDamage);
            }
        }

        if (m_thirstTimer >= m_thirstInterval)
        {
            CurrentThirst -= m_thirstAmount;
            m_thirstTimer = 0.0f;

            if (m_currentThirst < m_hungerThirstState)
            {
                TakeDamage(m_hungerThirstDamage);
            }
        }
    }

    public void EatDrink(int? _eat = 0, int? _drink = 0)
    {
        CurrentHunger += _eat == 0 ? 0 : (int)_eat;
        CurrentThirst += _drink == 0 ? 0 : (int)_drink;

    }

    /// <summary>
    /// Let the player natural regenerate health, if above given hunger and thirst.
    /// </summary>
    private void RegenerateHealth()
    {
        m_regenrationTimer += Time.deltaTime * m_regenrationMultiplier;

        if (m_regenrationTimer >= m_regenerationInterval)
        {
            if (((CurrentHunger >= m_healState) && (CurrentThirst >= m_healState)) || GameManager.Instance.GodMode)
            {
                Heal(m_regenerationAmount);
            }
            m_regenrationTimer = 0.0f;
        }
    }

    /// <summary>
    /// Let the player take damage. Can't be under 0.
    /// </summary>
    /// <param name="_damage">The amount of damage the player takes.</param>
    public void TakeDamage(int _damage)
    {
        if (_damage < 0)
        {
            Debug.LogWarning($"You took negative damage: {_damage}");
            return;
        }

        CurrentHealth -= _damage;
    }

    /// <summary>
    /// Set the current health to this value.
    /// </summary>
    /// <param name="_health">The value to set the health to.</param>
    public void SetCurrentHealth(int _health)
    {
        CurrentHealth = _health;
    }

    /// <summary>
    /// Set the current hunger to this value.
    /// </summary>
    /// <param name="_hunger">The value to set the hunger to.</param>
    public void SetCurrentHunger(int _hunger)
    {
        CurrentHunger = _hunger;
    }

    /// <summary>
    /// Set the current thirst to this value.
    /// </summary>
    /// <param name="_thirst">The value to set the thirst to.</param>
    public void SetCurrentThirst(int _thirst)
    {
        CurrentThirst = _thirst;
    }

    /// <summary>
    /// Heals the player. Can't be under 0.
    /// </summary>
    /// <param name="_healAmount">The amount to heal the player.</param>
    public void Heal(int _healAmount)
    {
        if (_healAmount < 0)
        {
            Debug.LogWarning($"You healed for a negative amount: {_healAmount}");
            return;
        }

        CurrentHealth += _healAmount;
    }

    /// <summary>
    /// Revive the player
    /// </summary>
    public void RevivePlayer()
    {
        CurrentHealth = Mathf.RoundToInt(m_maxHealth * m_revivePercentage);
        CurrentHunger = Mathf.RoundToInt(m_maxHunger * m_revivePercentage);
        CurrentThirst = Mathf.RoundToInt(m_maxThirst * m_revivePercentage);
        IsAlive = true;
        m_currentReviveTimer = 0.0f;
        m_dyingColorAdjustments.saturation.value = 0;
    }

    //public void OnTriggerEnter(Collider other)
    //{
    //    ILootable item = other.GetComponent<ILootable>();
    //    if (item == null) return;
    //    item.Loot();
    //    Destroy(other.gameObject);
    //}

    /// <summary>
    /// Set player stats on revive or load
    /// </summary>
    /// <param name="_currentHealth">current health value</param>
    /// <param name="_currentHunger">current hunger value</param>
    /// <param name="_currentThirst">current thirst value</param>
    public void SetPlayerStats(int _currentHealth, int _currentHunger, int _currentThirst)
    {
        // activate statusbars upon load and update values
        UIManager.Instance.ActivateLifeParameterUI(true);
        CurrentHealth = _currentHealth;
        CurrentHunger = _currentHunger;
        CurrentThirst = _currentThirst;
    }
}

[Serializable]
public class ThrowableSeed
{
    [SerializeField] private KeyCode m_throwSeedKey;
    [SerializeField] private GameObject m_seedPlantPrefab;
    [SerializeField] private ERESOURCETYPE m_seedResource;

    public KeyCode GetKey => m_throwSeedKey;
    public GameObject GetPrefab => m_seedPlantPrefab;
    public ERESOURCETYPE GetResourceType => m_seedResource;
}
