using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDrinkable : IMonoInterface
{
    /// <summary>
    /// Drinks something
    /// </summary>
    /// <param name="_drinkAmount">Desired amount to drink</param>
    /// <returns>Actually drunk amount (may be less, due to not supplying enough water)</returns>
    public int Drink(int _drinkAmount);
}
