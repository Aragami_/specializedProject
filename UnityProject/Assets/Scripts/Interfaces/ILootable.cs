using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILootable
{
    /// <summary>
    /// Loots this
    /// </summary>
    void Loot();
}
