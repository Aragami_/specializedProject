using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEdible : IMonoInterface
{
    /// <summary>
    /// Eats this
    /// </summary>
    /// <param name="_eatAmount">Desired amount to eat</param>
    /// <returns>Actually eaten amount (may be less, due to not supplying enough food</returns>
    public int EatFood(int _eatAmount);

    /// <summary>
    /// Amount of food this can supply right now
    /// </summary>
    public int GetFoodCount { get; }
}
