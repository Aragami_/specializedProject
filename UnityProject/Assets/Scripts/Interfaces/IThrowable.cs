using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IThrowable
{
    public void Throw(Vector3 _force);
}
