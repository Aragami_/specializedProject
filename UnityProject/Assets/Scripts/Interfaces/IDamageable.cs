using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageable : IMonoInterface
{
    /// <summary>
    /// Damages something
    /// </summary>
    /// <param name="_damageAmount">Amount of damage</param>
    /// <returns>True if HP is 0 or below after damage taken, else False</returns>
    public bool Damage(float _damageAmount = 1);
}
