using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMonoInterface
{
    /// <summary>
    /// The Transform attached to this GameObject.
    /// </summary>
    public Transform transform { get; }
    /// <summary>
    /// The game object this component is attached to. A component is always attached to a game object.
    /// </summary>
    public GameObject gameObject { get; }
}
