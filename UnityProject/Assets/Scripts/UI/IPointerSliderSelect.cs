using UnityEngine;
using UnityEngine.EventSystems;

public class IPointerSliderSelect : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [Tooltip("FMOD Sound Component for slider")]
    private FMODUnity.StudioEventEmitter m_sliderEvent;

    private void Awake()
    {
        m_sliderEvent = GetComponent<FMODUnity.StudioEventEmitter>();
    }

    /// <summary>
    /// button down - play sound once
    /// </summary>
    /// <param name="eventData">pointer event</param>
    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            PlaySound();
        }
    }

    /// <summary>
    /// button up - play sound once
    /// </summary>
    /// <param name="eventData">pointer event</param>
    public void OnPointerUp(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            PlaySound();
        }
    }

    /// <summary>
    /// play sound saved in StudioEventEmitter once
    /// </summary>
    private void PlaySound()
    {
        // play event (path) on selected on studioeventemitter
        FMODUnity.RuntimeManager.PlayOneShotAttached(m_sliderEvent.Event, PlayerController.Instance.gameObject);
    }
}
