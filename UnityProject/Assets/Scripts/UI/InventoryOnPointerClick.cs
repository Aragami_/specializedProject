using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventoryOnPointerClick : MonoBehaviour, IPointerClickHandler
{
    /// <summary>
    /// Button Click Event
    /// </summary>
    /// <param name="eventData">event that happen (PointerEventData)</param>
    public void OnPointerClick(PointerEventData eventData)
    {
        // if right clicked on button - open panel
        if (eventData.button == PointerEventData.InputButton.Right)
            UIManager.Instance.ItemOptionsPanel(GetComponent<Button>(), true);

        // left click on any button - close panel
        if (eventData.button == PointerEventData.InputButton.Left)
            UIManager.Instance.ItemOptionsPanel(GetComponent<Button>(), false);

    }

}
