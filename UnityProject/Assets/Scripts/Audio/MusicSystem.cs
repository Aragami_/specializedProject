using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicSystem : MonoBehaviour
{
    private FMODUnity.StudioEventEmitter m_emitter;
    private void Awake()
    {
        AreaManager.MapTypeChangedEvent += ChangeMusic;
        m_emitter = GetComponent<FMODUnity.StudioEventEmitter>();
    }

    public void ChangeMusic(MAPTYPE _mapType, AREATYPE _areaType)
    {
        if (_mapType == MAPTYPE.MUSIC)
        {
            switch (_areaType)
            {
                case AREATYPE.GRASS:
                    m_emitter.SetParameter("Area", 0);
                    break;
                case AREATYPE.SAND:
                    m_emitter.SetParameter("Area", 1);
                    break;
            }
        }
    }
}
