using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum AREATYPE
{
    NONE = 0,
    GRASS = 1,
    SAND = 2,
    STONE = 3,
    WATER = 4
}

public enum MAPTYPE
{
    NONE = 0,
    FOOTSTEP = 1,
    MUSIC = 2,
    TREE_SAPLINGS = 103,
    BERRY_SAPLINGS = 104
}

[System.Serializable]
public class Map
{
    [SerializeField] private MAPTYPE m_mapType;
    [SerializeField] private Texture2D m_map;
    private AREATYPE m_currentAreatype;
    public MAPTYPE GetMapType => m_mapType;
    public Texture2D GetMap => m_map;

    public AREATYPE CurrentAreaType
    {
        get => m_currentAreatype;
        set => m_currentAreatype = value;
    }

}

[System.Serializable]
public struct KnowColor
{
    [SerializeField] private AREATYPE m_areaType;
    [SerializeField] private Color m_color;

    public AREATYPE GetAreaType => m_areaType;
    public Color GetColor => m_color;
    public float r => m_color.r;
    public float g => m_color.g;
    public float b => m_color.b;
}

public delegate void MapTypeChangedEvent(MAPTYPE _mapType, AREATYPE _areaType);

public class AreaManager : MonoBehaviour
{
    public static event MapTypeChangedEvent MapTypeChangedEvent;

    public static AreaManager Instance { get; private set; }

#pragma warning disable 649 // Unassigned warning
    [SerializeField] private Transform m_uV00Position;
    [SerializeField] private Transform m_uV11Position;
    [SerializeField] private Map[] m_maps;
    [SerializeField] private KnowColor[] m_knowColors;
#pragma warning restore 649
    // ReSharper disable once InconsistentNaming -- Should function as const, but should also be able to edit from the inspector
    [SerializeField] private float TOLERANCE = 0.05f;

    private void Awake()
    {
        if (Instance)
        {
            Destroy(this.gameObject);
            return;
        }

        Instance = this;
    }

    /// <summary>
    /// Finds the AreaType at the given position and updates all event Maps
    /// </summary>
    /// <param name="_position">Position for AreaType</param>
    /// <param name="_mapType">MapType of requested map</param>
    /// <returns>AreaType at this position</returns>
    public AREATYPE GetAreaType(Vector3 _position, MAPTYPE _mapType)
    {
        UpdateAreaMaps(_position);
        foreach (Map map in m_maps)
        {
            if (map.GetMapType == _mapType)
                return map.CurrentAreaType;
        }
        throw new IndexOutOfRangeException("No map usable");
    }

    /// <summary>
    /// Finds the AreaType at the given position. Similar to GetAreaType(), but this method doesn't update other maps
    /// </summary>
    /// <param name="_position">Position for AreaType</param>
    /// <param name="_mapType">MapType of requested map</param>
    /// <returns>AreaType at this position</returns>
    public AREATYPE GetSingleAreaType(Vector3 _position, MAPTYPE _mapType) // For example when you want to find the AREATYPE at a location without updating the music to be there
    {
        foreach (Map map in m_maps)
        {
            if (map.GetMapType == _mapType)
            {
                Vector2 percentageBetweenTwoPoints = GetPercentageBetweenTwoPoints(_position);
                AREATYPE newAreaType = GetAreaByKnownColor(GetUvColor(percentageBetweenTwoPoints, map.GetMap));
                return newAreaType;
            }
        }
        throw new IndexOutOfRangeException("Map not found");
    }

    private void UpdateAreaMaps(Vector3 _position)
    {
        Vector2 percentageBetweenTwoPoints = GetPercentageBetweenTwoPoints(_position);
        foreach (Map map in m_maps)
        {
            AREATYPE newAreaType = GetAreaByKnownColor(GetUvColor(percentageBetweenTwoPoints, map.GetMap));
            if (newAreaType != map.CurrentAreaType)
            {
                map.CurrentAreaType = newAreaType;
                MapTypeChangedEvent?.Invoke(map.GetMapType, newAreaType);
            }
        }
    }

    /// <summary>
    /// Finds the AreaType of the given color, if it matches an already know color
    /// </summary>
    /// <param name="_color">Given color</param>
    /// <returns>AreaType of that color, NONE if no matching found</returns> // For example the image has a fluent transition between pixels and not set absolute
    private AREATYPE GetAreaByKnownColor(Color _color)
    {
        foreach (KnowColor knownColor in m_knowColors)
        {
            if (Math.Abs(_color.r - knownColor.r) < TOLERANCE && Math.Abs(_color.g - knownColor.g) < TOLERANCE && Math.Abs(_color.b - knownColor.b) < TOLERANCE)
            {
                return knownColor.GetAreaType;
            }
        }
        return AREATYPE.NONE;
    }

    /// <summary>
    /// Converts a World Space position into UV coordinates between the two given positions (members)
    /// </summary>
    /// <param name="_pointInBetween">Position in between</param>
    /// <returns>UV of position</returns>
    private Vector2 GetPercentageBetweenTwoPoints(Vector3 _pointInBetween)
    {
        return new Vector3(
            Mathf.InverseLerp(m_uV00Position.position.x, m_uV11Position.position.x, _pointInBetween.x),
            Mathf.InverseLerp(m_uV00Position.position.z, m_uV11Position.position.z, _pointInBetween.z));
    }

    /// <summary>
    /// Returns the color of the area map given the uv coordinates
    /// </summary>
    /// <param name="_percentage">UV coordinates</param>
    /// <param name="_invert">Inverted to mirror rotation</param> //Our use case needs to be inverted
    /// <returns>Color at that UV position</returns>
    private Color GetUvColor(Vector2 _percentage, Texture2D _map, bool _invert = true)
    {
        return _map.GetPixelBilinear(_invert ? _percentage.y : _percentage.x, _invert ? _percentage.x : _percentage.y);
    }
}
