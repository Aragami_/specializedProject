using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachEmitterToPlayer : MonoBehaviour
{
    private void Start()
    {
        if (PlayerController.Instance)
        {
            this.transform.parent = PlayerController.Instance.transform;
        }
    }
}
