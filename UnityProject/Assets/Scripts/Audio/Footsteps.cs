﻿#define WITHPARAMTERS
using System;
using System.Collections;
using System.Collections.Generic;
using FMOD.Studio;
using FMODUnity;
using UnityEngine;
using Random = UnityEngine.Random;

public class Footsteps : MonoBehaviour
{
#pragma warning disable 649
    [FMODUnity.EventRef, SerializeField] private string m_eventPath;
#pragma warning restore 649

    [SerializeField] private float m_StepDistance = 2.0f;
    private float m_stepRand;
    private Vector3 m_prevPos;
    private float m_distanceTraveled;

    private FMODUnity.StudioEventEmitter m_soundEmitterFootStep;

    private void Awake()
    {
        Random.InitState(System.DateTime.Now.Second);

        m_stepRand = Random.Range(0.0f, 0.5f);
        m_prevPos = transform.position;

        m_soundEmitterFootStep = GetComponent<FMODUnity.StudioEventEmitter>();
    }

    void Update()
    {
        m_distanceTraveled += (transform.position - m_prevPos).magnitude;
        if (PlayerController.Instance.IsGrounded && m_distanceTraveled >= m_StepDistance + m_stepRand)
        {
            PlayFootstepSound(PlayerController.Instance.IsOnTerrain);
            m_stepRand = Random.Range(0.0f, 0.5f); // Suttle differences in steps
            m_distanceTraveled = 0.0f;
        }

        m_prevPos = transform.position;
    }

    void PlayFootstepSound(bool _isOnTerrain)
    {

        if (m_eventPath != null)
        {
            FMOD.Studio.EventInstance e = FMODUnity.RuntimeManager.CreateInstance(m_eventPath);
            e.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));

            if (_isOnTerrain)
            {
                switch (AreaManager.Instance.GetAreaType(this.transform.position, MAPTYPE.FOOTSTEP)) // Player.Instance.transform.position
                {
                    case AREATYPE.GRASS:
                        e.setParameterByName("Underground", 1); // 1 - Grass
                        break;
                    case AREATYPE.SAND:
                        e.setParameterByName("Underground", 0); // 1 - Sand
                        break;
                    case AREATYPE.STONE:
                        e.setParameterByName("Underground", 2); // 2 - Stone
                        break;
                    case AREATYPE.WATER:
                        e.setParameterByName("Underground", 3); // 3 - Water
                        break;
                    case AREATYPE.NONE:
                    default:
                        e.setParameterByName("Underground", 4); // 4 - Wood
                        break;
                }
            }
            else
            {
                e.setParameterByName("Underground", 4); // 4 - Wood
            }

            e.start();
            e.release(); //Release each event instance immediately, there are fire and forget, one-shot instances. 
        }
    }
}
