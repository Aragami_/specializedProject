using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FMODUnity;

[RequireComponent(typeof(StudioEventEmitter))]
public class RiverSound : MonoBehaviour
{
    // this can be made public and assigned directly
    [SerializeField] private List<Transform> m_wayPoints = new List<Transform>();

    [SerializeField] private StudioListener m_player;

    [SerializeField] private float m_audioSourceSpeed = 2f;

    private Vector3 m_closestPoint;
    private Vector3 m_secondClosestPoint;

    private void Awake()
    {
        if (m_player == null)
        {
            m_player = FindObjectOfType<StudioListener>();
        }
    }

    void Update()
    {
        List<Vector3> closestPoints = new List<Vector3>();
        foreach (Transform wayPoint in m_wayPoints)
        {
            closestPoints.Add(wayPoint.position);
        }
        // Sort way-points by relative distance
        closestPoints = SortPointsByDistanceTo(closestPoints, m_player.transform.position);
        // Get the two closest way-points and find a point in between them
        this.transform.position = Vector3.Lerp(this.transform.position, ClosestPointOnLine(closestPoints[0], closestPoints[1], m_player.transform.position), Time.deltaTime * m_audioSourceSpeed);
    }

    private List<Vector3> SortPointsByDistanceTo(List<Vector3> _allPoints, Vector3 _pointToBeCloseTo)
    {
        _allPoints.Sort(delegate(Vector3 _first, Vector3 _second)
        {
            Vector3 directionToPointOne = _first - _pointToBeCloseTo;
            Vector3 directionToPointTwo = _second - _pointToBeCloseTo;
            return directionToPointOne.sqrMagnitude.CompareTo(directionToPointTwo.sqrMagnitude);
            //return Vector3.Distance(_first, m_player.transform.position).CompareTo(Vector3.Distance(_second, m_player.transform.position));
        });
        return _allPoints;
    }

    private Vector3 ClosestPointOnLine(Vector3 _firstPoint, Vector3 _secondPoint, Vector3 _closeTargetPoint)
    {
        Vector3 vVector1 = _closeTargetPoint - _firstPoint;
        Vector3 vVector2 = (_secondPoint - _firstPoint).normalized;

        float d = Vector3.Distance(_firstPoint, _secondPoint);
        float t = Vector3.Dot(vVector2, vVector1);

        if (t <= 0)
            return _firstPoint;

        if (t >= d)
            return _secondPoint;

        Vector3 vVector3 = vVector2 * t;

        Vector3 vClosestPoint = _firstPoint + vVector3;

        return vClosestPoint;
    }
}