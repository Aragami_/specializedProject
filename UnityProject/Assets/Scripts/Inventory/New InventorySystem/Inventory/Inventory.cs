using UnityEngine;

public class Inventory
{
    #region Variables

    [Tooltip("Item collection")]
    private Item[] m_items = new Item[16];

    [Tooltip("Inventory ID = UserID")]
    private int m_inventoryID = 0;

    [Tooltip("Slots")]
    private int[] m_slotID = new int[16];

    #endregion

    #region Properties

    public Item[] Items { get => m_items; set => m_items = value; }
    public int InventoryID { get => m_inventoryID; set => m_inventoryID = value; }
    public int[] SlotID { get => m_slotID; set => m_slotID = value; }

    #endregion

    #region Constructor

    /// <summary>
    /// default ctor
    /// </summary>
    public Inventory() { }

    /// <summary>
    /// ctor for Inventory
    /// </summary>
    /// <param name="_inventoryID">Inventory ID</param>
    /// <param name="_slotIDs">Slot IDs</param>
    /// <param name="_resourceID">Resource IDs</param>
    /// <param name="_toolID">Tool IDs</param>
    /// <param name="_consumableID">Consumable IDs</param>
    /// <param name="_amount">Amount</param>

    public Inventory(Item[] _items)
    {
        m_items = _items;
        m_inventoryID = GameManager.Instance.DbHandler.UserID;
        m_slotID = GameManager.Instance.DbHandler.SlotID;
    }
    #endregion
}
