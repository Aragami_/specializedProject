using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

// storage for all inventory belongings

public class UIImageContainer : MonoBehaviour
{
    public List<Image> m_InventorySprites = new List<Image>();
    public List<TMP_Text> m_InventoryAmounts = new List<TMP_Text>();
    public List<Button> m_InventoryButtons = new List<Button>();
}
