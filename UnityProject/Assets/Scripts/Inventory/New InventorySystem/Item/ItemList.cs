using System.Collections.Generic;

public class ItemList
{
    List<Item> m_allItems;

    public List<Item> AllItems { get => m_allItems; private set => m_allItems = value; }

    public ItemList() { m_allItems = new List<Item>(); }

    public void AddItems(List<Item> _items)
    {
        foreach (Item item in _items)
        {
            if (!m_allItems.Contains(item))
            {
                m_allItems.Add(item);
            }
        }
    }
    public void AddItem(Item _item)
    {
        m_allItems.Add(_item);
    }

    public Item GetItem(int _id)
    {
        Item tmp = new Item();
        foreach (Item item in m_allItems)
        {
            if (item.ItemID == _id)
            {
                tmp = new Item(item.ItemID, item.ItemName, item.Description, item.HungerBenefit, item.ThirstBenefit, item.Durability, item.Damage);
            }
        }
        return tmp;
    }

    public Item GetItem(ERESOURCETYPE _id)
    {
        Item tmp = new Item();
        foreach (Item item in m_allItems)
        {
            if (item.ItemID == (int)_id)
            {
                tmp = new Item(item.ItemID, item.ItemName, item.Description, item.HungerBenefit, item.ThirstBenefit, item.Durability, item.Damage);
            }
        }
        return tmp;
    }

}
