using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Item : AItem, ILootable
{

    public Item() { }

    public Item(int _id, string _name, string _desc, int? _hungerAmount = 0,
        int? _thirstAmount = 0, int? _durability = 0, int? _attack = 0, int? _amount = 0)
    {
        m_itemID = _id;
        m_itemName = _name;
        m_description = _desc;
        m_hungerBenefit = _hungerAmount == null ? 0 : (int)_hungerAmount;
        m_thirstBenefit = _thirstAmount == null ? 0 : (int)_thirstAmount;
        m_durability = _durability == null ? 0 : (int)_durability;
        m_damage = _attack == null ? 0 : (int)_attack;
        m_icon = Resources.Load<Sprite>($"Sprites/Items/Icons/{_name}");
        m_amount = _amount == null ? 0 : (int)_amount;
    }

    public Item(Item _item)
    {
        m_itemID = _item.ItemID;
        m_itemName = _item.ItemName;
        m_description = _item.Description;
        m_hungerBenefit = _item.HungerBenefit;
        m_thirstBenefit = _item.ThirstBenefit;
        m_durability = _item.Durability;
        m_damage = _item.Damage;
        m_icon = Resources.Load<Sprite>($"Sprites/Items/Icons/{_item.ItemName}");
        m_amount = _item.Amount;
    }

    public void Loot()
    {
        InventoryManager.Instance.AddItem(m_itemID, m_amount);
    }

    public override void Use(int _id)
    {
        // Plant Seed
        if (m_itemID == 101)
        {
            if (InventoryManager.Instance.GetItemAmount((int)InventoryManager.Instance.ThrowableSeeds[1].GetResourceType) >= 1)
            {
                MonoBehaviour.Instantiate(InventoryManager.Instance.ThrowableSeeds[1].GetPrefab, PlayerController.Instance.transform.position, Quaternion.identity);
            }
        }
        else if (m_itemID == 4)
        {
            if (InventoryManager.Instance.GetItemAmount((int)InventoryManager.Instance.ThrowableSeeds[0].GetResourceType) >= 1)
            {
                MonoBehaviour.Instantiate(InventoryManager.Instance.ThrowableSeeds[0].GetPrefab, PlayerController.Instance.transform.position, Quaternion.identity);
            }
        }
    }

    public override void Consume()
    {
        if (m_itemID >= 100 && m_itemID <= 500)
        {
            PlayerController.Instance.EatDrink(m_hungerBenefit, m_thirstBenefit);
            InventoryManager.Instance.SubtractItem(m_itemID, 1);
        }
    }
}
