using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AItem
{
    [Tooltip("Set Resource ID if it's a resource else keep 0")]
    [SerializeField]
    protected int m_itemID;

    [Tooltip("Set Amount")]
    [SerializeField]
    protected int m_amount;

    [Tooltip("Give a small story or description")]
    [TextArea(15, 20)]
    protected string m_description;

    [Tooltip("Itemname")]
    protected string m_itemName;

    [Tooltip("Durability amount - Tool specific")]
    protected int m_durability;

    [Tooltip("Damage amount - Tool specific")]
    protected int m_damage;

    [Tooltip("Recover amount - Consumable specific")]
    protected int m_hungerBenefit;

    [Tooltip("Recover amount - Consumable specific")]
    protected int m_thirstBenefit;

    protected Sprite m_icon;

    public int Amount
    { get => m_amount; set => m_amount = value; }
    public int ItemID { get => m_itemID; private set => m_itemID = value; }
    public string ItemName { get => m_itemName; set => m_itemName = value; }
    public int Durability { get => m_durability; set => m_durability = value; }
    public int Damage { get => m_damage; set => m_damage = value; }
    public int HungerBenefit { get => m_hungerBenefit; set => m_hungerBenefit = value; }
    public int ThirstBenefit { get => m_thirstBenefit; set => m_thirstBenefit = value; }
    public Sprite Icon { get => m_icon; set => m_icon = value; }
    public string Description { get => m_description; set => m_description = value; }

    public abstract void Use(int _id);
    public abstract void Consume();

}
