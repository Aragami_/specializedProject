using UnityEngine;

public class ItemScript : MonoBehaviour, ILootable
{
    [SerializeField]
    private Item[] m_item;

    public Item[] Item { get => m_item; set => m_item = value; }
    public PlantType Type { get => m_type; set => m_type = value; }

    private PlantType m_type;

    private void Awake()
    {
        // set planttype
        if (TryGetComponent(out Plant plant))
            m_type = plant.m_plantType;

        else
        {
            if (this.gameObject.name == "Log01 1(Clone)")
            {
                m_type = PlantType.NONE;
                return;
            }
            m_type = PlantType.NULL;
        }
    }

    /// <summary>
    /// Adds item on Loot call
    /// </summary>
    public void Loot()
    {
        foreach (Item item in m_item)
        {
            item.Loot();
        }
        GameManager.DestroyOnNextUpdate(this.gameObject);
    }
}
