using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DeveloperConsoleBehaviour : MonoBehaviour
{
    public static DeveloperConsoleBehaviour Instance = null;

    #region Variables

    #region SerializeField
    [Header("Commands")]
    [SerializeField]
    [Tooltip("An array with every existing command. IMPORTENT set this in the Editor.")]
    private AConsoleCommand[] m_commands = null;

    [SerializeField]
    [Tooltip("The prefix to use. This indecates that a command is used.")]
    private string m_prefix = string.Empty;

    [Header("UI")]
    [SerializeField]
    [Tooltip("The panel for the console.")]
    private GameObject m_console = null;

    [SerializeField]
    [Tooltip("The input field for the console.")]
    private TMP_InputField m_commandInput = null;
    #endregion

    [Tooltip("The current existing DeveloperConsole.")]
    private DeveloperConsole m_deleloperConsole = null;

    [Tooltip("Is the console active? Default = false.")]
    private bool m_isConsoleActive = false;

    public DeveloperConsole MyDeveloperConsole
    {
        get
        {
            if (m_deleloperConsole != null)
            {
                return m_deleloperConsole;
            }

            return m_deleloperConsole = new DeveloperConsole(m_prefix, m_commands);
        }
    }

    public bool IsConsoleActive
    {
        get => m_isConsoleActive;
        private set
        {
            m_isConsoleActive = value;
        }
    }
    #endregion

    #region Awake | Update
    private void Awake()
    {
        if (Instance)
        {
            Destroy(this.gameObject);
        }

        Instance = this;
    }

    private void Update()
    {
        ToggleConsole();

        // Only will happen if the console is also open.
        if (IsConsoleActive && Input.GetKeyDown(KeyCode.Return))
        {
            UseCommand(m_commandInput.text);
        }
    }
    #endregion

    /// <summary>
    /// Toggle the console on and off with the ^ key.
    /// </summary>
    public void ToggleConsole()
    {
        if (Input.GetKeyDown(KeyCode.F7))
        {
            // If the console is active, deactivate.
            if (IsConsoleActive)
            {
                m_console.SetActive(false);
                IsConsoleActive = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
            // If the console is inactive, activate. Also activates the input field so the user doesn't need to click on it.
            else
            {
                m_console.SetActive(true);
                IsConsoleActive = true;
                m_commandInput.ActivateInputField();
                BuildingSystem.Instance.CloseBuildingUI();
                BuildingSystem.Instance.DeactivateBuildingSystem();
                Cursor.lockState = CursorLockMode.None;
            }
        }
    }

    /// <summary>
    /// Send the command to the DeveloperConsole script, where it will be processed.
    /// </summary>
    /// <param name="_input">The input from the input field.</param>
    public void UseCommand(string _input)
    {
        MyDeveloperConsole.ProcessCommand(_input);

        m_commandInput.text = string.Empty;
        m_commandInput.ActivateInputField();
        Cursor.lockState = CursorLockMode.None;
    }
}
