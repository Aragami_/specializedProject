using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SetHunger Command", menuName = "Commands/Hunger")]
public class SetHunger : AConsoleCommand
{
    public override bool Process(string[] _args)
    {
        if (_args.Length != 1)
        {
            return false;
        }

        int result;

        if (!int.TryParse(_args[0], out result))
        {
            GameManager.Instance.EventMessage($"{_args[0]} is not a valid value!");
            return false;
        }

        PlayerController.Instance.SetCurrentHunger(result);

        return true;
    }
}
