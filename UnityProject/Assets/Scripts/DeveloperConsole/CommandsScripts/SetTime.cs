using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SetTime Command", menuName = "Commands/Time")]
public class SetTime : AConsoleCommand
{
    public override bool Process(string[] _args)
    {
        if (_args.Length != 1)
        {
            return false;
        }

        string result = _args[0];

        switch (result)
        {
            case "day":
            case "Day":
                GameManager.Instance.SetDayTime(true);
                break;
            case "night":
            case "Night":
                GameManager.Instance.SetDayTime(false);
                break;
            default:
                GameManager.Instance.EventMessage($"{result} is not a valid time!");
                break;
        }

        return true;
    }
}
