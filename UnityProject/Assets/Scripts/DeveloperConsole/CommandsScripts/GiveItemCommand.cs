using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GiveItem Command", menuName = "Commands/GiveItem")]
public class GiveItemCommand : AConsoleCommand
{
    public override bool Process(string[] _args)
    {
        if (_args.Length != 2)
        {
            return false;
        }

        int[] values = new int[_args.Length];

        for (int i = 0; i < _args.Length; i++)
        {
            // Checks every argument, which should be only 2, if it can be parsed into an int.
            // If not it will return false.l
            if (!int.TryParse(_args[i], out int result))
            {
                return false;
            }
            // Otherwise it will fill the array with the values.
            // First value should be the resource type, second item id and third the amount.
            values[i] = result;
        }

        InventoryManager.Instance.AddItem(values[0], values[1]);
        GameManager.Instance.EventMessage("Why are you cheating?...");
        return true;
    }
}
