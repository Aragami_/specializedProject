using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Godmode Command", menuName = "Commands/Godmode")]
public class GodmodeCommand : AConsoleCommand
{
    public override bool Process(string[] _args)
    {
        // This command doesn't actually need arguments.
        // Will also work if there are arugments.
        // If it is wanted that it only works if there is no arguments comment this below in.

        if (_args.Length != 0)
        {
            return false;
        }

        // Toggle godmode on and off, depending on if it is currently on or off.
        if (GameManager.Instance.GodMode)
        {
            GameManager.Instance.GodMode = false;
            GameManager.Instance.EventMessage("Godmode deactivated... but still a cheater.");
        }
        else
        {
            GameManager.Instance.GodMode = true;
            PlayerController.Instance.FreezingVignette.intensity.value = 0.0f;
            GameManager.Instance.EventMessage("Godmode activated... Cheater.");
        }

        return true;
    }
}
