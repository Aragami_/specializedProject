using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Heal Command", menuName = "Commands/Heal")]
public class Heal : AConsoleCommand
{
    public override bool Process(string[] _args)
    {
        // This command doesn't actually need arguments.
        // Will also work if there are arugments.
        // If it is wanted that it only works if there is no arguments comment this below in.

        if (_args.Length != 0)
        {
            return false;
        }


        PlayerController.Instance.SetCurrentHealth(PlayerController.Instance.MaxHealth);
        PlayerController.Instance.SetCurrentHunger(PlayerController.Instance.MaxHunger);
        PlayerController.Instance.SetCurrentThirst(PlayerController.Instance.MaxThirst);

        return true;
    }
}
