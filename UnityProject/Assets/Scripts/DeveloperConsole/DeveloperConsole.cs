using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DeveloperConsole
{
    [Tooltip("Indicates that a command is used.")]
    private readonly string m_prefix = string.Empty;
    [Tooltip("List or Array of existing commmands.")]
    private readonly IEnumerable<AConsoleCommand> m_commands;

    /// <summary>
    /// Create a DeveloperConsole
    /// </summary>
    /// <param name="_prefix">Indicator for commands.</param>
    /// <param name="_commands">Existing commands List/Array.</param>
    public DeveloperConsole(string _prefix, IEnumerable<AConsoleCommand> _commands)
    {
        m_prefix = _prefix;
        m_commands = _commands;
    }

    /// <summary>
    /// Process the command. Seperating command from arguments etc.
    /// For more details look into the Method.
    /// </summary>
    /// <param name="_input">The input in the console.</param>
    public void ProcessCommand(string _input)
    {
        // Check if the prefix is at the start. If not then it will return and do nothing.
        if(!_input.StartsWith(m_prefix))
        {
            return;
        }

        // Removes the prefix from the string.
        _input = _input.Remove(0, m_prefix.Length);
        // Seperates the string into command + arguments.
        string[] inputSplit = _input.Split(' ');

        // Take out the command.
        string commandName = inputSplit[0];
        // Skip the first value and make the rest to an array.
        string[] args = inputSplit.Skip(1).ToArray();

        // Checks every listed command if it is used.
        foreach (AConsoleCommand command in m_commands)
        {
            // If it isn't equal the command, it will continue to the next existing command.
            if (!commandName.Equals(command.CommandWord, System.StringComparison.Ordinal))
            {
                continue;
            }

            // If the command exist, then it will do it and stop this foreach.
            if (command.Process(args))
            {
                return;
            }
        }
    }
}
