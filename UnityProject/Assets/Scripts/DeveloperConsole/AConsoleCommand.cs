using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AConsoleCommand : ScriptableObject
{
    [SerializeField]
    [Tooltip("What is the command word for this command?")]
    private string m_commandWord = string.Empty;

    public string CommandWord
    {
        get => m_commandWord;
    }

    /// <summary>
    /// Need to be filled in the childs.
    /// </summary>
    /// <param name="_args">The arguments.</param>
    /// <returns>return true und successes and false on fail.</returns>
    public abstract bool Process(string[] _args);
}
