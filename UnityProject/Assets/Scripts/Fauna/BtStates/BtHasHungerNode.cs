using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.BehaviourTree;
using UnityEngine;

/// <summary>
/// This node determines whether this animal has hunger
/// </summary>
public class BtHasHungerNode : BtNode
{
    private readonly Animal m_animal;
    private readonly int m_threshold;

    /// <summary>
    /// This node determines whether this animal has hunger
    /// </summary>
    /// <param name="_animal">Animal which has hunger</param>
    /// <param name="_threshold">Below this threshold the animal has hunger</param>
    public BtHasHungerNode(Animal _animal, int _threshold)
    {
        m_animal = _animal;
        m_threshold = _threshold;
    }

    public override NodeState Evaluate()
    {
        return m_animal.GetCurrentSat <= m_threshold ? NodeState.SUCCESS : NodeState.FAILURE;
    }
}
