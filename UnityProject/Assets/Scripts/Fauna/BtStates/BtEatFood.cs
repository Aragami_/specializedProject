using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.BehaviourTree;
using UnityEngine;

/// <summary>
/// Eats closest food
/// </summary>
public class BtEatFood : BtNode
{
    private readonly Animal m_animal;
    private readonly int m_eatAmount;

    public override NodeState Evaluate()
    {
        IEdible closestFood = FloraManager.Instance.GetClosestEdible(m_animal.transform.position);
        if (closestFood == null)
            return NodeState.FAILURE;
        m_animal.Eat(closestFood.EatFood(m_eatAmount));
        return NodeState.SUCCESS;
    }

    /// <summary>
    /// Eats closest food
    /// </summary>
    /// <param name="_animal">Animal which eats</param>
    /// <param name="_eatAmount">Desired amount to eat</param>
    public BtEatFood(Animal _animal, int _eatAmount)
    {
        m_animal = _animal;
        m_eatAmount = _eatAmount;
    }
}
