using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.BehaviourTree;
using UnityEngine;

/// <summary>
/// Moves the animal to the nearest food source
/// </summary>
public class BtGoToFood : BtNode
{
    private readonly Animal m_animal;

    public override NodeState Evaluate()
    {
        IEdible closestFood = FloraManager.Instance.GetClosestEdible(m_animal.transform.position);
        if (closestFood == null)
            return NodeState.FAILURE;
        float distance = Vector3.Distance(closestFood.transform.position, m_animal.transform.position);
        if (distance > 2f)
        {
            m_animal.MoveTo(closestFood.transform.position);
            return NodeState.RUNNING;
        }
        else
        {
            m_animal.StopAgent();
            return NodeState.SUCCESS;
        }
    }

    /// <summary>
    /// Moves the animal to the nearest food source
    /// </summary>
    /// <param name="_animal">Animal to move to food source</param>
    public BtGoToFood(Animal _animal)
    {
        m_animal = _animal;
    }
}
