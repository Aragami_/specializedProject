using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.BehaviourTree;
using UnityEngine;

/// <summary>
/// Note: Node can not return SUCCESS
/// </summary>
public class BtWalkToAnimal : BtNode
{
    private readonly Animal m_animal;
    private readonly Animal m_otherAnimal;

    public override NodeState Evaluate()
    {
        if (m_animal.AgentIsMoving)
        {
            return NodeState.FAILURE;
        }
        else
        {
            Animal target = m_otherAnimal == null ? FaunaManager.Instance.GetRandomOtherAnimal(m_animal) : m_otherAnimal;
            if (target != null)
            {
                m_animal.MoveTo(target.transform.position);
            }
            else
                return NodeState.FAILURE;
            return NodeState.RUNNING;
        }
    }

    /// <summary>
    /// Walks to another Animal
    /// </summary>
    /// <param name="_animal">Own animal</param>
    /// <param name="_otherAnimal">Animal to walk to, if null walk to random other Animal</param>
    public BtWalkToAnimal(Animal _animal, Animal _otherAnimal = null)
    {
        m_animal = _animal;
        m_otherAnimal = _otherAnimal;
    }
}
