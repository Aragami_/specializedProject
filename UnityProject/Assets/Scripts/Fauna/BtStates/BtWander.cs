using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.BehaviourTree;
using UnityEngine;

/// <summary>
/// Wanders in random directions
/// </summary>
public class BtWander : BtNode
{
    private readonly Animal m_animal;
    private const int RANDOM_RANGE = 5;

    public override NodeState Evaluate()
    {
        if (m_animal.AgentIsMoving)
        {
            return NodeState.FAILURE;
        }
        else
        {
            m_animal.MoveTo(new Vector3(m_animal.transform.position.x + Random.Range(-RANDOM_RANGE, (float)RANDOM_RANGE), m_animal.transform.position.y, m_animal.transform.position.z + Random.Range(-RANDOM_RANGE, (float)RANDOM_RANGE)));
            return NodeState.RUNNING;
        }
    }

    /// <summary>
    /// Wanders in random directions
    /// </summary>
    /// <param name="_animal"></param>
    public BtWander(Animal _animal)
    {
        m_animal = _animal;
    }
}
