using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.BehaviourTree;
using UnityEngine;

/// <summary>
/// Moves the animal to the nearest water source
/// </summary>
public class BtGoToWater : BtNode
{
    private readonly Animal m_animal;

    public override NodeState Evaluate()
    {
        IDrinkable closestWater = FloraManager.Instance.GetClosestDrinkable(m_animal.transform.position);
        if (closestWater == null)
            return NodeState.FAILURE;
        float distance = Vector3.Distance(closestWater.transform.position, m_animal.transform.position);
        if (distance > 2f)
        {
            m_animal.MoveTo(closestWater.transform.position);
            return NodeState.RUNNING;
        }
        else
        {
            m_animal.StopAgent();
            return NodeState.SUCCESS;
        }
    }

    /// <summary>
    /// Moves the animal to the nearest water source
    /// </summary>
    /// <param name="_animal">Animal to move to water source</param>
    public BtGoToWater(Animal _animal)
    {
        m_animal = _animal;
    }
}
