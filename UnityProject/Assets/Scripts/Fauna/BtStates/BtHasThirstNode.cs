using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.BehaviourTree;
using UnityEngine;

/// <summary>
/// This node determines whether this animal has thirst
/// </summary>
public class BtHasThirstNode : BtNode
{
    private readonly Animal m_animal;
    private readonly int m_threshold;

    /// <summary>
    /// This node determines whether this animal has thirst
    /// </summary>
    /// <param name="_animal">Animal which has thirst</param>
    /// <param name="_threshold">Below this threshold the animal has thirst</param>
    public BtHasThirstNode(Animal _animal, int _threshold)
    {
        m_animal = _animal;
        m_threshold = _threshold;
    }

    public override NodeState Evaluate()
    {
        return m_animal.GetCurrentWater <= m_threshold ? NodeState.SUCCESS : NodeState.FAILURE;
    }
}
