using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.BehaviourTree;
using UnityEngine;

/// <summary>
/// Drink closest water
/// </summary>
public class BtDrinkWater : BtNode
{
    private readonly Animal m_animal;
    private readonly int m_drinkAmount;

    public override NodeState Evaluate()
    {
        IDrinkable closestDrinkable = FloraManager.Instance.GetClosestDrinkable(m_animal.transform.position);
        if (closestDrinkable == null)
            return NodeState.FAILURE;
        m_animal.Drink(closestDrinkable.Drink(m_drinkAmount));
        return NodeState.SUCCESS;
    }

    /// <summary>
    /// Drink closest water
    /// </summary>
    /// <param name="_animal">Animal which drinks</param>
    /// <param name="_drinkAmount">Desired amount to drink</param>
    public BtDrinkWater(Animal _animal, int _drinkAmount)
    {
        m_animal = _animal;
        m_drinkAmount = _drinkAmount;
    }
}
