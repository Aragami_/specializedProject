using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.BehaviourTree;
using UnityEngine;

public class CowTree : BtTree
{
    public override void ConstructBehaviourTree(Animal _thisAnimal)
    {
        m_myAnimal = _thisAnimal;
        #region Head
        #region Needs
        #region Thirst
        BtHasThirstNode hasThirstNode = new BtHasThirstNode(m_myAnimal, m_myAnimal.GetLowWater);
        BtGoToWater goToWaterNode = new BtGoToWater(m_myAnimal);
        BtDrinkWater drinkWaterNode = new BtDrinkWater(m_myAnimal, 50);
        #endregion

        #region Hunger
        BtHasHungerNode hasHungerNode = new BtHasHungerNode(m_myAnimal, m_myAnimal.GetLowSat);
        BtGoToFood goToFoodNode = new BtGoToFood(m_myAnimal);
        BtEatFood eatFoodNode = new BtEatFood(m_myAnimal, 50);
        #endregion

        BtSequence thirstSequence = new BtSequence(new List<BtNode> { hasThirstNode, goToWaterNode, drinkWaterNode });
        BtSequence hungerSequence = new BtSequence(new List<BtNode> { hasHungerNode, goToFoodNode, eatFoodNode });

        BtSelector needsSequence = new BtSelector(new List<BtNode> { thirstSequence, hungerSequence });
        #endregion

        #region Wander
        BtWander wanderNode = new BtWander(m_myAnimal);
        #endregion

        m_topNode = new BtSelector(new List<BtNode> { needsSequence, wanderNode});
        #endregion
    }
}
