using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.BehaviourTree;
using UnityEngine;

public class FoxTree : BtTree
{
    public override void ConstructBehaviourTree(Animal _thisAnimal)
    {
        m_myAnimal = _thisAnimal;
        #region Head
        #region Needs
        #region Thirst
        BtHasThirstNode hasThirstNode = new BtHasThirstNode(m_myAnimal, m_myAnimal.GetLowWater);
        BtGoToWater goToWaterNode = new BtGoToWater(m_myAnimal);
        BtDrinkWater drinkWaterNode = new BtDrinkWater(m_myAnimal, 50);
        #endregion

        #region Hunger
        BtHasHungerNode hasHungerNode = new BtHasHungerNode(m_myAnimal, m_myAnimal.GetLowSat);
        BtGoToFood goToFoodNode = new BtGoToFood(m_myAnimal);
        BtEatFood eatFoodNode = new BtEatFood(m_myAnimal, 50);
        #endregion

        BtSequence thirstSequence = new BtSequence(new List<BtNode> { hasThirstNode, goToWaterNode, drinkWaterNode });
        BtSequence hungerSequence = new BtSequence(new List<BtNode> { hasHungerNode, goToFoodNode, eatFoodNode });

        BtSelector needsSequence = new BtSelector(new List<BtNode> { thirstSequence, hungerSequence });
        #endregion

        #region Walk
        BtWander wanderNode = new BtWander(m_myAnimal);
        BtWalkToAnimal walkToNode = new BtWalkToAnimal(m_myAnimal, null);
        BtRandom randomWalkNode = new BtRandom(new List<BtNode>{wanderNode, walkToNode}, new List<float>{0.75f, 1f});
        #endregion

        m_topNode = new BtSelector(new List<BtNode> { needsSequence, randomWalkNode });
        #endregion
    }
}
