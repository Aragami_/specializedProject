using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.BehaviourTree;
using UnityEngine;
using UnityEngine.AI;

public class Animal : MonoBehaviour
{
    private Animator m_animator;
    private NavMeshAgent m_agent;

    /// <summary>
    /// Current amount of Hunger
    /// </summary>
    private float m_currentSatAmount;
    /// <summary>
    /// Current amount of thirst
    /// </summary>
    private float m_currentWaterAmount;

    [SerializeField, Tooltip("Starting amount for hunger")] private float m_startingSat = 20f;
    [SerializeField, Tooltip("Starting amount for thirst")] private float m_startingWater = 30f;
    [SerializeField, Tooltip("The loss of hunger each second")] private float m_losingSatPerSecond = 0.25f;
    [SerializeField, Tooltip("The loss of water each second")] private float m_losingWaterPerSecond = 0.5f;
    [SerializeField, Tooltip("The amount of hunger at which the animal should seek food")] private int m_lowSatAmount = 15;
    [SerializeField, Tooltip("The amount of thirst at which the animal should seek water")] private int m_lowWaterAmount = 20;

    private BtTree m_myTree;

    [SerializeField, Tooltip("Time of seconds waited between each behaviour tree update")] private float m_btTreeUpdateTime = 1f;
    private float m_btCurrentTreeUpdateTime = 0f;

    private void Awake()
    {
        m_animator = GetComponent<Animator>();
    }

    /// <summary>
    /// Returns the current amount of hunger
    /// </summary>
    public float GetCurrentSat => m_currentSatAmount;
    /// <summary>
    /// Returns the current amount of thirst
    /// </summary>
    public float GetCurrentWater => m_currentWaterAmount;
    /// <summary>
    /// Returns the amount of hunger at which the animal should seek food
    /// </summary>
    public int GetLowSat => m_lowSatAmount;
    /// <summary>
    /// Returns the amount of thirst at which the animal should seek water
    /// </summary>
    public int GetLowWater => m_lowWaterAmount;

    private void Start()
    {
        m_currentSatAmount = m_startingSat;
        m_currentWaterAmount = m_startingWater;
        m_agent = GetComponent<NavMeshAgent>();

        GetBehaviourTree();
    }

    /// <summary>
    /// Creates the behaviourTree
    /// </summary>
    private void GetBehaviourTree()
    {
        m_myTree = GetComponent<BtTree>();
        m_myTree.ConstructBehaviourTree(this);
    }

    private void OnEnable()
    {
        // Add to list of animals in the game
        FaunaManager.Instance?.AddAnimal(this);
    }

    private void OnDisable()
    {
        // Remove from list of animals in the game
        FaunaManager.Instance?.RemoveAnimal(this);
    }

    private void Update()
    {
        // Manage needs
        m_currentSatAmount -= m_losingSatPerSecond * Time.deltaTime;
        m_currentWaterAmount -= m_losingWaterPerSecond * Time.deltaTime;

        // Die if food to low
        if (m_currentSatAmount < 0f || m_currentWaterAmount < 0f)
        {
            Die();
        }

        // Update the behaviour tree
        m_btCurrentTreeUpdateTime += Time.deltaTime;
        if (m_btCurrentTreeUpdateTime >= m_btTreeUpdateTime)
        {
            m_btCurrentTreeUpdateTime = 0f;
            m_myTree.Evaluate();
        }

        if (m_agent.remainingDistance <= 0.05f)
        {
            StopAgent();
        }
    }

    private void Die()
    {
        GameManager.DestroyOnNextUpdate(this.gameObject);
    }

    /// <summary>
    /// Moves the animal to the designated position 
    /// </summary>
    /// <param name="_moveHere">Position to move to</param>
    public void MoveTo(Vector3 _moveHere)
    {
        m_agent.isStopped = false;
        m_agent.SetDestination(_moveHere);
        //if (m_agent.pathPending || m_agent.hasPath)
        {
            m_animator.SetBool("IsMoving", true);
        }
    }

    /// <summary>
    /// Stops the movement of the animal at the current position
    /// </summary>
    public void StopAgent()
    {
        m_agent.isStopped = true;
        m_animator.SetBool("IsMoving", false);
    }

    /// <summary>
    /// Determines whether the animal is moving right now
    /// </summary>
    public bool AgentIsMoving => !m_agent.isStopped && m_agent.hasPath ? true : false;

    /// <summary>
    /// Adds to the current hunger
    /// </summary>
    /// <param name="_eatAmount">Amount to eat</param>
    public void Eat(int _eatAmount)
    {
        m_currentSatAmount += _eatAmount;
    }

    /// <summary>
    /// Adds to the current thirst
    /// </summary>
    /// <param name="_drinkAmount">Amount to drink</param>
    public void Drink(int _drinkAmount)
    {
        m_currentWaterAmount += _drinkAmount;
    }
}
