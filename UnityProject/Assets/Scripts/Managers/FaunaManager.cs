using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-100)]
public class FaunaManager : MonoBehaviour
{
    public static FaunaManager Instance { get; private set; }

    private List<Animal> m_animals = new List<Animal>();

    private void Awake()
    {
        if (Instance)
        {
            Destroy(this.gameObject);
            return;
        }

        Instance = this;
    }

    /// <summary>
    /// Adds a new Animal to all available animals
    /// </summary>
    /// <param name="_newAnimal">Animal to add</param>
    public void AddAnimal(Animal _newAnimal)
    {
        m_animals.Add(_newAnimal);
    }

    /// <summary>
    /// Removes the animal from the list of globally available animals
    /// </summary>
    /// <param name="_removeAnimal">Animal to remove</param>
    public void RemoveAnimal(Animal _removeAnimal)
    {
        m_animals.Remove(_removeAnimal);
    }

    /// <summary>
    /// Finds a random animal that is not the own
    /// </summary>
    /// <param name="_ownAnimal">Own animal</param>
    /// <returns>Random other animal, null if there are not others</returns>
    public Animal GetRandomOtherAnimal(Animal _ownAnimal)
    {
        List<Animal> otherAnimals = m_animals;
        otherAnimals.Remove(_ownAnimal);

        if (otherAnimals.Count >= 1)
        {
            return otherAnimals[Random.Range(0, otherAnimals.Count)];
        }

        return null;
    }
}
