using FMODUnity;
using FMOD.Studio;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{

    #region Variables

    [Header("Sliders for sound volume editing")]
    [SerializeField]
    [Tooltip("Master Volume Slider")]
    private Slider m_masterSlider;

    [SerializeField]
    [Tooltip("Atmo Volume Slider")]
    private Slider m_atmoSlider;

    [SerializeField]
    [Tooltip("Music Volume Slider")]
    private Slider m_musicSlider;

    [SerializeField]
    [Tooltip("SFX Volume Slider")]
    private Slider m_sfxSlider;

    [Tooltip("Master bus for sound handling")]
    private Bus m_master;

    [Tooltip("Atmosphere bus for sound handling")]
    private Bus m_atmo;

    [Tooltip("Music bus for sound handling")]
    private Bus m_music;

    [Tooltip("SoundEffects bus for sound handling")]
    private Bus m_sfx;

    [Header("Emitters - add sound emitters below")]
    [SerializeField]
    [Tooltip("This is the emitter for the music.")]
    private StudioEventEmitter m_musicEmitter = null;
    [SerializeField]
    [Tooltip("This is the emitter for the heartbeat.")]
    private StudioEventEmitter m_heartbeatEmitter = null;
    [SerializeField]
    [Tooltip("When to play the heartbeat sound or to stop playing. If m_currentHealth <= hearbeatSound -> play Sound.")]
    private int m_heartbeatSound = 0;
    [SerializeField]
    [Tooltip("This is the emitter for the death notification.")]
    private StudioEventEmitter m_deathNotificationEmitter = null;
    [SerializeField]
    [Tooltip("This is the emitter for the hunger state.")]
    private StudioEventEmitter m_hungerEmitter = null;
    [SerializeField]
    [Tooltip("This is the emitter for the timber chop sound.")]
    private StudioEventEmitter m_chopEmitter = null;
    [SerializeField]
    [Tooltip("This is the emitter for the invalid build sound.")]
    private StudioEventEmitter m_invalidBuildEmitter = null;
    [SerializeField]
    [Tooltip("This is the emitter for the crafting sound.")]
    private StudioEventEmitter m_craftingEmitter = null;
    [SerializeField]
    [Tooltip("This is the emitter for the build sound.")]
    private StudioEventEmitter m_buildEmitter = null;
    #endregion

    #region Properties

    public static SoundManager Instance { get; private set; }
    public StudioEventEmitter MusicEmitter { get => m_musicEmitter; private set => m_musicEmitter = value; }
    public StudioEventEmitter HeartbeatEmitter { get => m_heartbeatEmitter; private set => m_heartbeatEmitter = value; }
    public StudioEventEmitter DeathNotificationEmitter { get => m_deathNotificationEmitter; private set => m_deathNotificationEmitter = value; }
    public StudioEventEmitter HungerEmitter { get => m_hungerEmitter; private set => m_hungerEmitter = value; }
    public StudioEventEmitter ChopEmitter { get => m_chopEmitter; private set => m_chopEmitter = value; }
    public int HeartbeatSound { get => m_heartbeatSound; private set => m_heartbeatSound = value; }
    public StudioEventEmitter InvalidBuildEmitter { get => m_invalidBuildEmitter; private set => m_invalidBuildEmitter = value; }
    public StudioEventEmitter CraftingEmitter { get => m_craftingEmitter; private set => m_craftingEmitter = value; }
    public StudioEventEmitter BuildEmitter { get => m_buildEmitter; set => m_buildEmitter = value; }

    #endregion

    private void Awake()
    {
        if (Instance)
        {
            Destroy(this);
            return;
        }

        Instance = this;

        // get all neede buses
        m_master = RuntimeManager.GetBus("bus:/SUB-MASTER");
        m_atmo = RuntimeManager.GetBus("bus:/SUB-MASTER/ATMO");
        m_music = RuntimeManager.GetBus("bus:/SUB-MASTER/MUSIC");
        m_sfx = RuntimeManager.GetBus("bus:/SUB-MASTER/SXF");
    }

    private void Start()
    {
        // start Listener for slider event
        m_masterSlider.onValueChanged.AddListener(delegate { VolumeChanged(); });
        m_atmoSlider.onValueChanged.AddListener(delegate { VolumeChanged(); });
        m_musicSlider.onValueChanged.AddListener(delegate { VolumeChanged(); });
        m_sfxSlider.onValueChanged.AddListener(delegate { VolumeChanged(); });
        VolumeChanged();

    }

    /// <summary>
    /// Set new value on slider changed
    /// </summary>
    public void VolumeChanged()
    {
        m_master.setVolume(m_masterSlider.value);
        m_atmo.setVolume(m_atmoSlider.value);
        m_music.setVolume(m_musicSlider.value);
        m_sfx.setVolume(m_sfxSlider.value);
    }

    /// <summary>
    /// Play timbersound depending if there's an axe or not
    /// </summary>
    /// <returns>Axe used or not</returns>
    public bool PlayTimberSound()
    {

        //start timbersound with an axe
        EventInstance e = RuntimeManager.CreateInstance(m_chopEmitter.Event);
        e.set3DAttributes(RuntimeUtils.To3DAttributes(PlayerController.Instance.transform));
        if (GameManager.Instance.AxeAvailable())
        {
            e.setParameterByName("Utensil", 1);
            e.start();
            e.release();
            return true;
        }

        // start timbersound without an axe
        else
        {
            e.setParameterByName("Utensil", 0);
        }
        e.start();
        e.release();
        return false;
    }

    public void PlaySoundEffect(GameObject _go, StudioEventEmitter _emitter)
    {
        RuntimeManager.PlayOneShotAttached(_emitter.Event, _go);
    }
}
