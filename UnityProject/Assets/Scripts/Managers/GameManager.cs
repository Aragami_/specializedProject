using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public enum EFirstTimeEvent
{
    None = 0,
    FindWood = 1,
    FindWater = 2,
}

[DefaultExecutionOrder(-10)]
public class GameManager : MonoBehaviour
{
    #region Variables
    public static GameManager Instance = null;

    [SerializeField]
    [Tooltip("The first time values for events.")]
    private FirstTimeValuesSO m_ftvSO = null;

    // needed for lightprobe calculation
    //[Tooltip("")]
    //public MeshCollider m_terrain;
    //public LightProbeGroup m_lightProbes;

    [Header("Day and night cycle")]
    [SerializeField]
    [Tooltip("The Sun")]
    private Light m_sun;

    [SerializeField]
    [Tooltip("The Moon")]
    private Light m_moon;

    [SerializeField]
    [Tooltip("Speeds up the daytime, 1 = 6 minutes 6 = 1 minute")]
    [Range(0.5f, 12)]
    private float m_dayAccelerator = 1.0f;

    [SerializeField]
    [Tooltip("Moon's brightness")]
    [Range(0, 0.5f)]
    private float m_moonLight;

    [SerializeField]
    [Tooltip("Sun's brightness")]
    [Range(0, 1f)]
    private float m_sunLight;

    [SerializeField]
    [Tooltip("Timer to check reset position if player is below map")]
    private float m_resetTimer = 0;

    [Tooltip("Position to respawn if player fell below the map")]
    [SerializeField]
    private Transform m_spawn;

    [Header("EventMessage system")]

    [SerializeField]
    [Tooltip("The text field for the event messages.")]
    private TMP_Text m_message = null;

    [SerializeField]
    [Tooltip("How long should the message stay? Default 3s.")]
    private float m_messageTime = 3.0f;

    [Tooltip("The last coroutine.")]
    private Coroutine m_lastCoroutine = null;

    [Tooltip("Container for items the player gained - handles them too")]
    private ItemList m_itemList = new ItemList();

    private static readonly List<GameObject> m_destroyTheseOnNextUpdate = new List<GameObject>();

    [Tooltip("Database handler - handles save and load - from and to database")]
    private DatabaseHandler m_dbHandler;

    public DatabaseHandler DbHandler { get => m_dbHandler; private set => m_dbHandler = value; }

    public FirstTimeValuesSO MyFTSSO
    {
        get => m_ftvSO;
    }

    #region Admin Console Variables
    [Tooltip("Is god mode on or off? Default = false.")]
    private bool m_godMode = false;

    [Tooltip("Tells if it's night or day")]
    private bool m_dayTime = true;

    [Tooltip("Sun start rotation to set day")]
    private Vector3 m_sunStartRot;
    [Tooltip("Moon start rotation to set night")]
    private Vector3 m_moonStartRot;

    #region Property
    public bool GodMode
    {
        get => m_godMode;
        set => m_godMode = value;
    }

    public ItemList ItemList { get => m_itemList; set => m_itemList = value; }

    public bool DayTime { get => m_dayTime; private set => m_dayTime = value; }

    #endregion

    #endregion
    #endregion

    #region Awake | Start | Update
    private void Awake()
    {

        if (Instance)
        {
            Destroy(this.gameObject);
            return;
        }

        Instance = this;

        m_dbHandler = new DatabaseHandler();




        m_sunStartRot = m_sun.transform.eulerAngles;
        m_moonStartRot = m_moon.transform.eulerAngles;

        LoadFirstTimeEvents();

        // remove comment if new lightprobes have to be calculated!
        //CalculateLightProbes();
    }

    // Start is called before the first frame update
    void Start()
    {
        // Should only do it if the message field exist. Otherwise it would throw an error.
        if (m_message)
        {
            // Deactivate it just in case it was forgotten to turned off.
            m_message.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            m_message = GameObject.FindGameObjectWithTag("EventMessage").GetComponent<TMP_Text>();
            if (m_message)
            {
                m_message.transform.parent.gameObject.SetActive(false);
            }
            else
            {
                Debug.LogWarning("No event message field!");
            }
        }
    }


    private void Update()
    {
        DayNightCycle();

        // Reset player position if he fell below the map
        if (m_resetTimer >= 5.0f)
        {
            if (PlayerController.Instance.transform.position.y <= -100)
            {
                PlayerController.Instance.transform.position = m_spawn.position;
            }
            m_resetTimer = 0.0f;
        }

        foreach (GameObject gameObject in m_destroyTheseOnNextUpdate)
        {
            Destroy(gameObject);
        }
        m_destroyTheseOnNextUpdate.Clear();
        m_resetTimer += Time.deltaTime;
    }
    #endregion

    /// <summary>
    /// calculate Lightprobes depending on vertices of terrain collider and rotate them to Terrain rotation
    /// </summary>
    private void CalculateLightProbes()
    {
        // calculate new lightprobepositions
        /*List<Vector3> vertices = new List<Vector3>();
        m_terrain.sharedMesh.GetVertices(vertices);
        float angle = m_terrain.transform.eulerAngles.x;
        for (int i = 0; i < vertices.Count; i++)
        {
        // rotate vertices to terrain positions (if terrain is rotated) - only x axis calculation!!
            vertices[i] = new Vector3(vertices[i].x, vertices[i].y * Mathf.Cos(Mathf.Deg2Rad * angle) - vertices[i].z * Mathf.Sin(Mathf.Deg2Rad * angle), vertices[i].y * Mathf.Sin(Mathf.Deg2Rad * angle) + vertices[i].z * Mathf.Cos(Mathf.Deg2Rad * angle));
        }
        m_lightProbes.probePositions = vertices.ToArray();*/
    }


    /// <summary>
    /// Show a message for x second (GameManager variable).
    /// </summary>
    /// <param name="_message">The message to display.</param>
    public void EventMessage(string _message)
    {

        //// change color depending on daytime
        //if (!m_dayTime)
        //    m_message.color = Color.red;
        //else
        //    m_message.color = Color.black;

        // If the GameManager doesn't have a text field for event messages.
        // Search for an event message in the szene.
        if (!m_message)
        {
            m_message = GameObject.FindGameObjectWithTag("EventMessage").GetComponent<TMP_Text>();
            // If it found it send the message otherwise, doesn't send the message.
            if (m_message)
            {
                if (m_lastCoroutine != null)
                {
                    return;
                }
                m_lastCoroutine = StartCoroutine(Message(_message));
            }
            else
            {
                Debug.LogWarning("No event message field!");
            }
        }
        // Otherwise just send the message.
        else
        {
            if (m_lastCoroutine != null)
            {
                return;
            }
            m_lastCoroutine = StartCoroutine(Message(_message));
        }
    }

    /// <summary>
    /// The actual timer for the event message
    /// </summary>
    /// <param name="_message">The message from EventMessage method.</param>
    /// <returns></returns>
    private IEnumerator Message(string _message)
    {
        m_message.text = _message;
        m_message.transform.parent.gameObject.SetActive(true);
        yield return new WaitForSeconds(m_messageTime);
        m_message.transform.parent.gameObject.SetActive(false);
        m_lastCoroutine = null;
    }

    /// <summary>
    /// Show a message for x second (GameManager variable).
    /// </summary>
    /// <param name="_message">The message to display.</param>
    /// <param name="_time">How long the message will stay.</param>
    public void EventMessage(string _message, float _time)
    {

        //// change color depending on daytime
        //if (!m_dayTime)
        //    m_message.color = Color.red;
        //else
        //    m_message.color = Color.black;

        // If the GameManager doesn't have a text field for event messages.
        // Search for an event message in the szene.
        if (!m_message)
        {
            m_message = GameObject.FindGameObjectWithTag("EventMessage").GetComponent<TMP_Text>();
            // If it found it send the message otherwise, doesn't send the message.
            if (m_message)
            {
                if (m_lastCoroutine != null)
                {
                    return;
                }
                m_lastCoroutine = StartCoroutine(Message(_message, _time));
            }
            else
            {
                Debug.LogWarning("No event message field!");
            }
        }
        // Otherwise just send the message.
        else
        {
            if (m_lastCoroutine != null)
            {
                return;
            }
            m_lastCoroutine = StartCoroutine(Message(_message, _time));
        }
    }

    /// <summary>
    /// The actual timer for the event message
    /// </summary>
    /// <param name="_message">The message from EventMessage method.</param>
    /// <param name="_time">How long the message will stay.</param>
    /// <returns></returns>
    private IEnumerator Message(string _message, float _time)
    {
        m_message.text = _message;
        m_message.transform.parent.gameObject.SetActive(true);
        yield return new WaitForSeconds(_time);
        m_message.transform.parent.gameObject.SetActive(false);
        m_lastCoroutine = null;
    }

    public static void DestroyOnNextUpdate(GameObject _destroyThis)
    {
        m_destroyTheseOnNextUpdate.Add(_destroyThis);
    }

    /// <summary>
    /// Set day or Night - common used for admin console
    /// </summary>
    /// <param name="_day">Day or night</param>
    public void SetDayTime(bool _day)
    {
        if (_day)
        {
            m_sun.transform.rotation = Quaternion.Euler(m_sunStartRot);
            m_moon.transform.rotation = Quaternion.Euler(m_moonStartRot);
        }
        else
        {
            m_sun.transform.rotation = Quaternion.Euler(m_sunStartRot + Vector3.right * 180);
            m_moon.transform.rotation = Quaternion.Euler(m_moonStartRot + Vector3.right * 180);
        }
    }

    /// <summary>
    /// Generate day and night cycle rotation moon and sun directional light
    /// </summary>
    public void DayNightCycle()
    {
        // rotate both lights
        Vector3 sunRotation = m_sun.transform.rotation.eulerAngles;
        m_sun.transform.Rotate(Time.deltaTime * m_dayAccelerator, 0, 0);
        m_moon.transform.Rotate(Time.deltaTime * m_dayAccelerator, 0, 0);

        // check if it's night
        if (m_sun.transform.eulerAngles.x > 180 || m_sun.transform.eulerAngles.x <= 0)
        {
            if (m_dayTime)
                m_dayTime = false;

            // higher moonlight intensity
            if (m_moon.intensity <= m_moonLight)
                m_moon.intensity += Time.deltaTime;

            // reduce sunlight intensity
            m_sun.intensity -= Time.deltaTime;

            // higher fog
            if (RenderSettings.fogDensity <= 0.05)
            {
                RenderSettings.fogDensity += Time.deltaTime / 10;
            }
        }

        // it's day
        else if (m_sun.intensity <= 1)
        {
            if (!m_dayTime)
                m_dayTime = true;

            // lower moon light
            if (m_moon.intensity > 0)
                m_moon.intensity -= Time.deltaTime;

            // raise sunlight
            m_sun.intensity += Time.deltaTime;

            //clear fog
            if (RenderSettings.fogDensity >= 0.01)
            {
                RenderSettings.fogDensity -= Time.deltaTime / 10;
            }
        }

    }
    /// <summary>
    /// Saves the first time events values from dictionary to arrays. Otherwise the SO will not save the values...
    /// </summary>
    public void SaveFirstTimeEvents()
    {
        // Saves the dictionary values as array.
        for (int i = 0; i < m_ftvSO.MyFirstTimeValues.Count; i++)
        {
            // Try to get the values first, just to be save.
            // Only need to save the bools, because the enums don't change.
            if (m_ftvSO.MyFirstTimeValues.TryGetValue(m_ftvSO.MyEFirstTimeValues[i], out bool value))
            {
                m_ftvSO.BoolForFTV[i] = value;
            }
        }
    }

    /// <summary>
    /// Loads the first time events values from arrays into dictionary. 
    /// </summary>
    private void LoadFirstTimeEvents()
    {
        // Load the dictionary with the arrays.
        for (int i = 0; i < m_ftvSO.MyEFirstTimeValues.Length; i++)
        {
            m_ftvSO.MyFirstTimeValues.Add(m_ftvSO.MyEFirstTimeValues[i], m_ftvSO.BoolForFTV[i]);
        }
    }

    /// <summary>
    /// Used to set player position on load
    /// </summary>
    public void FirstSpawn()
    {
        PlayerController.Instance.transform.position = m_spawn.position;
    }

    /// <summary>
    /// Check if there's an axe in Inventory
    /// </summary>
    /// <returns></returns>
    public bool AxeAvailable()
    {
        if (InventoryManager.Instance.GetItemAmount(ERESOURCETYPE.AXE) > 0)
        {
            return true;
        }
        return false;
    }
}
