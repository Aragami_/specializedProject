using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SavegameManager : MonoBehaviour
{

    #region Variables

    [Header("Objects to Save")]
    [Tooltip("List of building objects")]
    [SerializeField]
    private List<GameObject> m_buildings = new List<GameObject>();

    [Tooltip("List of resources placed on map (such as tree,bushed etc.)")]
    [SerializeField]
    private List<ItemScript> m_resources = new List<ItemScript>();

    [Header("Firstlogin")]
    [Tooltip("List of Objects which will be loaded on first login to ensure resources for newcomers")]
    [SerializeField]
    private List<GameObject> m_resourcePrefabs;

    #endregion

    #region Properties

    public static SavegameManager Instance { get; private set; }
    public List<GameObject> Buildings { get => m_buildings; }
    public List<GameObject> ResourcePrefabs { get => m_resourcePrefabs; }

    #endregion


    private void Awake()
    {
        if (Instance)
        {
            Destroy(this.gameObject);
            return;
        }
        Instance = this;
    }

    /// <summary>
    /// Saves current playerdata on database
    /// </summary>
    public void SaveplayerStats()
    {
        // get player transform info to store in database
        float xCoord = PlayerController.Instance.transform.position.x;
        float yCoord = PlayerController.Instance.transform.position.y;
        float zCoord = PlayerController.Instance.transform.position.z;
        int currentHealth = PlayerController.Instance.CurrentHealth;
        int currentHunger = PlayerController.Instance.CurrentHunger;
        int currentThirst = PlayerController.Instance.CurrentThirst;
        GameManager.Instance.DbHandler.SavePlayer(new Vector3(xCoord, yCoord, zCoord), currentHealth, currentHunger, currentThirst);

    }

    /// <summary>
    /// Save buildings the player built while gamesession
    /// </summary>
    public void SaveBuildings()
    {
        //get all building Objects in scene and save on Database
        Building[] tmp = FindObjectsOfType<Building>();
        GameManager.Instance.DbHandler.SaveBuildings(tmp.ToList());
    }


    /// <summary>
    /// Loads saved Playerdata from Database
    /// </summary>
    public void LoadplayerStats()
    {
        PlayerStats playerStats = GameManager.Instance.DbHandler.LoadPlayer();

        // set player position has to be read from database
        PlayerController.Instance.transform.position = new Vector3(playerStats.m_trans.x, playerStats.m_trans.y, playerStats.m_trans.z);
        PlayerController.Instance.SetPlayerStats(playerStats.m_currenthealth, playerStats.m_currenthunger, playerStats.m_currentthirst);
    }

    /// <summary>
    /// Save all resources planted on map while player's gamesession
    /// </summary>
    public void SaveResources()
    {
        // get all resources and save
        ItemScript[] resources = FindObjectsOfType<ItemScript>();
        GameManager.Instance.DbHandler.SaveResources(resources);
    }

    /// <summary>
    /// Activates resource common use on FirstLogin
    /// </summary>
    public void ActivateRes()
    {
        foreach (ItemScript tmp in m_resources)
        {
            tmp.gameObject.SetActive(true);
        }
    }
}

#region ----- Playerbelongings -----

/// <summary>
/// Playerdata class for storing data
/// </summary>
public class PlayerStats
{
    public Vector3 m_trans;
    public int m_currenthealth;
    public int m_currenthunger;
    public int m_currentthirst;

    // empty ctor
    public PlayerStats() { }

    /// <summary>
    /// Inits filled Playerstats class
    /// </summary>
    /// <param name="_trans">Transform as Vector3 (Position)</param>
    /// <param name="_currenthealth">Health amount of the player</param>
    /// <param name="_currenthunger">hunger amount of the player</param>
    /// <param name="_currentthirst">thirst amount of the player</param>
    public PlayerStats(Vector3 _trans, int _currenthealth, int _currenthunger, int _currentthirst)
    {
        m_trans = _trans;
        m_currenthealth = _currenthealth;
        m_currenthunger = _currenthunger;
        m_currentthirst = _currentthirst;
    }
}

#endregion