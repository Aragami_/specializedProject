using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    #region Variables

    [Header("Inventory Requirements")]
    [SerializeField]
    [Tooltip("Fill in slot amount")]
    private int m_maxSlots = 16;

    [SerializeField]
    [Tooltip("Inventory ID for Database")]
    private int m_inventoryID;

    [SerializeField]
    private UIImageContainer m_UIImageContainer;

    [Tooltip("Inventory")]
    private Inventory m_inventory = null;

    [Tooltip("Check if inventory is loaded")]
    private bool m_inventorySet = false;

    [Tooltip("Max stack size")]
    private int m_maxSlotSize = 255;

    [Tooltip("Min stack size")]
    private int m_minSlotSize = 0;

    [Tooltip("Inventory icon image list")]
    private Image[] m_inventoryIcons = new Image[16];



    [Header("Seeds")]
    [SerializeField]
    [Tooltip("Key to place Tree seed")]
    private List<ThrowableSeed> m_throwableSeeds;

    #endregion

    #region Properties

    public static InventoryManager Instance { get; set; }
    public bool InventorySet { get => m_inventorySet; set => m_inventorySet = value; }
    public int MaxSlots { get => m_maxSlots; set => m_maxSlots = value; }
    public Inventory Inventory { get => m_inventory; set => m_inventory = value; }
    public UIImageContainer UIImageContainer { get => m_UIImageContainer; set => m_UIImageContainer = value; }
    public List<ThrowableSeed> ThrowableSeeds { get => m_throwableSeeds; private set => m_throwableSeeds = value; }



    #endregion

    private void Awake()
    {
        //singleton
        if (Instance != null)
        {
            Destroy(this);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(this);
    }



    #region Save and Load

    /// <summary>
    /// Fills inventory class from Database
    /// </summary>
    public void LoadInventory()
    {

        m_inventory = GameManager.Instance.DbHandler.LoadInventory();
        if (m_inventory != null)
            m_inventorySet = true;


        if (GameManager.Instance.DbHandler.LoggedIn && m_inventorySet)
        {
            // set icon images filled in after loading
            if (m_UIImageContainer == null || m_inventoryIcons == null)
            {
                m_UIImageContainer = FindObjectOfType<UIImageContainer>();
                for (int i = 0; i < m_UIImageContainer.m_InventorySprites.Count; i++)
                {
                    m_inventoryIcons[i] = m_UIImageContainer.m_InventorySprites[i];
                }
            }
            // set icons to standard
            else if (m_inventoryIcons[0] == null)
            {
                for (int i = 0; i < m_UIImageContainer.m_InventorySprites.Count; i++)
                {
                    m_inventoryIcons[i] = m_UIImageContainer.m_InventorySprites[i];
                }
            }
            UpdateInventoryUI();
        }

    }

    /// <summary>
    /// Updates inventory icons and amount display
    /// </summary>
    public void UpdateInventoryUI()
    {
        if (GetItemAmount(ERESOURCETYPE.AXE) > 0)
            UpdateEqItemUI(true);
        else
            UpdateEqItemUI(false);

        if (m_UIImageContainer != null)
        {
            // iterate through inventory and update text boxes which show item amount
            for (int i = 0; i < m_UIImageContainer.m_InventorySprites.Count; i++)
            {
                m_inventoryIcons[i].sprite = m_inventory.Items[i].ItemID == 0 ? null : m_inventory.Items[i].Icon;
                m_UIImageContainer.m_InventoryAmounts[i].text = m_inventory.Items[i].ItemID == 0 ? "" : m_inventory.Items[i].Amount.ToString();
            }
        }
    }

    /// <summary>
    /// direction to dbhandler to save inventory
    /// </summary>
    public void SaveInventory()
    {
        if (m_inventorySet)
            GameManager.Instance.DbHandler.SaveInventory();
    }

    #endregion

    #region Itemmanagement

    #region AddItem

    /// <summary>
    /// Additem to inventory collection
    /// </summary>
    /// <param name="_id">Item id (int or enum)</param>
    /// <param name="_amount">amount you want to add</param>
    public void AddItem(int _id, int _amount)
    {

        for (int i = 0; i < m_inventory.Items.Length; i++)
        {
            if (_amount == 0)
                return;

            if (_id == m_inventory.Items[i].ItemID || m_inventory.Items[i].ItemID == 0)
            {
                // update slot if it's lower than max size and update UI
                if (m_inventory.Items[i].Amount + _amount < m_maxSlotSize)
                {
                    int tmp = m_inventory.Items[i].Amount;
                    m_inventory.Items[i] = new Item(GameManager.Instance.ItemList.GetItem(_id));
                    m_inventory.Items[i].Amount = _amount + tmp;
                    UpdateInventoryUI();
                    return;
                }

                // open new itemslot and add item to it
                else
                {
                    if (m_inventory.Items[i].ItemID == 0)
                        m_inventory.Items[i] = new Item(GameManager.Instance.ItemList.GetItem(_id));
                    int tmp = m_inventory.Items[i].Amount + _amount - m_maxSlotSize;
                    m_inventory.Items[i].Amount += _amount;
                    m_inventory.Items[i].Amount = Mathf.Clamp(m_inventory.Items[i].Amount, m_minSlotSize, m_maxSlotSize);
                    _amount = tmp;
                }
            }
        }
    }

    /// <summary>
    /// Additem to inventory collection
    /// </summary>
    /// <param name="_id">Item id (int or enum)</param>
    /// <param name="_amount">amount you want to add</param>
    public void AddItem(ERESOURCETYPE _id, int _amount)
    {
        for (int i = 0; i < m_inventory.Items.Length; i++)
        {
            if (_amount == 0)
                return;

            if ((int)_id == m_inventory.Items[i].ItemID || m_inventory.Items[i].ItemID == 0)
            {
                // update slot if it's lower than max size and update UI
                if (m_inventory.Items[i].Amount + _amount < m_maxSlotSize)
                {
                    int tmp = m_inventory.Items[i].Amount;
                    m_inventory.Items[i] = new Item(GameManager.Instance.ItemList.GetItem(_id));
                    m_inventory.Items[i].Amount = _amount + tmp;
                    UpdateInventoryUI();
                    return;
                }

                // open new itemslot and add item to it
                else
                {
                    if (m_inventory.Items[i].ItemID == 0)
                        m_inventory.Items[i] = new Item(GameManager.Instance.ItemList.GetItem(_id));
                    int tmp = m_inventory.Items[i].Amount + _amount - m_maxSlotSize;
                    m_inventory.Items[i].Amount += _amount;
                    m_inventory.Items[i].Amount = Mathf.Clamp(m_inventory.Items[i].Amount, m_minSlotSize, m_maxSlotSize);
                    _amount = tmp;
                }
            }
        }
    }

    // fast solution for eqItem symbol on top right corner
    private void UpdateEqItemUI(bool _icon)
    {
        if (_icon)
            UIManager.Instance.Eqitem.sprite = Resources.Load<Sprite>($"Sprites/Items/Icons/Axe");
        else
            UIManager.Instance.Eqitem.sprite = null;

    }

    #endregion

    #region SubtractItem

    /// <summary>
    /// Reduce item amount in inventory
    /// </summary>
    /// <param name="_id">Item id (int or enum)</param>
    /// <param name="_amount">amount you want to reduce</param>
    public void SubtractItem(int _id, int _amount)
    {

        // tell player there's no such item after inventory checked
        if (GetItemAmount(_id) < _amount)
        {
            GameManager.Instance.EventMessage("Not enough items of this type!");
            return;
        }

        for (int i = m_inventory.Items.Length - 1; i >= 0; i--)
        {
            // reduce inventory slot with given id and amount
            if (m_inventory.Items[i].ItemID == _id)
            {
                // if this inventoryslot's amount is lower set slot to null and go to next one
                if (m_inventory.Items[i].Amount < _amount)
                {
                    int tmp = m_inventory.Items[i].Amount - _amount;
                    tmp = Mathf.Abs(tmp);
                    m_inventory.Items[i].Amount -= _amount;
                    m_inventory.Items[i].Amount = Mathf.Clamp(m_inventory.Items[i].Amount, m_minSlotSize, m_maxSlotSize);
                    _amount = tmp;
                    m_inventory.Items[i] = new Item();
                }

                //if amount equals reset item slot to standard
                else if (m_inventory.Items[i].Amount == _amount)
                {

                    m_inventory.Items[i] = new Item();
                    break;
                }

                //subtract amount
                else
                {
                    m_inventory.Items[i].Amount -= _amount;
                    break;
                }
            }
        }
        UpdateInventoryUI();
    }

    /// <summary>
    /// Reduce item amount in inventory
    /// </summary>
    /// <param name="_id">Item id (int or enum)</param>
    /// <param name="_amount">amount you want to reduce</param>
    public void SubtractItem(ERESOURCETYPE _id, int _amount)
    {

        if (GetItemAmount((int)_id) < _amount)
        {
            GameManager.Instance.EventMessage("Not enough items of this type!");
            return;
        }

        for (int i = m_inventory.Items.Length - 1; i >= 0; i--)
        {

            // reduce inventory slot with given id and amount
            if (m_inventory.Items[i].ItemID == (int)_id)
            {

                // if this inventoryslot's amount is lower set slot to null and go to next one
                if (m_inventory.Items[i].Amount < _amount)
                {
                    int tmp = m_inventory.Items[i].Amount - _amount;
                    tmp = Mathf.Abs(tmp);
                    m_inventory.Items[i].Amount -= _amount;
                    m_inventory.Items[i].Amount = Mathf.Clamp(m_inventory.Items[i].Amount, m_minSlotSize, m_maxSlotSize);
                    _amount = tmp;
                    m_inventory.Items[i] = new Item();
                }

                //if amount equals reset item slot to standard
                else if (m_inventory.Items[i].Amount == _amount)
                {

                    m_inventory.Items[i] = new Item();
                    break;
                }

                //subtract amount
                else
                {
                    m_inventory.Items[i].Amount -= _amount;
                    break;
                }
            }
        }
        UpdateInventoryUI();
    }

    #endregion

    #endregion

    #region ----- GetItemAmount -----

    /// <summary>
    /// Check item Amount stored in Inventory
    /// </summary>
    /// <param name="_id">ItemID (int or enum)</param>
    /// <returns>Amount stored in Inventory at all</returns>
    public int GetItemAmount(int _id)
    {
        int amount = 0;
        for (int i = 0; i < m_inventory.Items.Length; i++)
        {
            if (m_inventory.Items[i].ItemID == _id)
            {
                amount += m_inventory.Items[i].Amount;
            }
        }
        return amount;
    }

    /// <summary>
    /// Check item Amount stored in Inventory
    /// </summary>
    /// <param name="_id">ItemID (int or enum)</param>
    /// <returns>Amount stored in Inventory at all</returns>
    public int GetItemAmount(ERESOURCETYPE _id)
    {
        int amount = 0;
        for (int i = 0; i < m_inventory.Items.Length; i++)
        {
            if (m_inventory.Items[i].ItemID == (int)_id)
            {
                amount += m_inventory.Items[i].Amount;
            }
        }
        return amount;
    }

    #endregion

    #region ----- obsolete -----

    /*
    /// <summary>
    /// Add items to inventory
    /// </summary>
    /// <param name="_type">Item type (Enum ENEWITEM)</param>
    /// <param name="_id">Item ID</param>
    /// <param name="_amount">Amount to add</param>
    public void Additem(ENEWITEM _type, int _id, int _amount)
    {
        switch (_type)
        {
            case ENEWITEM.NONE:
                return;

            case ENEWITEM.RESOURCE:
                for (int i = 0; i < m_inventory.SlotID.Length; i++)
                {
                    if (m_inventory.Amount[i] < m_maxSlotSize && ((m_inventory.ResourceID[i] == 0 || m_inventory.ResourceID[i] == _id) && m_inventory.ToolID[i] == 0 && m_inventory.ConsumableID[i] == 0))
                    {
                        if (m_inventory.Amount[i] < 1)
                            m_items++;

                        if (m_inventory.Amount[i] + _amount > m_maxSlotSize)
                        {
                            int tmp = m_inventory.Amount[i] + _amount - m_maxSlotSize;
                            m_inventory.Amount[i] += _amount;
                            m_inventory.Amount[i] = Mathf.Clamp(m_inventory.Amount[i], m_minSlotSize, m_maxSlotSize);
                            m_inventory.ResourceID[i] = _id;
                            Additem(_type, _id, tmp);
                            break;
                        }

                        else
                        {
                            m_inventory.Items[i].Amount += _amount;
                            m_inventory.ResourceID[i] = _id;
                            //Debug.Log(m_inventory.Amount[i]);
                            //Debug.Log(m_inventory.ResourceID[i]);
                            break;
                        }
                    }

                    else if (i == m_inventory.SlotID.Length - 1)
                        GameManager.Instance.EventMessage("Inventory is Full!");
                }
                break;

            case ENEWITEM.TOOL:
                for (int i = 0; i < m_inventory.SlotID.Length; i++)
                {
                    if (m_inventory.Amount[i] < 1 && m_inventory.ResourceID[i] == 0 && m_inventory.ToolID[i] == 0 && m_inventory.ConsumableID[i] == 0)
                    {
                        m_inventory.Amount[i]++;
                        m_inventory.ToolID[i] = _id;
                        m_items++;
                        break;
                    }

                    else if (i == m_inventory.SlotID.Length - 1)
                        GameManager.Instance.EventMessage("Inventory is Full!");
                }
                break;

            case ENEWITEM.CONSUMABLE:
                for (int i = 0; i < m_inventory.SlotID.Length; i++)
                {
                    if (m_inventory.Amount[i] < m_maxSlotSize && (m_inventory.ResourceID[i] == 0 && m_inventory.ToolID[i] == 0 && (m_inventory.ConsumableID[i] == _id || m_inventory.ConsumableID[i] == 0)))
                    {
                        if (m_inventory.Amount[i] < 1)
                            m_items++;

                        if (m_inventory.Amount[i] + _amount > m_maxSlotSize)
                        {
                            int tmp = m_inventory.Amount[i] + _amount - m_maxSlotSize;
                            m_inventory.Amount[i] += _amount;
                            m_inventory.Amount[i] = Mathf.Clamp(m_inventory.Amount[i], m_minSlotSize, m_maxSlotSize);
                            m_inventory.ConsumableID[i] = _id;
                            Additem(_type, _id, tmp);
                            break;
                        }

                        else
                        {
                            m_inventory.Amount[i] += _amount;
                            m_inventory.ConsumableID[i] = _id;
                            break;
                        }

                    }

                    else if (i == m_inventory.SlotID.Length - 1)
                        GameManager.Instance.EventMessage("Inventory is Full!");
                }
                break;
        }
        //UpdateInventoryUI();
    }
     
        /// <summary>
        /// Subtract Items from Inventory
        /// </summary>
        /// <param name="_type">Item type (Enum ENEWITEM)</param>
        /// <param name="_id">Item ID</param>
        /// <param name="_amount">Amount to subtract</param>
        public void SubtractItem(ENEWITEM _type, int _id, int _amount)
        {

            if (_amount == 0)
                return;
            if (ItemAmount(_type, _id) < _amount)
            {
                GameManager.Instance.EventMessage("Not enough items of this type!");
                return;
            }

            switch (_type)
            {
                case ENEWITEM.NONE:
                    return;

                case ENEWITEM.RESOURCE:
                    for (int i = 0; i < m_inventory.SlotID.Length; i++)
                    {
                        if (m_inventory.ResourceID[i] == _id)
                        {
                            if (m_inventory.Amount[i] - _amount < 0)
                            {
                                int tmp = m_inventory.Amount[i] - _amount;
                                m_inventory.Amount[i] -= _amount;
                                m_inventory.Amount[i] = Mathf.Clamp(m_inventory.Amount[i], m_minSlotSize, m_maxSlotSize);
                                m_items--;
                                m_inventory.ResourceID[i] = 0;
                                SubtractItem(_type, _id, Mathf.Abs(tmp));
                                UpdateInventoryUI();
                                return;
                            }
                            else
                            {
                                m_inventory.Amount[i] -= _amount;
                                if (m_inventory.Amount[i] == 0)
                                {
                                    m_items--;
                                    m_inventory.ResourceID[i] = 0;
                                    UpdateInventoryUI();
                                    return;
                                }
                                break;
                            }

                        }
                    }
                    break;

                case ENEWITEM.TOOL:
                    for (int i = 0; i < m_inventory.SlotID.Length; i++)
                    {
                        if (m_inventory.ToolID[i] == _id)
                        {
                            m_inventory.Amount[i]--;
                            m_inventory.ToolID[i] = _id;
                            m_items--;
                            UpdateInventoryUI();
                            break;
                        }
                    }
                    break;

                case ENEWITEM.CONSUMABLE:
                    for (int i = 0; i < m_inventory.SlotID.Length; i++)
                    {
                        if (m_inventory.ConsumableID[i] == _id)
                        {
                            if (m_inventory.Amount[i] - _amount < 0)
                            {
                                int tmp = m_inventory.Amount[i] - _amount;
                                m_inventory.Amount[i] -= _amount;
                                m_inventory.Amount[i] = Mathf.Clamp(m_inventory.Amount[i], m_minSlotSize, m_maxSlotSize);
                                m_items--;
                                m_inventory.ConsumableID[i] = 0;
                                SubtractItem(_type, _id, Mathf.Abs(tmp));
                                UpdateInventoryUI();
                                return;
                            }

                            else
                            {
                                m_inventory.Amount[i] -= _amount;
                                if (m_inventory.Amount[i] == 0)
                                {
                                    m_items--;
                                    m_inventory.ConsumableID[i] = 0;
                                    UpdateInventoryUI();
                                    return;
                                }
                                break;
                            }
                        }
                    }
                    break;
            }
            UpdateInventoryUI();
        }

    /// <summary>
    /// How many items of this type exists in inventory
    /// </summary>
    /// <param name="_type">Item type (Enum ENEWITEM)</param>
    /// <param name="_id">Item ID</param>
    /// <returns>Amount of this item type in inventory</returns>
    public int GetItemAmount(ENEWITEM _type, int _id)
    {
        int amount = 0;
        switch (_type)
        {
            case ENEWITEM.NONE:
                return amount;

            case ENEWITEM.RESOURCE:
                {
                    for (int i = 0; i < m_inventory.SlotID.Length; i++)
                    {
                        if (m_inventory.ResourceID[i] == _id)
                        {
                            amount += m_inventory.Amount[i];
                        }
                    }
                }
                break;

            case ENEWITEM.TOOL:
                for (int i = 0; i < m_inventory.SlotID.Length; i++)
                {
                    if (m_inventory.ToolID[i] == _id)
                    {
                        amount += m_inventory.Amount[i];
                    }
                }
                break;

            case ENEWITEM.CONSUMABLE:
                for (int i = 0; i < m_inventory.SlotID.Length; i++)
                {
                    if (m_inventory.ConsumableID[i] == _id)
                    {
                        amount += m_inventory.Amount[i];
                    }
                }
                break;

        }

        return amount;
    }
    */

    #endregion

    /// <summary>
    /// saves inventory and playerstats on appquit
    /// </summary>
    private void OnApplicationQuit()
    {
        GameManager.Instance.DbHandler.SaveInventory();
        SavegameManager.Instance.SaveplayerStats();
        SavegameManager.Instance.SaveBuildings();
        SavegameManager.Instance.SaveResources();
        GameManager.Instance.SaveFirstTimeEvents();
    }
}
