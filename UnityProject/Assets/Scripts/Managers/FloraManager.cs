using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-100)]
public class FloraManager : MonoBehaviour
{
    public static FloraManager Instance { get; private set; }

    private readonly List<IEdible> m_edibles = new List<IEdible>();
    // ReSharper disable once IdentifierTypo
    private readonly List<IDrinkable> m_drinkables = new List<IDrinkable>();

    private void Awake()
    {
        if (Instance)
        {
            Destroy(this.gameObject);
            return;
        }

        Instance = this;
    }

    /// <summary>
    /// Find the closest Edible
    /// </summary>
    /// <param name="_self">Own position</param>
    /// <returns>Closest Edible to self</returns>
    public IEdible GetClosestEdible(Vector3 _self)
    {
        IEdible bestEdible = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = _self;
        foreach (IEdible potentialEdible in m_edibles)
        {
            Vector3 directionToTarget = (potentialEdible).transform.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestEdible = potentialEdible;
            }
        }

        return bestEdible;
    }

    /// <summary>
    /// Find the closest Drinkable
    /// </summary>
    /// <param name="_self">Own position</param>
    /// <returns>Closest Drinkable to self</returns>
    public IDrinkable GetClosestDrinkable(Vector3 _self)
    {
        IDrinkable bestDrinkable = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = _self;
        foreach (IDrinkable potentialDrinkable in m_drinkables)
        {
            Vector3 directionToTarget = (potentialDrinkable).transform.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestDrinkable = potentialDrinkable;
            }
        }

        return bestDrinkable;
    }

    /// <summary>
    /// Adds a new Edible to all available edibles (which must be of type MonoBehaviour)
    /// </summary>
    /// <param name="_newEdible">Edible to add</param>
    public void AddEdible(IEdible _newEdible)
    {
        if (_newEdible.GetType().IsSubclassOf(typeof(MonoBehaviour)))
        {
            m_edibles.Add(_newEdible);
        }
#if UNITY_EDITOR
        else
        {
            Debug.LogWarning("Tried to add edible which isn't of type MonoBehaviour. Skipped!");
        }
#endif
    }

    // ReSharper disable once CommentTypo
    /// <summary>
    /// Adds a new Drinkable to all available drinkables (which must be of type MonoBehaviour)
    /// </summary>
    /// <param name="_newDrinkable">Drinkable to add</param>
    public void AddDrinkable(IDrinkable _newDrinkable)
    {
        if (_newDrinkable.GetType().IsSubclassOf(typeof(MonoBehaviour)))
        {
            m_drinkables.Add(_newDrinkable);
        }
#if UNITY_EDITOR
        else
        {
            Debug.LogWarning("Tried to add drinkable which isn't of type MonoBehaviour. Skipped!");
        }
#endif
    }

    /// <summary>
    /// Removes the edible from the list of globally available edibles
    /// </summary>
    /// <param name="_removeEdible">Edible to remove</param>
    public void RemoveEdible(IEdible _removeEdible)
    {
        m_edibles.Remove(_removeEdible);
    }

    /// <summary>
    /// Removes the drinkable from the list of globally available drinkable
    /// </summary>
    /// <param name="_removeDrinkable">Drinkable to remove</param>
    public void RemoveDrinkable(IDrinkable _removeDrinkable)
    {
        m_drinkables.Remove(_removeDrinkable);
    }
}
