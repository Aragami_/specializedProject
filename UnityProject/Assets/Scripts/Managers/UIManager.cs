using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    #region Variables

    [Header("Sliders")]

    [Tooltip("Reference healthbar")]
    [SerializeField]
    private Slider m_healthBar;
    [Tooltip("Reference hungerbar")]
    [SerializeField]
    private Slider m_hungerBar;
    [Tooltip("Reference thirstbar")]
    [SerializeField]
    private Slider m_thirstBar;

    [Header("TextFields")]
    [Tooltip("TMP Health value")]
    [SerializeField]
    private TMP_Text m_healthValue;

    [Tooltip("TMP Hunger value")]
    [SerializeField]
    private TMP_Text m_hungerValue;

    [Tooltip("TMP Thirst value")]
    [SerializeField]
    private TMP_Text m_thirstValue;

    [Header("Inputfields")]

    [Tooltip("Inputfield of username")]
    [SerializeField]
    private TMP_InputField m_username;

    [Tooltip("Inputfield of password")]
    [SerializeField]
    private TMP_InputField m_password;

    [Header("GameObjects")]

    [SerializeField]
    [Tooltip("LoginOverlay - Canvas")]
    private GameObject m_loginCanvas;

    [SerializeField]
    [Tooltip("Delete UI for Item Slot")]
    private GameObject m_itemOptionsUI;

    [SerializeField]
    [Tooltip("Equipped item image")]
    private Image m_eqitem;

    [SerializeField]
    [Tooltip("Inventory UI.")]
    private GameObject m_inventoryUI;


    [Tooltip("Is the Audio UI open?")]
    private bool m_openedAudioUI = false;

    [Tooltip("Is any UI open?")]
    private bool m_openedUI = false;

    [Tooltip("Optionspanel")]
    private GameObject m_optionsPanel;

    [Tooltip("Audiosettings")]
    private GameObject m_audioSettingsPanel;

    [Tooltip("Button to check which itemslot and how to handle")]
    private Button m_activeButton;

    #endregion

    #region Properties

    public static UIManager Instance { get; private set; }

    public bool OpenedUI { get => m_openedUI; set => m_openedUI = value; }

    public bool OpenedAudioUI { get => m_openedAudioUI; set => m_openedAudioUI = value; }

    public GameObject InventoryUI { get => m_inventoryUI; set => m_inventoryUI = value; }
    public Image Eqitem { get => m_eqitem; set => m_eqitem = value; }

    #endregion

    private void Awake()
    {
        if (m_loginCanvas == null)
            m_loginCanvas = GameObject.FindObjectOfType<LoginHandler>().gameObject;

        if (Instance)
        {
            Destroy(this);
            return;
        }
        Instance = this;

        // get healthbar text components
        m_healthValue = m_healthBar.GetComponentInChildren<TMP_Text>();
        m_hungerValue = m_hungerBar.GetComponentInChildren<TMP_Text>();
        m_thirstValue = m_thirstBar.GetComponentInChildren<TMP_Text>();

        //activate player parameters and deactivate options and audio panel
        ActivateLifeParameterUI(false);
        m_optionsPanel = FindObjectOfType<OptionsUI>().gameObject;
        m_optionsPanel.SetActive(false);
        m_audioSettingsPanel = FindObjectOfType<AudioUI>().gameObject;
        m_audioSettingsPanel.SetActive(false);
    }

    private void Update()
    {

        LoginPanelNavigation();

        ActivateUI();

        FastLogin();
    }

    /// <summary>
    /// Show OptionsUI
    /// </summary>
    public void ActivateUI()
    {
        if (GameManager.Instance.DbHandler.LoggedIn)
        {
            if (Input.GetKeyDown(KeyCode.F1) && !m_openedAudioUI)
            {
                m_openedUI = !m_optionsPanel.activeSelf;
                ShowOptionsUI();
            }
            ShowInventory();
        }
    }

    /// <summary>
    /// Admin login
    /// </summary>
    public void FastLogin()
    {
        // Disable LoginScreen and default login with admin account 
        //TODO: gotta be deleted later on (dev version only)
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (m_loginCanvas.activeSelf)
            {
                // Developer Cheat button for login
                GameManager.Instance.DbHandler.LoginUser("admin", "admin");
                m_loginCanvas.SetActive(false);
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
    }

    /// <summary>
    /// Fast navigation in Loginpanel
    /// </summary>
    public void LoginPanelNavigation()
    {
        if (!GameManager.Instance.DbHandler.LoggedIn)
        {
            // possibility to jump to password line
            if (m_username.isFocused)
            {
                if (Input.GetKeyDown(KeyCode.Tab))
                {
                    m_password.ActivateInputField();
                    EventSystem.current.SetSelectedGameObject(m_password.gameObject, null);

                    m_password.OnPointerClick(new PointerEventData(EventSystem.current));
                }
            }

            // possibility to pass login data with return button
            else if (!m_username.isFocused && m_password.IsActive())
            {
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    LoginHandler.Instance.OnClick_Login(m_username, m_password);
                }
            }
        }
    }

    /// <summary>
    /// Update player parameter UI
    /// </summary>
    /// <param name="_health">Health value</param>
    /// <param name="_hunger">Hunger value</param>
    /// <param name="_thirst">Thirst value</param>
    public void UpdateHealthParameterUI(int _health, int _hunger, int _thirst)
    {
        m_healthBar.value = (float)PlayerController.Instance.CurrentHealth / (float)PlayerController.Instance.MaxHealth;
        m_hungerBar.value = (float)PlayerController.Instance.CurrentHunger / (float)PlayerController.Instance.MaxHunger;
        m_thirstBar.value = (float)PlayerController.Instance.CurrentThirst / (float)PlayerController.Instance.MaxThirst;
        m_healthValue.text = $"{(float)PlayerController.Instance.CurrentHealth} / {(float)PlayerController.Instance.MaxHealth}";
        m_hungerValue.text = $"{(float)PlayerController.Instance.CurrentHunger} / {(float)PlayerController.Instance.MaxHunger}";
        m_thirstValue.text = $"{(float)PlayerController.Instance.CurrentThirst} / {(float)PlayerController.Instance.MaxThirst}";
    }

    /// <summary>
    /// Activate parameter UI
    /// </summary>
    /// <param name="_bool">show?</param>
    public void ActivateLifeParameterUI(bool _bool)
    {
        m_healthBar.gameObject.SetActive(_bool);
        m_hungerBar.gameObject.SetActive(_bool);
        m_thirstBar.gameObject.SetActive(_bool);
    }

    /// <summary>
    /// Hides Loginmask and locks cursor
    /// </summary>
    public void DisableLoginUI()
    {
        if (m_loginCanvas)
        {
            m_loginCanvas.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    /// <summary>
    /// Activate Options UI and lock/unlock cursor
    /// </summary>
    public void ShowOptionsUI()
    {
        m_inventoryUI.SetActive(false);
        m_itemOptionsUI.SetActive(false);
        bool activeUI = m_optionsPanel.activeSelf;
        m_optionsPanel.SetActive(!activeUI);

        if (activeUI)
            Cursor.lockState = CursorLockMode.Locked;

        else
            Cursor.lockState = CursorLockMode.None;

        Cursor.visible = !activeUI;
    }

    /// <summary>
    ///  Activate Audio UI and lock/unlock cursor
    /// </summary>
    public void ShowAudioUI()
    {
        m_openedAudioUI = true;
        bool activeUI = m_audioSettingsPanel.activeSelf;
        m_audioSettingsPanel.SetActive(!activeUI);

        if (activeUI)
            Cursor.lockState = CursorLockMode.Locked;

        else
            Cursor.lockState = CursorLockMode.None;

        Cursor.visible = !activeUI;
        m_openedUI = true;
    }

    /// <summary>
    /// Save the whole game
    /// </summary>
    public void OnClick_SaveGame()
    {
        GameManager.Instance.DbHandler.SaveInventory();
        SavegameManager.Instance.SaveplayerStats();
        SavegameManager.Instance.SaveBuildings();
        SavegameManager.Instance.SaveResources();
    }

    /// <summary>
    /// Play Buttonsound on select
    /// </summary>
    /// <param name="_button"></param>
    public void OnClick_ButtonSound(Button _button)
    {
        try
        {
            FMODUnity.StudioEventEmitter buttonEvent = _button.GetComponent<FMODUnity.StudioEventEmitter>();
            SoundManager.Instance.PlaySoundEffect(PlayerController.Instance.gameObject, buttonEvent);
        }
        catch (Exception _e)
        {
            Debug.Log("Reference or script is missing on Button!" + _e);
        }
    }

    /// <summary>
    /// Hide Options UI and lock Cursor
    /// </summary>
    public void OnClick_HideOptionsUI()
    {
        m_optionsPanel.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        m_openedUI = false;
    }

    /// <summary>
    /// hide AudioSettings and lock cursor
    /// </summary>
    public void OnClick_HideAudioSettingsUI()
    {
        m_openedAudioUI = false;
        m_audioSettingsPanel.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        m_openedUI = m_optionsPanel.activeSelf;

        m_openedUI = true;
    }

    /// <summary>
    /// Quit Game
    /// </summary>
    public void OnClick_QuitGame()
    {
#if UNITY_EDITOR

        UnityEditor.EditorApplication.isPlaying = false;
#else

Application.Quit();

#endif
    }

    /// <summary>
    /// Check Logindata
    /// </summary>
    public void OnClick_Login()
    {
        GameManager.Instance.DbHandler.LoginUser(m_username.text, m_password.text);
    }

    /// <summary>
    /// Active Itemslot Panel for further Itemoptions
    /// </summary>
    /// <param name="_button">ItemSlot</param>
    /// <param name="_open">Open UI?</param>
    public void ItemOptionsPanel(Button _button, bool _open)
    {
        m_activeButton = _button;
        for (int i = 0; i < InventoryManager.Instance.Inventory.SlotID.Length; i++)
        {
            if (InventoryManager.Instance.UIImageContainer.m_InventoryButtons[i].Equals(m_activeButton))
            {
                if (InventoryManager.Instance.Inventory.Items[i].Amount > 0)
                {
                    m_itemOptionsUI.transform.position = Input.mousePosition;
                    m_itemOptionsUI.gameObject.SetActive(_open);
                }
            }
        }
    }

    /// <summary>
    /// Delete Item in Inventory Slot
    /// </summary>
    public void DeleteItemSlot()
    {
        for (int i = 0; i < InventoryManager.Instance.Inventory.SlotID.Length; i++)
        {
            if (InventoryManager.Instance.UIImageContainer.m_InventoryButtons[i].Equals(m_activeButton))
            {
                InventoryManager.Instance.Inventory.Items[i] = new Item();
                InventoryManager.Instance.UpdateInventoryUI();
                m_itemOptionsUI.gameObject.SetActive(false);
            }
        }
    }

    /// <summary>
    /// Call UseFunction of item in Slot
    /// </summary>
    public void UseItemSlot()
    {
        for (int i = 0; i < InventoryManager.Instance.Inventory.SlotID.Length; i++)
        {
            if (InventoryManager.Instance.UIImageContainer.m_InventoryButtons[i].Equals(m_activeButton))
            {
                InventoryManager.Instance.Inventory.Items[i].Use(InventoryManager.Instance.Inventory.Items[i].ItemID);
                if (InventoryManager.Instance.Inventory.Items[i].ItemID == 4 || InventoryManager.Instance.Inventory.Items[i].ItemID == 101 || InventoryManager.Instance.Inventory.Items[i].ItemID == 102)
                    InventoryManager.Instance.Inventory.Items[i].Amount -= 1;

                if (InventoryManager.Instance.Inventory.Items[i].Amount == 0)
                {
                    InventoryManager.Instance.Inventory.Items[i] = new Item();
                }

                InventoryManager.Instance.UpdateInventoryUI();
                m_itemOptionsUI.gameObject.SetActive(false);
            }
        }
    }

    /// <summary>
    /// call Consume function of item in slot
    /// </summary>
    public void ConsumeItemSlot()
    {
        for (int i = 0; i < InventoryManager.Instance.Inventory.SlotID.Length; i++)
        {
            if (InventoryManager.Instance.UIImageContainer.m_InventoryButtons[i].Equals(m_activeButton))
            {
                InventoryManager.Instance.Inventory.Items[i].Consume();
                InventoryManager.Instance.UpdateInventoryUI();
                m_itemOptionsUI.gameObject.SetActive(false);
            }
        }
    }

    /// <summary>
    /// Close itemoptions panel
    /// </summary>
    public void CloseItemOptionsPanel()
    {
        m_itemOptionsUI.gameObject.SetActive(false);
    }

    /// <summary>
    /// show inventory panel, update cursor mode and player rotation
    /// </summary>
    public void ShowInventory()
    {
        if (DeveloperConsoleBehaviour.Instance.IsConsoleActive)
        {
            if (m_inventoryUI.activeSelf)
                m_inventoryUI.SetActive(false);
            return;
        }

        if (Input.GetKeyDown(KeyCode.I) && !m_openedUI && !m_openedAudioUI)
        {
            bool showInventory = !m_inventoryUI.activeSelf;
            m_inventoryUI.SetActive(showInventory);
            if (!showInventory)
            {
                PlayerController.Instance.CanRotate = true;
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                UIManager.Instance.CloseItemOptionsPanel();
            }

            else if (showInventory)
            {
                PlayerController.Instance.CanRotate = false;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }
    }
}
