using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.BehaviourTree;
using UnityEngine;

namespace Assets.Scripts.BehaviourTree
{
    /// <summary>
    /// Behaviour Tree of the Animal
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Animal))]
    public abstract class BtTree : MonoBehaviour
    {
        /// <summary>
        /// Head node of the behaviour tree
        /// </summary>
        protected BtNode m_topNode;
        /// <summary>
        /// Animal this behaviour tree is made for
        /// </summary>
        protected Animal m_myAnimal;

        /// <summary>
        /// Evaluates the state of the current tree using its nodes
        /// </summary>
        /// <returns>Success state of the entire tree</returns>
        public NodeState Evaluate()
        {
            return m_topNode.Evaluate();
        }

        /// <summary>
        /// Creates the behaviour tree, including the top node
        /// </summary>
        /// <param name="_thisAnimal">Animal, this behaviour tree is using</param>
        public abstract void ConstructBehaviourTree(Animal _thisAnimal);
    }
}
