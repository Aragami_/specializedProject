using UnityEngine;
using System;
using System.Collections.Generic;

namespace Assets.Scripts.BehaviourTree
{
    /// <summary>
    /// Sequence node, which will only execute the next node if the previous one returned success
    /// </summary>
    public class BtSequence : BtNode
    {
        protected List<BtNode> m_nodes = new List<BtNode>();

        /// <summary>
        /// Sequence node, which will only execute the next node if the previous one returned success
        /// </summary>
        /// <param name="_nodes">List of child nodes (Order matters)</param>
        public BtSequence(List<BtNode> _nodes)
        {
            this.m_nodes = _nodes;
        }

        public override NodeState Evaluate()
        {
            foreach (BtNode node in m_nodes)
            {
                switch (node.Evaluate())
                {
                    case NodeState.RUNNING:
                        m_nodeState = NodeState.RUNNING;
                        return NodeState.RUNNING;
                    case NodeState.SUCCESS:
                        m_nodeState = NodeState.SUCCESS;
                        break;
                    case NodeState.FAILURE:
                        m_nodeState = NodeState.FAILURE;
                        return m_nodeState;
                    default:
                        break;
                }
            }

            return m_nodeState;
            throw new NullReferenceException("Sequence has no nodes");
        }
    }
}
