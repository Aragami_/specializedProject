using UnityEngine;
using System;
using System.Collections.Generic;

namespace Assets.Scripts.BehaviourTree
{
    /// <summary>
    /// Inverts the State of the child node (if the child is RUNNING, it also returns RUNNING)
    /// </summary>
    public class BtInverter : BtNode
    {
        protected BtNode m_node;

        /// <summary>
        /// Inverts the State of the child node (if the child is RUNNING, it also returns RUNNING)
        /// </summary>
        /// <param name="_node">Child node</param>
        public BtInverter(BtNode _node)
        {
            this.m_node = _node;
        }

        public override NodeState Evaluate()
        {
            switch (m_node.Evaluate())
            {
                case NodeState.RUNNING:
                    m_nodeState = NodeState.RUNNING;
                    break;
                case NodeState.SUCCESS:
                    m_nodeState = NodeState.FAILURE;
                    break;
                case NodeState.FAILURE:
                    m_nodeState = NodeState.SUCCESS;
                    break;
                default:
                    break;
            }

            return m_nodeState;
        }
    }
}
