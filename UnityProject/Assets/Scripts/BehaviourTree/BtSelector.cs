using UnityEngine;
using System;
using System.Collections.Generic;

namespace Assets.Scripts.BehaviourTree
{
    /// <summary>
    /// Selector node, which will execute all nodes in order until one of them returns success and then skips the others
    /// </summary>
    public class BtSelector : BtNode
    {
        protected List<BtNode> m_nodes = new List<BtNode>();

        /// <summary>
        /// Selector node, which will execute all nodes in order until one of them returns success and then skips the others
        /// </summary>
        /// <param name="_nodes">List of child nodes (Order matters)</param>
        public BtSelector(List<BtNode> _nodes)
        {
            this.m_nodes = _nodes;
        }

        public override NodeState Evaluate()
        {
            foreach (BtNode node in m_nodes)
            {
                switch (node.Evaluate())
                {
                    case NodeState.RUNNING:
                        m_nodeState = NodeState.RUNNING;
                        return m_nodeState;
                    case NodeState.SUCCESS:
                        m_nodeState = NodeState.SUCCESS;
                        return m_nodeState;
                    case NodeState.FAILURE:
                        break;
                    default:
                        break;
                }
            }

            m_nodeState = NodeState.FAILURE;
            return m_nodeState;
        }
    }
}
