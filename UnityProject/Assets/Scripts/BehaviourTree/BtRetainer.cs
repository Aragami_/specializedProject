using UnityEngine;
using System;
using System.Collections.Generic;

namespace Assets.Scripts.BehaviourTree
{
    /// <summary>
    /// Retains the child node state (has no functionality, only used for tree overview)
    /// </summary>
    public class BtRetainer : BtNode
    {
        protected BtNode m_node;

        /// <summary>
        /// Retains the child node state (has no functionality, only used for tree overview)
        /// </summary>
        /// <param name="_node">Child node</param>
        public BtRetainer(BtNode _node)
        {
            this.m_node = _node;
        }

        public override NodeState Evaluate()
        {
            m_nodeState = m_node.Evaluate();

            return m_nodeState;
        }
    }
}
