using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.BehaviourTree
{
    /// <summary>
    /// Random node, which will evaluate a random child node and return its state
    /// </summary>
    public class BtRandom : BtNode
    {
        protected Dictionary<float, BtNode> m_weightedNodes = new Dictionary<float, BtNode>();

        /// <summary>
        /// Random node, which will execute a random child node
        /// </summary>
        /// <param name="_nodes">List of child nodes (all having the same occurrence rate)</param>
        public BtRandom(List<BtNode> _nodes)
        {
            List<float> range = new List<float>();
            range.Add(0f);
            for (int i = 1; i < _nodes.Count; i++)
            {
                range.Add((float)i/_nodes.Count);
            }
        }

        /// <summary>
        /// Random node, which will execute a random child node
        /// </summary>
        /// <param name="_weightedNodes">List of child nodes. Order needs to match with range counting upwards</param>
        /// <param name="_range">Splits the range between 0 and 1 in COUNT parts. The larger the range the more likely for the node to be selected. Only the range ending is needed. Has to be sorted</param>
        public BtRandom(List<BtNode> _weightedNodes, List<float> _range)
        {
            if (_weightedNodes.Count != _range.Count) throw new IndexOutOfRangeException("WeightedNodes and Range do not have the same count");
            Dictionary<float, BtNode> weightedNotes = new Dictionary<float, BtNode>();
            for (var i = 0; i < _weightedNodes.Count-1; i++)
            {
                weightedNotes.Add(_range[i], _weightedNodes[i]);
            }
            weightedNotes.Add(1, _weightedNodes[_weightedNodes.Count-1]); // Last one is always 1

            this.m_weightedNodes = weightedNotes;
        }

        public override NodeState Evaluate()
        {
            float rnd = Random.Range(0f, 1f);
            foreach (KeyValuePair<float, BtNode> pair in m_weightedNodes)
            {
                if (pair.Key > rnd)
                {
                    return pair.Value.Evaluate();
                }
            }
            throw new IndexOutOfRangeException("No range or nodes");
        }
    }
}
