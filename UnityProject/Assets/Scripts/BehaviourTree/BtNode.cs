using UnityEngine;
using System;

namespace Assets.Scripts.BehaviourTree
{
    /// <summary>
    /// State of the Node
    /// </summary>
    public enum NodeState
    {
        /// <summary>
        /// RUNNING = This Node is currently executing
        /// </summary>
        RUNNING,
        /// <summary>
        /// SUCCESS = This Node returned a success
        /// </summary>
        SUCCESS,
        /// <summary>
        /// FAILURE = This Node returned a failure
        /// </summary>
        FAILURE
    }

    /// <summary>
    /// Base Node used for creating new Nodes
    /// </summary>
    public abstract class BtNode
    {
        /// <summary>
        /// State of the current Node
        /// </summary>
        protected NodeState m_nodeState;
        /// <summary>
        /// Returns the last evaluated state of the node
        /// </summary>
        public NodeState NodeState => m_nodeState;

        /// <summary>
        /// Evaluates the node
        /// </summary>
        /// <returns>Evaluated State of the Node</returns>
        public abstract NodeState Evaluate();
    }
}
