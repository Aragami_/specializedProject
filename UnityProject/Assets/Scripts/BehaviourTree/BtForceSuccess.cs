using UnityEngine;
using System;
using System.Collections.Generic;

namespace Assets.Scripts.BehaviourTree
{
    /// <summary>
    /// This node always returns SUCCESS, no matter if the child was successful, unless it is RUNNING which always returns RUNNING
    /// </summary>
    public class BtForceSuccess : BtNode
    {
        protected BtNode m_node;

        /// <summary>
        /// This node always returns SUCCESS, no matter if the child was successful, unless it is RUNNING which always returns RUNNING
        /// </summary>
        /// <param name="_node">Child node</param>
        public BtForceSuccess(BtNode _node)
        {
            this.m_node = _node;
        }

        public override NodeState Evaluate()
        {
            switch (m_node.Evaluate())
            {
                case NodeState.RUNNING:
                    m_nodeState = NodeState.RUNNING;
                    break;
                case NodeState.SUCCESS:
                case NodeState.FAILURE:
                    m_nodeState = NodeState.SUCCESS;
                    break;
                default:
                    break;
            }

            return m_nodeState;
        }
    }
}
