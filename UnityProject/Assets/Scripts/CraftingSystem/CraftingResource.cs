using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CraftingResource
{
    [SerializeField]
    [Tooltip("The item type.")]
    private ERESOURCETYPE m_itemType = ERESOURCETYPE.NONE;

    [SerializeField]
    [Tooltip("The amount of this resource needed.")]
    private int m_neededAmount = 0;

    public ERESOURCETYPE ItemType
    {
        get => m_itemType;
    }

    public int NeededAmount
    {
        get => m_neededAmount;
    }

    public CraftingResource(ERESOURCETYPE _type, int _amount)
    {
        m_itemType = _type;
        m_neededAmount = _amount;
    }
}
