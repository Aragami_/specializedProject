using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraftingSystem : MonoBehaviour
{
    public static CraftingSystem Instance = null;

    #region Variable
    [SerializeField]
    [Tooltip("An array with every crafting recipe. Add/Remove recipe in Editor.")]
    private CraftingRecipe[] m_craftingRecipes = null;

    [SerializeField]
    [Tooltip("The panel for the crafting ui.")]
    private GameObject m_craftingPanel = null;

    [SerializeField]
    [Tooltip("The button prefab to use.")]
    private GameObject m_buttonPrefab = null;

    [SerializeField]
    [Tooltip("The parent for the buttons.")]
    private Transform m_buttonParent = null;

    [Tooltip("A list of all buttons for the crafting UI.")]
    private List<Button> m_craftingButtons = new List<Button>();

    [Tooltip("Is the crafting panel active? Default = false.")]
    private bool m_craftingPanelActive = false;

    public CraftingRecipe[] CraftingRecipes
    {
        get => m_craftingRecipes;
    }

    public bool CraftingPanelActive
    {
        get => m_craftingPanelActive;
    }
    #endregion

    private void Awake()
    {
        if (Instance)
        {
            Destroy(this.gameObject);
            return;
        }

        Instance = this;

        m_craftingPanel.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        GenerateButtons();
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.Instance.DbHandler.LoggedIn)
        {
            return;
        }

        if (!DeveloperConsoleBehaviour.Instance.IsConsoleActive)
        {
            if (Input.GetKeyDown(KeyCode.C))
            {
                ToggleCraftingUI();
            }
            else if (Input.GetKeyDown(KeyCode.Escape))
            {
                CloseCraftingUI();
            }
        }
    }

    /// <summary>
    /// Toggle the crafting UI with "C".
    /// </summary>
    public void ToggleCraftingUI()
    {
        if (CraftingPanelActive)
        {
            CloseCraftingUI();
        }
        else
        {
            OpenCraftingUI();
        }
    }

    /// <summary>
    /// Close the crafting UI.
    /// </summary>
    public void CloseCraftingUI()
    {
        m_craftingPanel.SetActive(false);
        m_craftingPanelActive = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        PlayerController.Instance.CanRotate = true;
    }

    /// <summary>
    /// Open the crafting UI.
    /// </summary>
    private void OpenCraftingUI()
    {
        m_craftingPanel.SetActive(true);
        m_craftingPanelActive = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        PlayerController.Instance.CanRotate = false;
    }

    /// <summary>
    /// Generates buttons.
    /// </summary>
    public void GenerateButtons()
    {
        // The amount of needed buttons.
        int buttonAmount = m_craftingRecipes.Length;

        // Creating new buttons with the prefab, which is just a empty button.
        for (int i = 0; i < buttonAmount; i++)
        {
            // Set this object as the parent of the button.
            Button button = Instantiate(m_buttonPrefab, m_buttonParent).GetComponent<Button>();
            // Add a new component to the button.
            button.gameObject.AddComponent<CraftingButton>().Index = i;
            m_craftingButtons.Add(button);
        }
    }
}
