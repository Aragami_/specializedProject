using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class CraftingButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    #region Variables
    [Tooltip("The CraftingRecipe this button uses.")]
    private CraftingRecipe m_myRecipe = null;

    private GraphicRaycaster m_raycast = null;

    private int m_index = 0;

    public int Index
    {
        get => m_index;
        set => m_index = value;
    }

    public CraftingRecipe MyRecipe
    {
        get => m_myRecipe;
    }
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        // Add the method to the OnClick list.
        GetComponent<Button>().onClick.AddListener(Craft);

        // Get the GraphicRaycaster from the canvas.
        m_raycast = transform.root.GetComponent<GraphicRaycaster>();

        // This script get his index from the CraftingSystem, while creating the buttons.
        m_myRecipe = CraftingSystem.Instance.CraftingRecipes[Index];

        GetComponentInChildren<TMP_Text>().text = $"{m_myRecipe.ItemName}";
    }

    public void Craft()
    {
        // If the check is false, there is nothing to do here anymore.
        if (!CheckInventory())
        {
            SoundManager.Instance.PlaySoundEffect(PlayerController.Instance.gameObject, SoundManager.Instance.InvalidBuildEmitter);
            return;
        }

        SoundManager.Instance.PlaySoundEffect(PlayerController.Instance.gameObject, SoundManager.Instance.CraftingEmitter);
        InventoryManager.Instance.AddItem(m_myRecipe.ItemType, m_myRecipe.CraftingAmount);
    }

    /// <summary>
    /// Checks the inventory for the needed resources.
    /// </summary>
    /// <returns>If the player has enough resources and they were reduces as well, then it returns true.</returns>
    private bool CheckInventory()
    {
        if (GameManager.Instance.GodMode)
        {
            return true;
        }

        CraftingResource resource = null;
        int resourceAmount = 0;

        // Checks if the needed resources are enough or even there.
        for (int i = 0; i < m_myRecipe.CraftingResources.Length; i++)
        {
            resource = m_myRecipe.CraftingResources[i];

            try
            {
                resourceAmount = InventoryManager.Instance.GetItemAmount(resource.ItemType);

                // This is asking if there is NOT enough resources...
                if (resource.NeededAmount > resourceAmount)
                {
                    GameManager.Instance.EventMessage($"I don't have enough {resource.ItemType}");
                    return false;
                }
            }
            catch
            {
                GameManager.Instance.EventMessage("I don't have enough resources!");
                return false;
            }
        }

        // "Use" the resources.
        for (int i = 0; i < m_myRecipe.CraftingResources.Length; i++)
        {
            resource = m_myRecipe.CraftingResources[i];

            try
            {
                InventoryManager.Instance.SubtractItem(resource.ItemType, resource.NeededAmount);
            }
            catch
            {
                GameManager.Instance.EventMessage($"I don't have this resource!");
                return false;
            }
        }

        return true;
    }

    /// <summary>
    /// Set the text in the tooltip.
    /// </summary>
    private void SetTooltipText()
    {
        RecipeTooltip.Instance.TooltipItemName.text = $"{MyRecipe.ItemName}";

        for (int i = 0; i < MyRecipe.CraftingResources.Length; i++)
        {
            // Set the first one, aka override what was inside.
            if (i == 0)
            {
                RecipeTooltip.Instance.TooltipResources.text = $"{MyRecipe.CraftingResources[i].ItemType} x{MyRecipe.CraftingResources[i].NeededAmount}" ;
            }
            // If more than one resource is needed, add the rest.
            else
            {
                RecipeTooltip.Instance.TooltipResources.text += $"\n{MyRecipe.CraftingResources[i].ItemType} x{MyRecipe.CraftingResources[i].NeededAmount}" ;
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        List<RaycastResult> results = new List<RaycastResult>();

        m_raycast.Raycast(eventData, results);

        for (int i = 0; i < results.Count; i++)
        {
            if (results[i].gameObject.CompareTag("CraftingButton"))
            {
                results[i].gameObject.GetComponentInChildren<CraftingButton>()?.SetTooltipText();

                RecipeTooltip.Instance.OpenTooltip();
            }
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (eventData.pointerEnter.CompareTag("Tooltip"))
        {
            return;
        }
        else if (eventData.pointerEnter.CompareTag("CraftingButton"))
        {
            RecipeTooltip.Instance.CloseTooltip();
        }
    }
}
