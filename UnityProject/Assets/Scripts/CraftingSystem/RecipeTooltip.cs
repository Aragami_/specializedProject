using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RecipeTooltip : MonoBehaviour
{
    public static RecipeTooltip Instance = null;

    #region Variable
    #region SerializeField
    [SerializeField]
    [Tooltip("The parent of the tooltip parts.")]
    private RectTransform m_tooltip = null;

    [SerializeField]
    [Tooltip("The text field from the tooltip for the item name.")]
    private TMP_Text m_tooltipItemName = null;

    [SerializeField]
    [Tooltip("The text field from the tooltip for the needed resources.")]
    private TMP_Text m_tooltipResources = null;
    #endregion

    #region Private
    [Tooltip("Is the tooltip active? Default = false.")]
    private bool m_tooltipIsActive = false;

    [Tooltip("The half width of the screen size.")]
    private float m_halfWidth = Screen.width * 0.5f;
    [Tooltip("The half height of the screen size.")]
    private float m_halfHeight = Screen.height * 0.5f;

    [Tooltip("The tooltip width.")]
    private float m_TooltipWidth = 0.0f;
    [Tooltip("The tooltip height.")]
    private float m_TooltipHeight = 0.0f;
    #endregion

    public TMP_Text TooltipItemName
    {
        get => m_tooltipItemName;
    }

    public TMP_Text TooltipResources
    {
        get => m_tooltipResources;
    }
    #endregion

    #region Awake | Start | Update
    private void Awake()
    {
        if (Instance)
        {
            Destroy(this.gameObject);
            return;
        }
        Instance = this;

        CloseTooltip();
    }

    // Start is called before the first frame update
    void Start()
    {
        m_halfWidth = Screen.width * 0.5f;
        m_halfHeight = Screen.height * 0.5f;
        m_TooltipWidth = m_tooltip.rect.width;
        m_TooltipHeight = m_tooltip.rect.height;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_tooltipIsActive)
        {
            RepositionTooltip();
        }
    }
    #endregion

    /// <summary>
    /// Show the tooltip.
    /// </summary>
    public void OpenTooltip()
    {
        m_tooltipIsActive = true;
        m_tooltip.gameObject.SetActive(true);
    }

    /// <summary>
    /// Hides the tooltip.
    /// </summary>
    public void CloseTooltip()
    {
        m_tooltipIsActive = false;
        m_tooltip.gameObject.SetActive(false);
    }

    /// <summary>
    /// Moves the tooltip with the mouse position and also will move the tooltip so it stays in side the viewable canvas.
    /// </summary>
    private void RepositionTooltip()
    {
        Vector3 mousePosition = Input.mousePosition;

        // Right top
        // If the mouse is at the top right corner, the tooltip will be moved to the left bottom side of the mouse.
        if (mousePosition.x >= m_halfWidth && mousePosition.y >= m_halfHeight)
        {
            m_tooltip.transform.position = mousePosition - new Vector3(m_TooltipWidth + 50, m_TooltipHeight + 50);
        }
        // Left top
        // If the mouse is at the left top corner, the tooltip will be moved to the right bottom side of the mouse.
        else if (mousePosition.x <= m_halfWidth && mousePosition.y >= m_halfHeight)
        {
            m_tooltip.transform.position = mousePosition + new Vector3(m_TooltipWidth + 50, -m_TooltipHeight - 50);
        }
        // Right bottom
        // If the mouse is at the right bottom corner, the tooltip will be moved to the left top side of the mouse.
        else if (mousePosition.x >= m_halfWidth && mousePosition.y <= m_halfHeight)
        {
            m_tooltip.transform.position = mousePosition + new Vector3(-m_TooltipWidth - 50, m_TooltipHeight + 50);
        }
        // Left bottom
        // If the mouse is at the left bottom corner, the tooltip will be moved to the right top side of the mouse.
        else
        {
            m_tooltip.transform.position = mousePosition + new Vector3(m_TooltipWidth + 50, m_TooltipHeight + 50);
        }
    }

    /// <summary>
    /// Recalculate the half size of the screen and the size of the tooltip.
    /// </summary>
    private void RecalculateHalfSize()
    {
        m_halfWidth = Screen.width * 0.5f;
        m_halfHeight = Screen.height * 0.5f;
        m_TooltipWidth = m_tooltip.rect.width;
        m_TooltipHeight = m_tooltip.rect.height;
    }
}
