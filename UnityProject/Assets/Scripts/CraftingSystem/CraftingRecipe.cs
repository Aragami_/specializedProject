using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Crafting/Recipe")]
public class CraftingRecipe : ScriptableObject
{
    [SerializeField]
    [Tooltip("The name of the item.")]
    private string m_itemName = null;

    [SerializeField]
    [Tooltip("The item type.")]
    private ERESOURCETYPE m_itemType = ERESOURCETYPE.NONE;

    [Range(1, 255)]
    [Tooltip("The amount it will craft. Default = 1, Max = 255.")]
    private int m_craftingAmount = 1;

    [SerializeField]
    [Tooltip("An array with the needed resources. Add/Remove resources in the Editor.")]
    private CraftingResource[] m_craftingResources = null;

    public ERESOURCETYPE ItemType
    {
        get => m_itemType;
    }

    public string ItemName
    {
        get => m_itemName;
    }

    public int CraftingAmount
    {
        get => m_craftingAmount;
    }

    public CraftingResource[] CraftingResources
    {
        get => m_craftingResources;
    }
}
