using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingUI : MonoBehaviour
{
    #region Variables
    [SerializeField]
    [Tooltip("The button prefab to use.")]
    private GameObject m_buttonPrefab = null;

    [Tooltip("A list of all buttons for the building UI.")]
    private List<Button> m_buildingButtons = new List<Button>();
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        GenerateButtons();
    }

    /// <summary>
    /// Generates button with the length of the PreviewPart in BuildingSystem.
    /// </summary>
    public void GenerateButtons()
    {
        // The amount of needed buttons depends on the PreviewPart length.
        int buttonAmount = BuildingSystem.Instance.PreviewPart.Length;

        // Creating new buttons with the prefab, which is just a empty button.
        for (int i = 0; i < buttonAmount; i++)
        {
            // Set this object as the parent of the button.
            Button button = Instantiate(m_buttonPrefab, this.transform).GetComponent<Button>();
            // Add a new component to the button, which handels the rest like setting the PreviewIndex in BuildingSystem.
            button.gameObject.AddComponent<BuildingButton>().Index = i;
            m_buildingButtons.Add(button);
        }
    }
}
