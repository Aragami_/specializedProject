using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnappingPoint : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Building types that deactives this object. Need to be set in the editor.")]
    private EBuildingType[] m_types = null;

    private void OnTriggerStay(Collider _other)
    {
        // Only happens if the object has this tag.
        if (_other.CompareTag("Building"))
        {
            Building building = _other.GetComponent<Building>();
            if (building == null)
            {
                return;
            }

            // Checks if the building has the type which would deactivate this object.
            foreach (EBuildingType type in m_types)
            {
                // If it does it will deactivate this object.
                if (building.MyType == type)
                {
                    // Add this SnappingPoint to the others list of snapping point which it deactivated.
                    building.MySnappingPoints.Add(this);
                    this.gameObject.SetActive(false);
                    return;
                }
            }
        }
    }
}
