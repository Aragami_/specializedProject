using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour
{
    #region Variable
    [SerializeField]
    [Tooltip("The BuildingPreview from this building.")]
    private BuildingPreview m_buildingPreview = null;

    [SerializeField]
    [Tooltip("The building type from this building. Set this in the editor.")]
    private EBuildingType m_myType = EBuildingType.None;

    #region Private
    [Tooltip("A list of snapping points which this objects deactivated.")]
    private List<SnappingPoint> m_snappigPoints = new List<SnappingPoint>();

    [Tooltip("The amount of resources the player gets when destroying a building.")]
    private CraftingResource[] m_returnAmount = null;
    #endregion

    #region Property
    public List<SnappingPoint> MySnappingPoints
    {
        get => m_snappigPoints;
    }

    public EBuildingType MyType
    {
        get => m_myType;
    }
    #endregion
    #endregion

    private void Awake()
    {
        SetUsedAmount();
    }

    /// <summary>
    /// Set the amount of ressource which was used to build this. With the BuildingPreview script.
    /// </summary>
    public void SetUsedAmount()
    {
        CraftingResource[] res = m_buildingPreview.Resources;
        m_returnAmount = new CraftingResource[res.Length];
        for (int i = 0; i < res.Length; i++)
        {
            m_returnAmount[i] = new CraftingResource(res[i].ItemType, Mathf.RoundToInt(res[i].NeededAmount * BuildingSystem.Instance.ReturnAmount));
            continue;
        }
    }

    /// <summary>
    /// The amount of ressources which are returned.
    /// </summary>
    public void ReturnAmount()
    {
        for (int i = 0; i < m_returnAmount.Length; i++)
        {
            InventoryManager.Instance.AddItem(m_returnAmount[i].ItemType, m_returnAmount[i].NeededAmount);
        }
    }
}
