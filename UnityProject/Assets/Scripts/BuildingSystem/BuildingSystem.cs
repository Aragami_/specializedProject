using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingSystem : MonoBehaviour
{
    public static BuildingSystem Instance = null;

    #region Variables

    #region SerializeField
    [SerializeField]
    [Tooltip("Building parts that are allowed to be placed without snapping.")]
    private List<EBuildingType> m_allowedTypes = new List<EBuildingType>();

    [SerializeField]
    [Tooltip("Array with all building parts preview.")]
    private GameObject[] m_previewParts = null;
    [SerializeField]
    [Tooltip("The panel for the building ui.")]
    private GameObject m_buildingPanel = null;

    [SerializeField]
    [Tooltip("Use this to prevent the preview from getting hit by the raycast.")]
    private LayerMask m_ignorePreviewLayer = 0;
    [SerializeField]
    [Tooltip("Use this to only hit the building layer.")]
    private LayerMask m_buildingLayer = 0;
    [SerializeField]
    [Tooltip("Use this to only hit the building snap point layer.")]
    private LayerMask m_snapPointLayer = 0;

    [SerializeField]
    [Tooltip("The length of the building raycast.")]
    private float m_rayLength = 0.0f;
    [SerializeField]
    [Tooltip("Rotation speed of the preview.")]
    private float m_previewRotationSpeed = 0.0f;

    [SerializeField]
    [Range(0.0f, 1.0f)]
    [Tooltip("The amount to return after destroying a building. Default = 0.5f. 0 - 1 means 0% - 100%.")]
    private float m_returnAmount = 0.5f;

    #endregion

    #region Private
    [Tooltip("The BuildingPreview script of the selected part.")]
    private BuildingPreview m_selectedPartBP = null;

    [Tooltip("The selected building part which will be shown/builded.")]
    private GameObject m_selectedPart = null;

    [Tooltip("The transform from the camera which the player uses.")]
    private Transform m_cameraTransform = null;

    [Tooltip("The ray which will be used to decide the position of the building part.")]
    private Ray m_buildingRay = new Ray();

    [Tooltip("The last rotation before destroying the preview.")]
    private Quaternion m_lastBuildingRotation = Quaternion.identity;

    [Tooltip("Is the building system active? Default = false.")]
    private bool m_isActive = false;
    [Tooltip("Is the building panle active? Default = false.")]
    private bool m_buildingPanelActive = false;

    [Tooltip("The index for the m_previewParts array. Decides which preview will be shown.")]
    private int m_previewIndex = 0;

    [Tooltip("Is the preview in the NotBuildable collider? Default = false;")]
    private bool m_isInNotBuildable = false;
    #endregion

    #region Property
    public bool IsActive
    {
        get => m_isActive;
        private set => m_isActive = value;
    }

    public GameObject[] PreviewPart
    {
        get => m_previewParts;
    }

    public int PreviewIndex
    {
        get => m_previewIndex;
        set => m_previewIndex = value;
    }

    public float ReturnAmount
    {
        get => m_returnAmount;
    }

    public bool IsInNotBuildable
    {
        get => m_isInNotBuildable;
        set => m_isInNotBuildable = value;
    }

    public LayerMask BuildingLayer
    {
        get => m_buildingLayer;
        private set => m_buildingLayer = value;
    }
    #endregion

    #endregion

    #region Awake | Update
    private void Awake()
    {
        if (Instance)
        {
            Destroy(this.gameObject);
            return;
        }

        Instance = this;

        m_buildingPanel.SetActive(false);
    }

    private void Start()
    {
        m_cameraTransform = Camera.main.transform;
    }

    // Update is called once per frame
    private void Update()
    {
        if (!GameManager.Instance.DbHandler.LoggedIn)
        {
            return;
        }

        if (!DeveloperConsoleBehaviour.Instance.IsConsoleActive)
        {
            if (Input.GetKeyDown(KeyCode.V))
            {
                ToggleBuildingSystem();
            }
            else if (Input.GetKeyDown(KeyCode.Tab))
            {
                ToggleBuildingUI();
            }
            else if (Input.GetKeyDown(KeyCode.Escape))
            {
                CloseBuildingUI();
            }

            if (IsActive)
            {
                BuildingRaycast();
                SnapToPos();
                Build();
                DestroyBuildingRaycast();
            }
        }
    }
    #endregion

    #region Methods
    /// <summary>
    /// Shoots a raycast to decide the position for the building part.
    /// </summary>
    private void BuildingRaycast()
    {
        // Create the ray with the position of the camera, going forward.
        m_buildingRay = new Ray(m_cameraTransform.position, m_cameraTransform.forward);

        // Shoots the ray. Will only hit the layers which are marked.
        if (Physics.Raycast(m_buildingRay, out RaycastHit hit, m_rayLength, m_ignorePreviewLayer))
        {
            if (m_selectedPart)
            {
                m_selectedPart.SetActive(true);
                if (Input.GetKey(KeyCode.Mouse1))
                {
                    PlayerController.Instance.CanRotate = false;
                }

                // Need to check if BP exist. There is a frame where the BP doesn't exist, when the preview is changed to another one.
                if (m_selectedPartBP && !m_selectedPartBP.IsSnapped)
                {
                    m_selectedPart.transform.position = hit.point + Vector3.up * 0.01f;
                    RotatePreview();
                }
            }
            // If selected part doesn't exist, create a new one.
            else
            {
                m_selectedPart = Instantiate(m_previewParts[PreviewIndex], hit.point, m_lastBuildingRotation);
                m_selectedPartBP = m_selectedPart.GetComponentInChildren<BuildingPreview>();
            }
        }
        // If raycast hits nothing the selected part will be deactivated.
        else
        {
            if (m_selectedPart)
            {
                m_selectedPart.SetActive(false);
                IsInNotBuildable = false;
                PlayerController.Instance.CanRotate = true;
            }
        }
    }

    /// <summary>
    /// Shoots a raycast to destroy buildings.
    /// </summary>
    private void DestroyBuildingRaycast()
    {
        if (Input.GetKeyDown(KeyCode.Mouse2))
        {
            if (Physics.Raycast(m_buildingRay, out RaycastHit hit, m_rayLength, m_buildingLayer))
            {
                ActivateSP(hit);
                Building building = hit.collider.gameObject.GetComponentInParent<Building>();
                if (building == null)
                {
                    return;
                }
                building.ReturnAmount();
                Destroy(hit.collider.gameObject.transform.root.gameObject);
            }
        }
    }

    /// <summary>
    /// This is a complicated method... It activates snapping points which were deactivated. It also removes non existing snapping points which were destroyed from other list.
    /// Look into the method if you want to understand it more. Comments included...
    /// </summary>
    /// <param name="_hit">The RaycastHit from the DestroyBuildingRaycast().</param>
    private void ActivateSP(RaycastHit _hit)
    {
        // This is complicated but I will try to explain it...
        // First get the Building script, which contains a list with snapping point this building has deactivated.
        Building building = _hit.collider.gameObject.GetComponent<Building>();
        if (building == null)
        {
            return;
        }

        // Here it goes for every SnappingPoint in the list. The parent Building script also knows which object he has deactivated.
        // Which means when snapped the Building script, which will be destroy after this, will give a missing object ref to the other Building script, which has his snapping points in it.
        // To prefend that I found this complicated solution...
        foreach (SnappingPoint sp in building.MySnappingPoints)
        {
            // This shouldn't happen anymore, but better safe than sorry I would say.
            if (sp == null)
            {
                continue;
            }
            // So here we will get every Building script from the SnappingPoints which the to destroying building has deactivated.
            Building[] tmp = sp.GetComponentsInParent<Building>();

            // In here we will get every ManageSP, which contains every snapping points which are child of this parent.
            foreach (Building otherBuilding in tmp)
            {
                ManageSP[] tmp2 = building.GetComponentsInChildren<ManageSP>();

                // Then we will go in here and go through every ManageSP in the array.
                foreach (ManageSP manageSP in tmp2)
                {
                    // Now we remove the SnappingPoint from Building scripts which still exist but would get an missing ref from the to destroy building.
                    // With that we would cover every snapping point a building has.
                    foreach (SnappingPoint childSP in manageSP.SnappingPoints)
                    {
                        otherBuilding.MySnappingPoints.Remove(childSP);
                    }
                }
            }

            // Afterwards activate the sp which has been deactivated from the to destroy building.
            sp.GetComponentInParent<ManageSP>()?.ActivateSP(sp);
        }
    }

    /// <summary>
    /// Destroys the object in the m_selectedPart variable, 
    /// also set the BuildingPreview variable (m_selectedPartBP) to null.
    /// </summary>
    public void DestroySelectedPartVar()
    {
        if (!m_selectedPart)
        {
            return;
        }

        IsInNotBuildable = false;
        m_lastBuildingRotation = m_selectedPart.transform.rotation;
        Destroy(m_selectedPart);
        m_selectedPartBP = null;
        PlayerController.Instance.CanRotate = true;
    }

    /// <summary>
    /// Rotate the preview with the mouse.
    /// </summary>
    private void RotatePreview()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            PlayerController.Instance.CanRotate = false;
        }
        else if (Input.GetKeyUp(KeyCode.Mouse1))
        {
            PlayerController.Instance.CanRotate = true;
        }

        if (Input.GetKey(KeyCode.Mouse1))
        {
            Vector3 rotation = m_selectedPart.transform.localEulerAngles;
            rotation.y += Input.GetAxisRaw("Mouse X") * m_previewRotationSpeed;
            m_selectedPart.transform.localEulerAngles = rotation;
        }
    }

    /// <summary>
    /// This manages the preview position when snapped or if it gets detached from snapping points.
    /// If it gets detached it will set the position in BuildingRaycast().
    /// </summary>
    private void SnapToPos()
    {
        if (!m_selectedPart || !m_selectedPartBP)
        {
            return;
        }

        // A raycast that only hits the snapping points.
        // This ray goes through colliders and returns every hit.
        RaycastHit[] snappingHits = Physics.RaycastAll(m_buildingRay, m_rayLength, m_snapPointLayer);
        // If anything was hit on this given layer.
        if (snappingHits != null && snappingHits.Length > 0)
        {
            // If it his one the preview will be activated, just in case if the snapping point is in the air.
            m_selectedPart.SetActive(true);
            if (Input.GetKey(KeyCode.Mouse1))
            {
                PlayerController.Instance.CanRotate = false;
            }

            // Check every hit from the ray
            for (int i = 0; i < snappingHits.Length; i++)
            {
                // Check if the snapping tag from the selected part is given by one of the hits.
                foreach (string sp in m_selectedPartBP.SnapTo)
                {
                    // If yes then it will snap.
                    if (snappingHits[i].collider.CompareTag(sp))
                    {
                        m_selectedPartBP.SnapToSP(snappingHits[i].collider.gameObject);
                        return;
                    }
                }
            }
        }
        // Otherwise if nothing was hit in this layer then detach the selected part from the snap point.
        else
        {
            m_selectedPartBP.Detach();
        }
    }

    /// <summary>
    /// This method just build the actual building.
    /// </summary>
    private void Build()
    {
        // If the player press the left mouse button then the preview will be changed to the actuall building
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            // This can happen if the player has the building menu open and is not in the building mode.
            // In this one moment this can be null when changing the building part.
            if (!m_selectedPart && !m_selectedPartBP)
            {
                return;
            }
            else if (!m_selectedPart.activeSelf)
            {
                return;
            }

            // Get the BuildingPreview script which has the actual building as variable.
            GameObject buildVersion = m_selectedPartBP.BuildVersion;
            // Check if the the variable is set or not. If the variable is set place the building and destroy the preview.
            if (buildVersion && m_selectedPartBP.IsPlaceable && !m_buildingPanelActive)
            {
                // This is gona be the Building script which is on the building.
                Building building = null;

                // Check if the building part has one of the allowes types which doesn't need to snap first.
                foreach (EBuildingType type in m_allowedTypes)
                {
                    // If it has one of thous type it will check the inventory and build it if there are enough resources.
                    // Will return after building or not enough resource message.
                    if (m_selectedPartBP.MyType == type)
                    {
                        if (CheckInventory())
                        {
                            SoundManager.Instance.PlaySoundEffect(PlayerController.Instance.gameObject, SoundManager.Instance.BuildEmitter);
                            // Save it, so it can be check for null.
                            building = Instantiate(buildVersion, m_selectedPart.transform.position, m_selectedPart.transform.rotation).GetComponent<Building>();

                            if (building != null)
                            {
                                building.SetUsedAmount();
                            }

                            DestroySelectedPartVar();
                            return;
                        }
                        else
                        {
                            if (GameManager.Instance)
                            {
                                SoundManager.Instance.PlaySoundEffect(PlayerController.Instance.gameObject, SoundManager.Instance.InvalidBuildEmitter);
                                GameManager.Instance.EventMessage("I don't have enough resources...");
                                return;
                            }
                        }
                    }
                }

                // If the building part doesn't have an allowed type it will continue here.
                // Check if the building part is snapped to something, if not it will trhow a message and return.
                if (!m_selectedPartBP.IsSnapped)
                {
                    SoundManager.Instance.PlaySoundEffect(PlayerController.Instance.gameObject, SoundManager.Instance.InvalidBuildEmitter);
                    GameManager.Instance.EventMessage("This building part is not snapped to anything!");
                    return;
                }

                // If it's snapped then check the inventory for resources.
                if (CheckInventory())
                {
                    SoundManager.Instance.PlaySoundEffect(PlayerController.Instance.gameObject, SoundManager.Instance.BuildEmitter);
                    // Save it, so it can be check for null.
                    building = Instantiate(buildVersion, m_selectedPart.transform.position, m_selectedPart.transform.rotation).GetComponent<Building>();

                    if (building != null)
                    {
                        building.SetUsedAmount();
                    }

                    DestroySelectedPartVar();
                }
                else
                {
                    if (GameManager.Instance)
                    {
                        SoundManager.Instance.PlaySoundEffect(PlayerController.Instance.gameObject, SoundManager.Instance.InvalidBuildEmitter);
                        GameManager.Instance.EventMessage("I don't have enough resources...");
                    }
                }

            }
            else if (IsInNotBuildable)
            {
                SoundManager.Instance.PlaySoundEffect(PlayerController.Instance.gameObject, SoundManager.Instance.InvalidBuildEmitter);
                GameManager.Instance.EventMessage("Maybe I should build on the beach...");
            }
            else
            {
                SoundManager.Instance.PlaySoundEffect(PlayerController.Instance.gameObject, SoundManager.Instance.InvalidBuildEmitter);
                GameManager.Instance.EventMessage("I can't build this here. There isn't enough space here!");
            }
        }
    }

    #region Helpers (closing, opening stuff etc.)
    /// <summary>
    /// This will toggle the building system on or off.
    /// </summary>
    public void ToggleBuildingSystem()
    {
        if (m_isActive)
        {
            m_isActive = false;
            DestroySelectedPartVar();
        }
        else
        {
            m_isActive = true;
        }
    }

    /// <summary>
    /// Deactivates the building system.
    /// </summary>
    public void DeactivateBuildingSystem()
    {
        m_isActive = false;
        DestroySelectedPartVar();
    }

    /// <summary>
    /// Activates the building system.
    /// </summary>
    public void ActivateBuildingSystem()
    {
        m_isActive = true;
    }

    /// <summary>
    /// Check the inventory for the required ressources.
    /// Also will reduce the ressources if there is enough ressources.
    /// </summary>
    /// <returns>If there is enough ressources it will return true. Otherwise false.</returns>
    private bool CheckInventory()
    {
        if (GameManager.Instance.GodMode)
        {
            return true;
        }

        CraftingResource resource = null;
        int resourceAmount = 0;

        // Checks if the needed resources are enough or even there.
        for (int i = 0; i < m_selectedPartBP.Resources.Length; i++)
        {
            resource = m_selectedPartBP.Resources[i];

            try
            {
                resourceAmount = InventoryManager.Instance.GetItemAmount(resource.ItemType);

                // This is asking if there is NOT enough resources...
                if (resource.NeededAmount > resourceAmount)
                {
                    GameManager.Instance.EventMessage($"I don't have enough {resource.ItemType}!");
                    return false;
                }
            }
            catch
            {
                GameManager.Instance.EventMessage($"I don't have enough {resource.ItemType}!");
                return false;
            }
        }

        // "Use" the resources.
        for (int i = 0; i < m_selectedPartBP.Resources.Length; i++)
        {
            resource = m_selectedPartBP.Resources[i];

            try
            {
                InventoryManager.Instance.SubtractItem(resource.ItemType, resource.NeededAmount);
            }
            catch
            {
                GameManager.Instance.EventMessage($"I don't have {resource.ItemType}!");
                return false;
            }
        }

        return true;
    }

    /// <summary>
    /// Toggle the building UI.
    /// </summary>
    public void ToggleBuildingUI()
    {
        if (m_buildingPanelActive)
        {
            CloseBuildingUI();
        }
        else
        {
            OpenBuildingUI();
        }
    }

    /// <summary>
    /// Close the building UI.
    /// </summary>
    public void CloseBuildingUI()
    {
        m_buildingPanel.SetActive(false);
        m_buildingPanelActive = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        PlayerController.Instance.CanRotate = true;
    }

    /// <summary>
    /// Open the building UI.
    /// </summary>
    private void OpenBuildingUI()
    {
        m_buildingPanel.SetActive(true);
        m_buildingPanelActive = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        PlayerController.Instance.CanRotate = false;
    }
    #endregion
    #endregion
}
