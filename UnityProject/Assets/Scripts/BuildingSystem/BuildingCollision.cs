using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingCollision : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Does the model has details as own parts. Default = false.")]
    private bool m_hasDetails = false;

    [Tooltip("The BuildingPreview script on the root.")]
    private BuildingPreview m_buildingPreview = null;

    [Tooltip("MeshRenderer from the details.")]
    private MeshRenderer[] m_detailsMeshRenderer = null;

    [Tooltip("Material array to change the material of this object.")]
    private Material[] m_buildableMatArray = null;
    [Tooltip("Material array to change the material of this object.")]
    private Material[] m_notBuildableMatArray = null;

    public bool HasDetails
    {
        get => m_hasDetails;
    }

    public MeshRenderer[] DetailsMeshRenderer
    {
        get => m_detailsMeshRenderer;
    }

    /// <summary>
    /// Set the three values: previewScript, builableMatArray and notBuildableMatArray.
    /// </summary>
    /// <param name="_previewScript">The preview script on the root.</param>
    /// <param name="_buildableMat">The materials which are used to show that is buildable.</param>
    /// <param name="_notBuildableMat">The materials which are used to show that isn't buildable.</param>
    public void SetValues(BuildingPreview _previewScript, Material[] _buildableMat, Material[] _notBuildableMat)
    {
        m_buildingPreview = _previewScript;
        m_buildableMatArray = _buildableMat;
        m_notBuildableMatArray = _notBuildableMat;

        if (m_hasDetails)
        {
            m_detailsMeshRenderer = GetComponentsInChildren<MeshRenderer>();
            foreach (MeshRenderer mr in m_detailsMeshRenderer)
            {
                mr.materials = m_buildableMatArray;
            }
        }
    }

    private void OnTriggerEnter(Collider _other)
    {
        // Makes this collider to ignore the snapping collider.
        for (int i = 0; i < m_buildingPreview.SnapTo.Length; i++)
        {
            string toIgnore = m_buildingPreview.SnapTo[i];

            if (_other.gameObject.CompareTag(toIgnore))
            {
                Physics.IgnoreCollision(this.GetComponent<Collider>(), _other);
            }
        }
    }

    private void OnTriggerStay(Collider _other)
    {
        if (!m_buildingPreview)
        {
            return;
        }

        if (_other.CompareTag("NotBuildable"))
        {
            BuildingSystem.Instance.IsInNotBuildable = true;
        }

        m_buildingPreview.MyMeshRenderer.materials = m_notBuildableMatArray;
        if (m_hasDetails)
        {
            foreach (MeshRenderer mr in m_detailsMeshRenderer)
            {
                mr.materials = m_notBuildableMatArray;
            }
        }
        m_buildingPreview.IsPlaceable = false;
    }

    private void OnTriggerExit(Collider _other)
    {
        if (_other.CompareTag("NotBuildable"))
        {
            BuildingSystem.Instance.IsInNotBuildable = false;
        }

        m_buildingPreview.MyMeshRenderer.materials = m_buildableMatArray;
        if (m_hasDetails)
        {
            foreach (MeshRenderer mr in m_detailsMeshRenderer)
            {
                mr.materials = m_buildableMatArray;
            }
        }
        m_buildingPreview.IsPlaceable = true;
    }
}
