using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EBuildingType
{
    None,
    Floor,
    Wall,
    Window,
    RoofAngled,
    RoofFlat,
    Doorway,
    Well,
    Chest,
    RoofSide,
    RoofCornerL,
    RoofCornerR,
    FlowerBed,
}

public class BuildingPreview : MonoBehaviour
{
    #region Variables

    #region SerializeField
    [SerializeField]
    [Tooltip("What type of building this is. Default None.")]
    private EBuildingType m_myType = EBuildingType.None;

    [SerializeField]
    [Tooltip("The needed resources to build. Add/Remove in the Editor.")]
    private CraftingResource[] m_resources = null;

    [SerializeField]
    [Tooltip("The prefab of the actually version.")]
    private GameObject m_buildVersion = null;

    [SerializeField]
    [Tooltip("The material to use when it's buildable.")]
    private Material m_buildableMat = null;
    [SerializeField]
    [Tooltip("The material to use when it ISN'T buildable.")]
    private Material m_notBuildableMat = null;

    [SerializeField]
    [Tooltip("The MeshRenderer of this object.")]
    private MeshRenderer m_meshRenderer = null;

    [SerializeField]
    [Tooltip("Tag the preview snaps to. Fill in editor.")]
    private string[] m_snapTo = null;
    #endregion

    #region Private
    [Tooltip("The BuildingCollision from this object.")]
    private BuildingCollision m_buldingCollision = null;

    [Tooltip("The snapping point to which this is snapped.")]
    private SnappingPoint m_mySnappingPoint = null;

    [Tooltip("An array with the ManageSP from childs.")]
    private ManageSP[] m_manageSP = null;

    [Tooltip("Material array to change the material of this object.")]
    private Material[] m_buildableMatArray = null;
    [Tooltip("Material array to change the material of this object.")]
    private Material[] m_notBuildableMatArray = null;

    [Tooltip("Is the building placeable? Default = true.")]
    private bool m_isPlaceable = true;
    [Tooltip("Is the building snapped to something? Default = false.")]
    private bool m_isSnapped = false;
    #endregion

    #region Property
    public ManageSP[] MyManageSP
    {
        get => m_manageSP;
    }

    public EBuildingType MyType
    {
        get => m_myType;
    }

    public CraftingResource[] Resources
    {
        get => m_resources;
    }

    public GameObject BuildVersion
    {
        get => m_buildVersion;
    }

    public SnappingPoint MySnappingPoint
    {
        get => m_mySnappingPoint;
        private set => m_mySnappingPoint = value;
    }

    public MeshRenderer MyMeshRenderer
    {
        get => m_meshRenderer;
        private set => m_meshRenderer = value;
    }

    public bool IsPlaceable
    {
        get => m_isPlaceable;
        set => m_isPlaceable = value;
    }

    public bool IsSnapped
    {
        get => m_isSnapped;
        private set
        {
            m_isSnapped = value;
            if (m_isSnapped)
            {
                // This is the same what the same what the OnTriggerExit does. Used when OnTriggerExit doesn't happen because of snapping.
                MyMeshRenderer.materials = m_buildableMatArray;

                if (m_buldingCollision.HasDetails)
                {
                    foreach (MeshRenderer mr in m_buldingCollision.DetailsMeshRenderer)
                    {
                        mr.materials = m_buildableMatArray;
                    }
                }
                IsPlaceable = true;
            }
        }
    }

    public string[] SnapTo
    {
        get => m_snapTo;
    }
    #endregion

    #endregion

    private void Awake()
    {
        MyMeshRenderer = GetComponentInChildren<MeshRenderer>();
        m_buildableMatArray = new Material[MyMeshRenderer.materials.Length];
        m_notBuildableMatArray = new Material[MyMeshRenderer.materials.Length];
        for (int i = 0; i < MyMeshRenderer.materials.Length; i++)
        {
            m_buildableMatArray[i] = m_buildableMat;
            m_notBuildableMatArray[i] = m_notBuildableMat;
        }
        MyMeshRenderer.materials = m_buildableMatArray;
        m_buldingCollision = GetComponentInChildren<BuildingCollision>();
        if (m_buldingCollision)
        {
            m_buldingCollision.SetValues(this, m_buildableMatArray, m_notBuildableMatArray);
        }
        m_manageSP = GetComponentsInChildren<ManageSP>();
    }

    /// <summary>
    /// Snap the preview to a snapping point on a building.
    /// </summary>
    /// <param name="_go">The gameObject with it snaps to. Should be only a collider which is a trigger.</param>
    public void SnapToSP(GameObject _go)
    {
        // Nothing to do here if already snapped.
        if (IsSnapped)
        {
            return;
        }

        for (int i = 0; i < m_snapTo.Length; i++)
        {
            string snapTo = m_snapTo[i];

            if (_go.CompareTag(snapTo))
            {
                IsSnapped = true;
                this.transform.position = _go.transform.parent.position;
                this.transform.rotation = _go.transform.parent.rotation;

                // Save the ref to the snapping point to which this is attached.
                if (_go.GetComponent<SnappingPoint>())
                {
                    MySnappingPoint = _go.GetComponent<SnappingPoint>();
                }
                return;
            }
        }
    }

    public void Detach()
    {
        IsSnapped = false;

        // Remove the ref.
        if (MySnappingPoint != null)
        {
            MySnappingPoint = null;
        }
    }
}
