using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageSP : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Every snapping point which is a child from this object.")]
    private List<SnappingPoint> m_snappingPoints = new List<SnappingPoint>();

    public List<SnappingPoint> SnappingPoints
    {
        get => m_snappingPoints;
    }

    private void Awake()
    {
        foreach (Transform child in this.transform)
        {
            m_snappingPoints.Add(child.gameObject.GetComponentInChildren<SnappingPoint>());
        }
    }

    /// <summary>
    /// Activates a snapping point which was deactivated, because there was already something snapped to.
    /// </summary>
    /// <param name="_sp">The snapping point to activate.</param>
    public void ActivateSP(SnappingPoint _sp)
    {
        // Goes through all snapping points.
        foreach (SnappingPoint sp in m_snappingPoints)
        {
            // If the one which should be activated is found, it will beactivated and stop the foreach.
            if (sp == _sp)
            {
                sp.gameObject.SetActive(true);
                return;
            }
        }
    }

    /// <summary>
    /// Deactivates a snapping point which is active, so it can't be used anymore, until activating again.
    /// </summary>
    /// <param name="_sp">The snapping point to deactivate.</param>
    public void DeactivateSP(SnappingPoint _sp)
    {
        foreach (SnappingPoint sp in m_snappingPoints)
        {
            // If the one which should be activated is found, it will beactivated and stop the foreach.
            if (sp == _sp)
            {
                sp.gameObject.SetActive(false);
                return;
            }
        }
    }
}
