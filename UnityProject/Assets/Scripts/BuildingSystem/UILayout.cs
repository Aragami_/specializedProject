using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILayout : LayoutGroup
{
    #region Variables
    [Tooltip("The size of the buttons.")]
    private Vector2 m_buttonScale = Vector2.zero;

    [Tooltip("The amount of rows.")]
    private int m_row = 0;
    [Tooltip("The amount of coloums.")]
    private int m_coloum = 0;
    #endregion

    public override void CalculateLayoutInputHorizontal()
    {
        base.CalculateLayoutInputHorizontal();

        // Calculate the amount of rows and coloums.
        float sqrt = Mathf.Sqrt(transform.childCount);
        m_coloum = Mathf.CeilToInt(sqrt);
        m_row = Mathf.CeilToInt(sqrt);

        m_buttonScale.x = this.rectTransform.rect.width / (float)m_coloum;
        m_buttonScale.y = this.rectTransform.rect.height / (float)m_row;

        int coloumCount = 0;
        int rowCount = 0;

        for (int i = 0; i < rectChildren.Count; i++)
        {
            // Calculate coloum and row.
            rowCount = i / m_coloum;
            coloumCount = i % m_coloum;

            // Get the child with the index i.
            RectTransform building = rectChildren[i];

            // Calculate the new position.
            float xPosition = (m_buttonScale.x * coloumCount);
            float yPosition = (m_buttonScale.y * rowCount);

            // Set the child to the new position and rescale.
            SetChildAlongAxis(building, 0, xPosition, m_buttonScale.x);
            SetChildAlongAxis(building, 1, yPosition, m_buttonScale.y);
        }
    }

    public override void CalculateLayoutInputVertical()
    {

    }

    public override void SetLayoutHorizontal()
    {

    }

    public override void SetLayoutVertical()
    {

    }
}
