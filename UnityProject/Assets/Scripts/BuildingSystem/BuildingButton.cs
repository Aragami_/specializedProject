using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BuildingButton : MonoBehaviour
{
    #region Variables
    [Tooltip("The building preview of the preview.")]
    private BuildingPreview m_myBuildingPreview = null;

    [Tooltip("The text in the button.")]
    private TMP_Text m_text = null;

    private int m_index = 0;

    public int Index
    {
        get => m_index;
        set => m_index = value;
    }
    #endregion

    public void Start()
    {
        // Add the method to the OnClick list.
        GetComponent<Button>().onClick.AddListener(SetIndex);

        // Get the BuildingPreview and split the name of the GameObject to the actual name of the building part.
        m_myBuildingPreview = BuildingSystem.Instance.PreviewPart[Index].GetComponentInChildren<BuildingPreview>();
        m_text = GetComponentInChildren<TMP_Text>();

        SetButtonText();
    }

    /// <summary>
    /// Set the index for the PreviewIndex in BuildingSystem.
    /// </summary>
    public void SetIndex()
    {
        // Should only happen if the player is in the building mode.
        if (BuildingSystem.Instance.IsActive)
        {
            // Set the index and destroys the old preview.
            BuildingSystem.Instance.PreviewIndex = Index;
            BuildingSystem.Instance.DestroySelectedPartVar();
            // Also toggle the building UI off, so the player doesn't have to do it manually.
            BuildingSystem.Instance.ToggleBuildingUI();
        }
        else
        {
            GameManager.Instance.EventMessage("You are not in building mode.");
        }
    }

    /// <summary>
    /// Set the text in the Button.
    /// </summary>
    private void SetButtonText()
    {
        string[] split = m_myBuildingPreview.gameObject.name.Split('_');
        m_text.text = split[0];

        for (int i = 0; i < m_myBuildingPreview.Resources.Length; i++)
        {
            m_text.text += $"\n{m_myBuildingPreview.Resources[i].ItemType} x{m_myBuildingPreview.Resources[i].NeededAmount}";
        }
    }
}
