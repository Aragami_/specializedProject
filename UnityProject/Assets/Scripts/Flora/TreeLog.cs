using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Obsolete("Don't use this script. It is obsolete since there is already a general loot pickup.", true)]
public class TreeLog : MonoBehaviour, ILootable
{
    IEnumerator DestroyAfterLoot()
    {
        yield return new WaitForSeconds(0.01f);
        Destroy(this.gameObject);
    }

    public void Loot()
    {
        StartCoroutine(DestroyAfterLoot());
        //return new LootableRetVal(EEItem.LOG, 1);
    }
}
