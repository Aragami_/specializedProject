using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ItemScript))]
public class Seed : MonoBehaviour, IThrowable
{
    //[SerializeField] private PlantType m_plantTypeAfterPlacing = PlantType.TREE;
    [SerializeField] private MAPTYPE m_mapToHatch = MAPTYPE.FOOTSTEP;
    [SerializeField] private List<AREATYPE> m_areaTypeAllowedToHatch = new List<AREATYPE>();
    [SerializeField] private GameObject m_plantPrefab;
    [SerializeField] private bool m_hatchInFlowerBed = false;
    private FlowerBed m_closestFlowerBed = null;
    private Rigidbody m_rigid;

    private void Hatch()
    {
        // Disable physics interaction
        m_rigid.useGravity = false;
        m_rigid.isKinematic = true;
        //Leave collider on for now

        bool hatchable = false;

        // Check for Hatch allowed
        // First check for FlowerBeds
        if (m_hatchInFlowerBed)
        {
            // Get all buildings in range
            Collider[] buildings = Physics.OverlapSphere(this.transform.position, 2f, LayerMask.GetMask("Building"));
            // Only store flowerBeds
            List<FlowerBed> flowerBeds = new List<FlowerBed>();
            foreach (Collider building in buildings)
            {
                if (building.GetType() == typeof(BoxCollider) && building.TryGetComponent(out FlowerBed foundFlowerBed) && foundFlowerBed.HasEmptyHatchSlot)
                {
                    flowerBeds.Add(foundFlowerBed);
                }
            }
            // Find the closest FlowerBed
            if (flowerBeds.Count > 0)
            {
                m_closestFlowerBed = flowerBeds[0];
                float closestFlowerBedSqrM = (m_closestFlowerBed.transform.position - this.transform.position).sqrMagnitude;
                for (int i = 1; i < flowerBeds.Count; i++)
                {
                    float nextSqrM = (flowerBeds[i].transform.position - this.transform.position).sqrMagnitude;
                    if (nextSqrM < closestFlowerBedSqrM)
                    {
                        m_closestFlowerBed = flowerBeds[i];
                        closestFlowerBedSqrM = nextSqrM;
                    }
                }

                hatchable = true;
            }
        }

        // Hatch on normal ground
        if (!hatchable) // Skip check if already hatching in FlowerBed
        {
            AREATYPE currentAreaType = AreaManager.Instance.GetSingleAreaType(this.transform.position, m_mapToHatch);
            foreach (AREATYPE areaType in m_areaTypeAllowedToHatch)
            {
                // if allowed to hatch here
                if (currentAreaType == areaType)
                {
                    hatchable = true;
                    break;
                }
            }
        }

        // Return Seed Item if not placeAble
        if (hatchable)
        {
            Plant hatchedPlant = Instantiate(m_plantPrefab, m_closestFlowerBed == null ? this.transform.position : m_closestFlowerBed.NextHatchSpot, m_plantPrefab.transform.rotation).GetComponent<Plant>();
            hatchedPlant.m_shouldGrow = true;
            hatchedPlant.RestartGrowth();
            hatchedPlant.InFlowerBed = m_closestFlowerBed != null;
            m_closestFlowerBed?.AddPlant(hatchedPlant);
            GameManager.DestroyOnNextUpdate(this);
        }
        else
        {
            ItemScript item = GetComponent<ItemScript>();
            for (int i = 0; i < item.Item.Length; i++)
            {
                InventoryManager.Instance.AddItem(item.Item[i].ItemID, item.Item[i].Amount);
            }
            GameManager.DestroyOnNextUpdate(this);
        }
    }

    private void Start()
    {
        m_rigid = GetComponent<Rigidbody>();
    }

    public void Throw(Vector3 _force)
    {
        transform.parent = null;
        m_rigid.AddForce(_force);
    }

    private void OnCollisionEnter(Collision _collision)
    {
        Hatch();
    }

    public static implicit operator GameObject(Seed _seed) => _seed.gameObject;

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(this.transform.position, 2f);
    }
}
