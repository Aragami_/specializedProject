using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public enum PlantType
{
    NONE = 0,
    TREE,
    BUSH,
    GRAS,
    NULL,
}

public enum FoodGrowType
{
    /// <summary>
    /// Plant doesn't have food
    /// </summary>
    NONE = 0,
    /// <summary>
    /// Food grows instantly after food grow timer is done
    /// </summary>
    INSTANT,
    /// <summary>
    /// Food grows gradually over time
    /// </summary>
    LINEAR
}

public class Plant : MonoBehaviour, ILootable, IEdible, IDamageable
{
    // General
    [SerializeField] private List<Item> m_plantDestroyItems = new List<Item>();
    public int m_plantMaxHp = 5;
    public PlantType m_plantType = PlantType.NONE;
    // Tree
    public GameObject m_logPrefab;
    public float m_timberTime = 5f;
    public Transform m_bottomPos;
    public Transform m_topPos;
    // Bush
    public bool m_regenLootItems = true;
    public int m_fullRegenTime = 60;
    [SerializeField] private List<Transform> m_berries;
    public Material m_normalBerryMaterial;
    public Material m_flowerBedBerryMaterial;
    // Berries - FlowerBed multiplier
    public float m_growthMultiplier = 2f;
    private bool m_inFlowerBed = false;


    // Growth - This part has a custom inspector
    public bool m_shouldGrow = false;
    public AnimationCurve m_growSpeed = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f); // From instantiating to full scale growth
    public float m_growTime = 10f; // how many units it scales

    // Food - This part has a custom inspector
    public FoodGrowType m_foodGrowType = FoodGrowType.NONE;
    public float m_foodGrowSpeed = 1f;
    public int m_maxFood = 20;

    /// <summary>
    /// The time until the plant got all its loot regenerated again
    /// </summary>
    private readonly List<float> m_currentLootRegen = new List<float>();
    private readonly List<float> m_lootRegenPerSlotPerItem = new List<float>();
    private ItemScript m_ownItemScript;
    /// <summary>
    /// The maximum amount of a loot item for item slot
    /// </summary>
    private readonly List<int> m_maxLootItems = new List<int>();

    /// <summary>
    /// The current amount of Food stored in the plant NOTE: this doesn't represent the amount of food the plant can give right now
    /// </summary>
    private float m_currentFood = 0f;

    private Vector3 m_targetScale;
    private float m_currentGrowTime = 0f;
    private bool m_currentlyGrowing = false;

    private float m_plantHp = 5f;
    private bool m_isDestroyed = false;
    [SerializeField] private FMODUnity.StudioEventEmitter m_soundEmitter_timber;
    [SerializeField] private FMODUnity.StudioEventEmitter m_soundEmitter_wiggle;


    // Wiggle
    private Vector3 m_startRot;

    private void OnEnable()
    {
        m_targetScale = transform.localScale;
        m_currentlyGrowing = m_shouldGrow;
#if UNITY_EDITOR
        if (m_plantType == PlantType.NONE)
            Debug.LogError("No PlantType defined for this plant!", this.gameObject);
#endif
        m_startRot = transform.eulerAngles;

        FloraManager.Instance?.AddEdible(this);

        m_ownItemScript = GetComponent<ItemScript>();
        m_currentLootRegen.Clear();
        m_maxLootItems.Clear();
        m_lootRegenPerSlotPerItem.Clear();
        Item[] items = m_ownItemScript.Item;
        for (int i = 0; i < items.Length; i++)
        {
            m_currentLootRegen.Add(0f);
            m_maxLootItems.Add(items[i].Amount);
            m_lootRegenPerSlotPerItem.Add((float)m_fullRegenTime / items[i].Amount);
            // Set item to 0 at the beginning
            if (m_regenLootItems)
                items[i].Amount = 0;
        }
        ShowBerriesOnModel();
    }

    private void OnDisable()
    {
        FloraManager.Instance?.RemoveEdible(this);
    }

    private void Update()
    {
        // Growth
        if (m_currentlyGrowing)
        {
            m_currentGrowTime += Time.deltaTime;
            if (m_currentGrowTime > m_growTime)
            {
                m_currentGrowTime = m_growTime;
                m_currentlyGrowing = false;
            }

            float growRatio = m_currentGrowTime / m_growTime;

            Vector3 growScale = m_growSpeed.Evaluate(growRatio) * m_targetScale;

            transform.localScale = growScale;
        }

        // Loot Regen
        if (m_regenLootItems)
        {
            for (int i = 0; i < m_lootRegenPerSlotPerItem.Count; i++)
            {
                m_currentLootRegen[i] += Time.deltaTime * (m_inFlowerBed ? m_growthMultiplier : 1f);
                if (m_currentLootRegen[i] > m_lootRegenPerSlotPerItem[i])
                {
                    m_currentLootRegen[i] -= m_lootRegenPerSlotPerItem[i];
                    if (m_ownItemScript.Item[i].Amount < m_maxLootItems[i])
                    {
                        m_ownItemScript.Item[i].Amount++;
                        ShowBerriesOnModel();
                    }
                }
            }
        }

        // Food
        if (IsEdible && m_currentFood < m_maxFood)
        {
            m_currentFood += (m_foodGrowSpeed * Time.deltaTime);
        }
    }

    /// <summary>
    /// Restarts the growth of the plant
    /// </summary>
    public void RestartGrowth()
    {
        m_currentGrowTime = 0f;
        m_currentlyGrowing = true;
    }

    #region Food
    /// <summary>
    /// Gets the current food count of the plant
    /// </summary>
    public int GetFoodCount
    {
        get
        {
            return m_foodGrowType switch
            {
                FoodGrowType.INSTANT => m_currentFood >= m_maxFood ? (int)m_currentFood : 0,
                FoodGrowType.LINEAR => (int)m_currentFood,
                FoodGrowType.NONE => throw new NoFoodPlantException("This plant doesn't grow food!"),
                _ => throw new NoFoodPlantException("This plant doesn't grow food!")
            };
        }
    }

    /// <summary>
    /// Returns if this plant is edible
    /// </summary>
    public bool IsEdible => m_foodGrowType != FoodGrowType.NONE;

    /// <summary>
    /// Eats food
    /// </summary>
    /// <param name="_eatAmount">Wanted amount to eat</param>
    /// <returns>Actual eaten amount</returns>
    public int EatFood(int _eatAmount)
    {
        if (!IsEdible)
            throw new NoFoodPlantException("This plant doesn't grow food!");

        int foodCount = GetFoodCount;
        int eatenAmount = _eatAmount > foodCount ? foodCount : _eatAmount;
        m_currentFood -= eatenAmount;

        return eatenAmount;
    }
    #endregion

    #region Loot
    /// <summary>
    /// Gets a random resource from this plant, removes it from its list and returns it
    /// </summary>
    /// <returns>Random item this plant has, Item.NONE if non existent</returns>
    private ERESOURCETYPE GetRandomLoot()
    {
        List<ERESOURCETYPE> items = new List<ERESOURCETYPE>(); // List of all items stacked up
        foreach (Item plantResource in m_ownItemScript.Item)
        {
            for (int i = 0; i < plantResource.Amount; i++)
            {
                if (plantResource.ItemID != (int)ERESOURCETYPE.WOOD) // Don't get wood by as random loot
                    items.Add((ERESOURCETYPE)plantResource.ItemID);
            }
        }

        if (items.Count > 0)
        {
            ERESOURCETYPE retVal = items[Random.Range(0, items.Count)];
            foreach (Item plantResource in m_ownItemScript.Item)
            {
                if ((ERESOURCETYPE)plantResource.ItemID == retVal && plantResource.Amount > 0)
                {
                    plantResource.Amount--;
                    break;
                }
            }

            return retVal;
        }
        return ERESOURCETYPE.NONE;
    }

    /// <summary>
    /// Loots a random resource and adds it to the player inventory
    /// </summary>
    public void Loot()
    {
        StartCoroutine(WiggleAnimation(0.1f, 3, 3));
        ERESOURCETYPE nextLoot = GetRandomLoot();
        if (nextLoot != ERESOURCETYPE.NONE)
        {
            for (int i = 0; i < m_ownItemScript.Item.Length; i++)
            {
                //InventoryManager.Instance.Additem(item.Item[i].Type, item.Item[i].ItemID, item.Item[i].Amount);
                InventoryManager.Instance.AddItem(m_ownItemScript.Item[i].ItemID, 1);
                if (nextLoot == ERESOURCETYPE.BERRY && m_ownItemScript.Item[i].Amount == 0)
                {
                    m_currentLootRegen[i] = 0f;
                }
            }
            ShowBerriesOnModel();
            //PlayerController.Instance.m_inventory.AddItem(GetScriptableObjectWithResourceType(nextLoot), 1);
        }
    }

    /// <summary>
    /// Fits X amount of logs between two points and returns each logs position
    /// </summary>
    /// <param name="_start">Start point of logs</param>
    /// <param name="_end">End point of logs</param>
    /// <param name="_amountOfLogs">Amount of logs between two points</param>
    /// <returns>Array of position of logs</returns>
    private Vector3[] TreeLogPositions(Vector3 _start, Vector3 _end, int _amountOfLogs)
    {
        Vector3[] retVal = new Vector3[_amountOfLogs];
        Vector3 dirWithLength = _end - _start; // Vector between two points
        Vector3 step = 1 / (float)_amountOfLogs * dirWithLength; // dirWithLength = step * _amountOfLogs
        Vector3 startStep = _start + step / 2; // Start step which is offset since the log at the start and end is only one sided
        for (int i = 0; i < _amountOfLogs; i++)
        {
            retVal[i] = i * step + startStep; // Position of each log between two points
        }

        return retVal;
    }

    /// <summary>
    /// Damages this
    /// </summary>
    /// <param name="_damageAmount">Amount of damage</param>
    /// <returns>True if HP is 0 or below after damage taken, else False</returns>
    public bool Damage(float _damageAmount = 1)
    {
        if (!m_isDestroyed)
        {
            m_plantHp -= _damageAmount;
            if (m_plantHp <= 0)
            {
                m_isDestroyed = true;
                if (m_plantType == PlantType.TREE)
                {
                    StartCoroutine(Timber());
                }
                else
                {
                    foreach (Item plantDestroyItem in m_plantDestroyItems)
                    {
                        InventoryManager.Instance.AddItem(plantDestroyItem.ItemID, plantDestroyItem.Amount);
                    }
                    Destroy(this.gameObject);
                }
                return true;
            }

            m_soundEmitter_wiggle?.Play();
            StartCoroutine(WiggleAnimation(0.1f, 3, 5));
            return false;
        }

        return true;
    }

    private IEnumerator WiggleAnimation(float _timeOfEachWiggle, int _amountOfWiggles, float _wiggleDegree)
    {
        for (int i = 0; i < _amountOfWiggles; i++)
        {
            if (!m_isDestroyed) // Don't wiggle when the tree is already destroyed
            {
                transform.rotation = Quaternion.Euler(m_startRot.x + RandomPositiveNegative(_wiggleDegree), m_startRot.y, m_startRot.z + RandomPositiveNegative(_wiggleDegree));
                yield return new WaitForSeconds(_timeOfEachWiggle);
            }
            else
            {
                yield break;
            }
        }
        transform.rotation = Quaternion.Euler(m_startRot);
    }

    /// <summary>
    /// Returns a random float between -_absoluteMax and +_absoluteMax
    /// </summary>
    /// <param name="_absoluteMax">Maximum absolute value</param>
    private float RandomPositiveNegative(float _absoluteMax)
    {
        return Random.Range(-_absoluteMax, _absoluteMax);
    }

    /// <summary>
    /// The tree got destroyed and is now falling down / or collapsing
    /// </summary>
    private IEnumerator Timber()
    {
        m_soundEmitter_timber.Play();
        GetComponent<MeshCollider>().enabled = false;
        GetComponent<CapsuleCollider>().enabled = true;
        gameObject.AddComponent<Rigidbody>().AddTorque(Vector3.forward * 5);
        yield return new WaitForSeconds(5);
        Vector3[] logPositions = TreeLogPositions(m_bottomPos.position, m_topPos.position, GetLogAmount());
        for (int i = 0; i < GetLogAmount(); i++)
        {
            Instantiate(m_logPrefab, logPositions[i], Quaternion.identity);
        }

        // Give non wood items to the player inventory
        foreach (Item plantDestroyItem in m_plantDestroyItems)
        {
            if (plantDestroyItem.ItemID != (int)ERESOURCETYPE.WOOD)
            {
                InventoryManager.Instance.AddItem(plantDestroyItem.ItemID, plantDestroyItem.Amount);
            }
        }

        Destroy(this.gameObject);
    }

    private int GetLogAmount()
    {
        int retVal = 0;

        foreach (Item plantResource in m_plantDestroyItems)
        {
            if ((ERESOURCETYPE)plantResource.ItemID == ERESOURCETYPE.WOOD)
            {
                retVal += plantResource.Amount;
            }
        }

        return retVal;
    }

    private void ShowBerriesOnModel(bool _inFlowerBed = false)
    {
        if (m_plantType == PlantType.BUSH && m_regenLootItems && m_berries.Count > 0)
        {
            for (int i = 0; i < m_ownItemScript.Item.Length; i++)
            {
                if (m_ownItemScript.Item[i].ItemID == (int)ERESOURCETYPE.BERRY)
                {
                    float percentageBerries = m_ownItemScript.Item[i].Amount / (float)m_maxLootItems[i];
                    for (int j = 0; j < m_berries.Count * percentageBerries; j++)
                    {
                        m_berries[j].gameObject.SetActive(true);
                    }

                    for (int j = Mathf.CeilToInt(m_berries.Count * percentageBerries); j < m_berries.Count; j++)
                    {
                        m_berries[j].gameObject.SetActive(false);
                    }
                }
            }
        }
    }

    public bool InFlowerBed
    {
        get => m_inFlowerBed;
        set
        {
            m_inFlowerBed = value;
            if (value)
            {
                foreach (Transform berry in m_berries)
                {
                    if (berry.TryGetComponent(out MeshRenderer meshRenderer))
                    {
                        meshRenderer.sharedMaterial = m_flowerBedBerryMaterial;
                    }
                }
            }
            else
            {
                foreach (Transform berry in m_berries)
                {
                    if (berry.TryGetComponent(out MeshRenderer meshRenderer))
                    {
                        meshRenderer.sharedMaterial = m_normalBerryMaterial;
                    }
                }
            }
        }
    }
    #endregion
}

public class NoFoodPlantException : Exception
{
    public NoFoodPlantException(string _errorMessage) : base(_errorMessage) { }

    public NoFoodPlantException(string _errorMessage, Exception _innerException) : base(_errorMessage, _innerException) { }
}