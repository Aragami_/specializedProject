using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pond : MonoBehaviour, IDrinkable
{
    private float m_currentWater;
    [SerializeField, Tooltip("How much water is regenerated per second")] private float m_waterRegenPerSecond = 1f;
    [SerializeField, Tooltip("Maximum amount of water this pond can hold")] private float m_maxWater = 100f;

    /// <summary>
    /// Generates water
    /// </summary>
    private void GenerateWater()
    {
        if (m_currentWater <= m_maxWater)
            m_currentWater += m_waterRegenPerSecond * Time.deltaTime;
    }

    /// <summary>
    /// Reduces the ponds water by drinking amount
    /// </summary>
    /// <param name="_drinkAmount">Amount the drinker wants to drink</param>
    /// <returns>Amount actually drunk (may be less due to not having enough water)</returns>
    public int Drink(int _drinkAmount)
    {
        if (_drinkAmount > m_currentWater)
            _drinkAmount = (int)m_currentWater;
        m_currentWater -= _drinkAmount;
        return _drinkAmount;
    }

    /// <summary>
    /// Returns the amount of water this pond can supply right now
    /// </summary>
    public int GetDrinkCount => (int)m_currentWater;

    private void Update()
    {
        GenerateWater();
    }

    private void OnEnable()
    {
        // Add to list of water sources in the game
        FloraManager.Instance?.AddDrinkable(this);
    }

    private void OnDisable()
    {
        // Remove from list of water sources in the game
        FloraManager.Instance?.RemoveDrinkable(this);
    }
}
