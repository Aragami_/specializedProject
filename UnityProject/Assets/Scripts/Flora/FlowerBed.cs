using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FlowerBedSlot
{
    [SerializeField] private Transform m_hatchPosition;
    [HideInInspector]
    public Plant HatchingPlant = null;

    public Vector3 GetPosition => m_hatchPosition.position;
}

[RequireComponent(typeof(Building))]
public class FlowerBed : MonoBehaviour
{
    [SerializeField] private List<FlowerBedSlot> m_growingSpots = new List<FlowerBedSlot>();

    public Vector3 NextHatchSpot
    {
        get
        {
            if (m_growingSpots.Count > 0)
            {
                return m_growingSpots[Random.Range(0, m_growingSpots.Count)].GetPosition;
            }
            return Vector3.zero;
        }
    }

    public bool HasEmptyHatchSlot
    {
        get
        {
            foreach (FlowerBedSlot bedSlot in m_growingSpots)
            {
                if (bedSlot.HatchingPlant == null)
                    return true;
            }

            return false;
        }
    }

    public bool AddPlant(Plant _newPlant)
    {
        foreach (FlowerBedSlot bedSlot in m_growingSpots)
        {
            if (bedSlot.HatchingPlant == null)
            {
                bedSlot.HatchingPlant = _newPlant;
                return true;
            }
        }

        return false;
    }

    private void FindFlowerBedPlant()
    {
        foreach (FlowerBedSlot bed in m_growingSpots)
        {
            Collider[] plantColliders = Physics.OverlapSphere(bed.GetPosition, 0.1f, LayerMask.GetMask("Plant"));
            // Only store Plants
            List<Plant> plantList = new List<Plant>();
            foreach (Collider plantCol in plantColliders)
            {
                if (plantCol.GetType() == typeof(MeshCollider) && plantCol.TryGetComponent(out Plant foundPlant) && /*foundPlant.m_plantType == PlantType.BUSH && */foundPlant.m_regenLootItems)
                {
                    plantList.Add(foundPlant);
                }
            }
            // Find the closest Plant
            if (plantList.Count > 0)
            {
                Plant closestPlant = plantList[0];
                float closestFlowerBedSqrM = (closestPlant.transform.position - this.transform.position).sqrMagnitude;
                for (int i = 1; i < plantList.Count; i++)
                {
                    float nextSqrM = (plantList[i].transform.position - this.transform.position).sqrMagnitude;
                    if (nextSqrM < closestFlowerBedSqrM)
                    {
                        closestPlant = plantList[i];
                        closestFlowerBedSqrM = nextSqrM;
                    }
                }

                closestPlant.InFlowerBed = true;
                bed.HatchingPlant = closestPlant;
            }
        }
    }

    private void OnEnable()
    {
        FindFlowerBedPlant();
    }
}
