using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ItemScript), typeof(FMODUnity.StudioEventEmitter))]
public class OnDestroyInventoryItem : MonoBehaviour, IDamageable
{
    private bool m_isDestroyed = false;
    [SerializeField] private float m_hp = 5;
    private FMODUnity.StudioEventEmitter m_soundEmitter_wiggle;
    private ItemScript m_ownItems;
    private Vector3 m_startRot;

    private void Awake()
    {
        m_soundEmitter_wiggle = GetComponent<FMODUnity.StudioEventEmitter>();
        m_ownItems = GetComponent<ItemScript>();
        m_startRot = transform.eulerAngles;
    }

    /// <summary>
    /// Damages thus
    /// </summary>
    /// <param name="_damageAmount">Amount of damage</param>
    /// <returns>True if HP is 0 or below after damage taken, else False</returns>
    public bool Damage(float _damageAmount = 1)
    {
        if (!m_isDestroyed)
        {
            m_hp -= _damageAmount;
            if (m_hp <= 0)
            {
                m_isDestroyed = true;

                foreach (Item destroyItem in m_ownItems.Item)
                {
                    InventoryManager.Instance.AddItem(destroyItem.ItemID, destroyItem.Amount);
                }
                Destroy(this.gameObject);

                return true;
            }
            m_soundEmitter_wiggle?.Play();
            StartCoroutine(WiggleAnimation(0.1f, 3, 5));
            return false;
        }

        return true;
    }

    private IEnumerator WiggleAnimation(float _timeOfEachWiggle, int _amountOfWiggles, float _wiggleDegree)
    {
        for (int i = 0; i < _amountOfWiggles; i++)
        {
            if (!m_isDestroyed) // Don't wiggle when the tree is already destroyed
            {
                transform.rotation = Quaternion.Euler(m_startRot.x + RandomPositiveNegative(_wiggleDegree), m_startRot.y, m_startRot.z + RandomPositiveNegative(_wiggleDegree));
                yield return new WaitForSeconds(_timeOfEachWiggle);
            }
            else
            {
                yield break;
            }
        }
        transform.rotation = Quaternion.Euler(m_startRot);
    }

    /// <summary>
    /// Returns a random float between -_absoluteMax and +_absoluteMax
    /// </summary>
    /// <param name="_absoluteMax">Maximum absolute value</param>
    private float RandomPositiveNegative(float _absoluteMax)
    {
        return Random.Range(-_absoluteMax, _absoluteMax);
    }
}
