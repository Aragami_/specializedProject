using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "FirstTimeValuesSO", menuName = "FirstTimeValuesSo/FirstTimeValuesSo")]
public class FirstTimeValuesSO : ScriptableObject
{
    [SerializeField]
    [Tooltip("This will be edited in the Editor. Important is that it has to have the same length as the m_boolForFTV. This will be added to a dictionary.")]
    private EFirstTimeEvent[] m_eFirstTimeEvent;

    [SerializeField]
    [Tooltip("This will be edited in the Editor. Important is that it has to have the same length as the m_eFirstTimeValues. This will be added to a dictionary.")]
    private bool[] m_boolForFTV;

    /// <summary>
    /// This is a dictionary, which combines the m_eFirstTimeValues and m_boolForFTV arrays. Also the dictionary will not be saved from the SO, thats why there are arrays. 
    /// The dictionary is only there to make it easier to use, because otherwise there need to be two arrays always to be changed.
    /// </summary>
    private Dictionary<EFirstTimeEvent, bool> m_firstTimeValues = new Dictionary<EFirstTimeEvent, bool>();

    /// <summary>
    /// This is a dictionary, which combines the m_eFirstTimeEvent and m_boolForFTV arrays. Also the dictionary will not be saved from the SO, thats why there are arrays. 
    /// The dictionary is only there to make it easier to use, because otherwise there need to be two arrays always to be changed.
    /// </summary>
    public Dictionary<EFirstTimeEvent, bool> MyFirstTimeValues
    {
        get => m_firstTimeValues;
    }

    public EFirstTimeEvent[] MyEFirstTimeValues
    {
        get => m_eFirstTimeEvent;
    }

    public bool[] BoolForFTV
    {
        get => m_boolForFTV;
    }
}
