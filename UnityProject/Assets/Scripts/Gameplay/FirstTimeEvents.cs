using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstTimeEvents : MonoBehaviour
{
    [SerializeField]
    [Tooltip("The first time event this object has.")]
    private EFirstTimeEvent m_myFirstTimeEvent = EFirstTimeEvent.None;

    [SerializeField]
    [Tooltip("How long the message will stay.")]
    private float m_messageTime = 0.0f;

    [SerializeField]
    [TextArea]
    [Tooltip("The event message.")]
    private string m_eventMessage = null;

    private void OnTriggerEnter(Collider _other)
    {
        if (_other.gameObject.CompareTag("Player"))
        {
            if (GameManager.Instance.MyFTSSO.MyFirstTimeValues.ContainsKey(m_myFirstTimeEvent))
            {
                if (GameManager.Instance.MyFTSSO.MyFirstTimeValues.TryGetValue(m_myFirstTimeEvent, out bool value))
                {
                    if (value)
                    {
                        GameManager.Instance.EventMessage(m_eventMessage, m_messageTime);
                        GameManager.Instance.MyFTSSO.MyFirstTimeValues[m_myFirstTimeEvent] = false;
                    }
                }
            }
            this.gameObject.SetActive(false);
        }
    }
}
