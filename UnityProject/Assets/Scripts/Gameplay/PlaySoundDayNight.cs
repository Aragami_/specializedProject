using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundDayNight : MonoBehaviour
{
    [Tooltip("The sound emitter.")]
    private FMODUnity.StudioEventEmitter m_emitter = null;

    private void Awake()
    {
        m_emitter = GetComponent<FMODUnity.StudioEventEmitter>();
    }

    // Update is called once per frame
    void Update()
    {
        // Depending on the day time the sound will be played.
        if (GameManager.Instance.DayTime)
        {
            // Here the time is day and will start the emitter if it isn't playing.
            if (!m_emitter.IsPlaying())
            {
                m_emitter.Play();
            }
        }
        else
        {
            // Here the time is night and will stop the emitter if it is playing.
            if (m_emitter.IsPlaying())
            {
                m_emitter.Stop();
            }
        }
    }
}
