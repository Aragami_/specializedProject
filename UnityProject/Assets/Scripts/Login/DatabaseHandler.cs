using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;



//IMPORTANT! This script has to be server sides, shouldn't be inplemented in gamedata! In official Release this should be outsourced to server!
public class DatabaseHandler
{

    #region Variables

    [Tooltip("Sql Connection")]
    private MySqlConnection m_connection;

    [Tooltip("Login data was valid?")]
    private bool m_loggedIn = false;

    [Tooltip("deposit Userid on valid login")]
    private int m_userID = 0;

    [Tooltip("Inventory class filled on valid login and load")]
    private Inventory m_inventory;

    [Tooltip("Array with all 16 Slots of Inventory")]
    private int[] m_slotID = new int[16];

    #endregion

    #region Properties

    public bool LoggedIn { get => m_loggedIn; private set => m_loggedIn = value; }

    public int UserID { get => m_userID; private set => m_userID = value; }
    public int[] SlotID { get => m_slotID; set => m_slotID = value; }

    #endregion

    #region ctor

    /// <summary>
    /// ctor for DatabaseHandler - opens connection to Database if possible
    /// </summary>
    public DatabaseHandler()
    {
        m_connection = new MySqlConnection("server=survivalgame.ddns.net;user=ingameadmin;database=sae_specializedproject;port=3306;password=/vkLt9m4j@TK*wfm");
        //m_connection = new MySqlConnection("server=localhost;database=sae_specializedproject;user=ingameadmin;port=3306;password=/vkLt9m4j@TK*wfm");
        //Debug.Log(m_connection.ConnectionString);
        m_inventory = new Inventory();
        try
        {
            m_connection.Open();
        }
        catch (Exception cEx)
        {
            Debug.Log(cEx.ToString());
        }
    }





    /// <summary>
    /// Destructor to Close connection to Database, has to be called at the on of the game
    /// </summary>
    ~DatabaseHandler()
    {
        m_connection.Close();
    }

    #endregion

    #region Public functions

    /// <summary>
    /// Log in User called on UI button Event
    /// </summary>
    /// <param name="_username">username saved on database</param>
    /// <param name="_password">chosen crypted password at register page</param>
    public void LoginUser(string _username, string _password)
    {
        string sql = "SELECT * FROM player WHERE username='" + _username + "';";

        MySqlCommand cmd = new MySqlCommand(sql, m_connection);
        try
        {
            // get stored password and compare to given password
            MySqlDataReader sqlReader = cmd.ExecuteReader();
            while (sqlReader.Read())
            {
                if (sqlReader.GetString(2) == HashPW(_password))
                {
                    UIManager.Instance.DisableLoginUI();

                    m_loggedIn = true;

                    // set userID - equals inventoryID
                    m_userID = sqlReader.GetInt32(0);
                }
            }
            sqlReader.Close();
            if (m_loggedIn)
            {
                // slotID,itemID,amount,inventoryID
                LoadItemList();
                InventoryManager.Instance.LoadInventory();
                SavegameManager.Instance.LoadplayerStats();
                LoadResources();
                LoadBuildings();

            }
        }
        catch (Exception _e)
        {
            Debug.LogWarning($"MySQL login failed! Error: {_e}.");
        }


    }

    /// <summary>
    /// Save world's buildings to database
    /// </summary>
    /// <param name="buildings">List of all Buildings placed in scene</param>
    public void SaveBuildings(List<Building> _buildings)
    {
        if (_buildings.Count == 0)
            return;

        if (m_loggedIn && m_userID != 0)
        {
            // fill sql string and send command to database
            try
            {
                //delete old data to update to new data
                string sql = $"DELETE FROM buildings WHERE playerID={m_userID};";
                MySqlCommand cmd = new MySqlCommand(sql, m_connection);
                cmd.ExecuteNonQuery();

                //update to new data
                for (int i = 0; i < _buildings.Count; i++)
                {
                    cmd = new MySqlCommand();

                    cmd.Connection = m_connection;
                    cmd.CommandText = "INSERT INTO buildings(playerID,x,y,z,rotx,roty,rotz,rotw,type) VALUES(@playerID,@x,@y,@z,@rotx,@roty,@rotz,@rotw,@type)";

                    cmd.Parameters.AddWithValue("@playerID", m_userID);
                    cmd.Parameters.AddWithValue("@x", _buildings[i].transform.position.x);
                    cmd.Parameters.AddWithValue("@y", _buildings[i].transform.position.y);
                    cmd.Parameters.AddWithValue("@z", _buildings[i].transform.position.z);
                    cmd.Parameters.AddWithValue("@rotx", _buildings[i].transform.rotation.x);
                    cmd.Parameters.AddWithValue("@roty", _buildings[i].transform.rotation.y);
                    cmd.Parameters.AddWithValue("@rotz", _buildings[i].transform.rotation.z);
                    cmd.Parameters.AddWithValue("@rotw", _buildings[i].transform.rotation.w);
                    cmd.Parameters.AddWithValue("@type", _buildings[i].MyType);

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception _e)
            {
                Debug.Log("DBERROR; Can't save building values! Error: " + _e);
            }
        }
    }

    /// <summary>
    /// load saved objects from last session from database and instantiate them
    /// </summary>
    public void LoadBuildings()
    {
        //List<GameObject> buildings = new List<GameObject>();

        if (m_loggedIn && m_userID != 0)
        {
            GameObject _go = new GameObject("BuildingCollection");
            string sql = $"SELECT * FROM buildings WHERE playerID={m_userID};";

            // get row values
            MySqlCommand cmd = new MySqlCommand(sql, m_connection);
            try
            {
                MySqlDataReader sqlReader = cmd.ExecuteReader();
                while (sqlReader.Read())
                {
                    //store temp values
                    int type = sqlReader.GetInt32(9);
                    float x = sqlReader.GetFloat(2);
                    float y = sqlReader.GetFloat(3);
                    float z = sqlReader.GetFloat(4);
                    float rotx = sqlReader.GetFloat(5);
                    float roty = sqlReader.GetFloat(6);
                    float rotz = sqlReader.GetFloat(7);
                    float rotw = sqlReader.GetFloat(8);

                    Quaternion rot = new Quaternion(rotx, roty, rotz, rotw);
                    Vector3 pos = new Vector3(x, y, z);

                    //instantiate loaded objects to scene
                    GameObject _tmp = GameObject.Instantiate(SavegameManager.Instance.Buildings[type], pos, rot);
                }
                sqlReader.Close();
            }
            catch (Exception _e)
            {
                Debug.LogWarning($"MySQL inventory load failed! Error: {_e}.");
            }
        }
    }

    /// <summary>
    /// loads inventory from database into inventory class
    /// </summary>
    /// <returns>Inventory class</returns>
    public Inventory LoadInventory()
    {
        if (m_loggedIn && m_userID != 0)
        {
            string sql = "SELECT * FROM inventoryslots WHERE inventoryID='" + m_userID + "';";

            MySqlCommand cmd = new MySqlCommand(sql, m_connection);
            try
            {
                MySqlDataReader sqlReader = cmd.ExecuteReader();
                int i = 0;
                m_inventory.InventoryID = m_userID;
                while (sqlReader.Read())
                {
                    if (i < InventoryManager.Instance.MaxSlots)
                    {

                        if (m_inventory.InventoryID == 0)
                            m_inventory.InventoryID = (int)sqlReader.GetValue(3);
                        Item item = new Item();
                        item = GameManager.Instance.ItemList.GetItem((int)sqlReader.GetValue(1));
                        m_inventory.Items[i] = new Item(item);
                        m_slotID[i] = (int)sqlReader.GetValue(0);
                        m_inventory.Items[i].Amount = (int)sqlReader.GetValue(2);
                    }
                    i++;
                }
                sqlReader.Close();
                m_inventory.SlotID = m_slotID;
            }
            catch (Exception _e)
            {
                Debug.LogWarning($"MySQL inventory load failed! Error: {_e}.");
            }
        }
        return m_inventory;
    }

    /// <summary>
    /// Saves inventorydata on Database
    /// </summary>
    public void SaveInventory()
    {
        Inventory inventory = InventoryManager.Instance.Inventory;
        if (m_loggedIn && m_userID != 0)
        {
            // fill sql string and send command to database
            try
            {
                for (int i = 0; i < inventory.Items.Length; i++)
                {
                    //TODO: edit this to inventoryslots record
                    string sql = $"UPDATE inventoryslots SET itemID={inventory.Items[i].ItemID}, amount={inventory.Items[i].Amount} WHERE slotID={inventory.SlotID[i]};";
                    MySqlCommand cmd = new MySqlCommand(sql, m_connection);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception _e)
            {
                GameManager.Instance.EventMessage("Cant save inventory!");
                Debug.Log("DBERROR; Can't save Inventory values! Error: " + _e);
            }
        }
    }

    /// <summary>
    /// Send current gamesession resources to deposit in database
    /// </summary>
    /// <param name="_resources">Resource collection</param>
    public void SaveResources(ItemScript[] _resources)
    {
        if (m_loggedIn && m_userID != 0)
        {
            // fill sql string and send command to database
            try
            {
                //delete old data to update to new data
                string sql = $"DELETE FROM resources WHERE playerID={m_userID};";
                MySqlCommand cmd = new MySqlCommand(sql, m_connection);
                cmd.ExecuteNonQuery();

                //update to new data
                for (int i = 0; i < _resources.Length; i++)
                {
                    if (_resources[i].Type == PlantType.NULL)
                    {
                        continue;
                    }

                    Vector3 tmp = _resources[i].transform.position;
                    Quaternion tmpQuat = _resources[i].transform.rotation;
                    cmd = new MySqlCommand();

                    cmd.Connection = m_connection;
                    cmd.CommandText = "INSERT INTO resources(playerID,resourcetype,x,y,z,rotx,roty,rotz,rotw) VALUES(@playerID,@resourcetype,@x,@y,@z,@rotx,@roty,@rotz,@rotw)";

                    cmd.Parameters.AddWithValue("@playerID", m_userID);
                    cmd.Parameters.AddWithValue("@resourcetype", (int)_resources[i].Type);
                    cmd.Parameters.AddWithValue("@x", tmp.x);
                    cmd.Parameters.AddWithValue("@y", tmp.y);
                    cmd.Parameters.AddWithValue("@z", tmp.z);
                    cmd.Parameters.AddWithValue("@rotx", tmpQuat.x);
                    cmd.Parameters.AddWithValue("@roty", tmpQuat.y);
                    cmd.Parameters.AddWithValue("@rotz", tmpQuat.z);
                    cmd.Parameters.AddWithValue("@rotw", tmpQuat.w);

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception _e)
            {
                Debug.Log("DBERROR; Can't save resources! Error: " + _e);
            }
        }
    }

    /// <summary>
    /// Load resources from database
    /// </summary>
    public void LoadResources()
    {
        if (m_loggedIn && m_userID != 0)
        {
            //fill sql string and send command to database
            try
            {
                string sql = $"SELECT * FROM inventory WHERE playerID={m_userID};";
                MySqlCommand logincheck = new MySqlCommand(sql, m_connection);
                MySqlDataReader loginReader = logincheck.ExecuteReader();
                bool firstLogin = false;
                while (loginReader.Read())
                {
                    //check first login and activate resources else load resources
                    if (loginReader.GetInt32(2) == 1)
                    {
                        SavegameManager.Instance.ActivateRes();
                        GameManager.Instance.FirstSpawn();
                        firstLogin = true;
                    }
                }
                loginReader.Close();
                if (firstLogin)
                {
                    sql = $"UPDATE inventory SET firstlogin=0  WHERE playerID={m_userID};";
                    MySqlCommand firstlogindone = new MySqlCommand(sql, m_connection);
                    firstlogindone.ExecuteNonQuery();
                    return;
                }
                // read resource data from database and spawn remaining Objects from last session
                sql = $"SELECT * FROM resources WHERE playerID={m_userID};";
                MySqlCommand cmd = new MySqlCommand(sql, m_connection);
                MySqlDataReader sqlReader = cmd.ExecuteReader();
                while (sqlReader.Read())
                {
                    int plantType = sqlReader.GetInt32(2);
                    float x = (float)sqlReader.GetValue(3);
                    float y = (float)sqlReader.GetValue(4);
                    float z = (float)sqlReader.GetValue(5);
                    float rotx = (float)sqlReader.GetValue(6);
                    float roty = (float)sqlReader.GetValue(7);
                    float rotz = (float)sqlReader.GetValue(8);
                    float rotw = (float)sqlReader.GetValue(9);

                    Vector3 position = new Vector3(x, y, z);
                    Quaternion rotation = new Quaternion(rotx, roty, rotz, rotw);

                    if (SavegameManager.Instance.ResourcePrefabs[plantType] == null)
                    {
                        continue;
                    }

                    GameObject.Instantiate(SavegameManager.Instance.ResourcePrefabs[plantType], position, rotation);
                }
                sqlReader.Close();
            }
            catch (Exception _e)
            {
                Debug.Log("DBERROR; Can't load resources! Error: " + _e);
            }
        }
    }

    /// <summary>
    /// Saves current playerdata on database
    /// </summary>
    /// <param name="_trans">Vector position</param>
    /// <param name="_currentHealth">current health value</param>
    /// <param name="_currentHunger">current hunger value</param>
    /// <param name="_currentThirst">current thirst value</param>
    public void SavePlayer(Vector3 _trans, int _currentHealth, int _currentHunger, int _currentThirst)
    {

        if (m_loggedIn && m_userID != 0)
        {
            // set sql string and send information to database
            try
            {
                string sql = $"UPDATE playerstats SET x={(int)_trans.x}, y={(int)_trans.y + 1}, z={(int)_trans.z}," +
                    $" currenthealth={_currentHealth}, currenthunger={_currentHunger}, currentthirst={_currentThirst} WHERE playerID={m_userID};";
                MySqlCommand cmd = new MySqlCommand(sql, m_connection);
                cmd.ExecuteNonQuery();
            }
            catch (Exception _e)
            {
                GameManager.Instance.EventMessage("SaveCharacter failed!");
                Debug.Log("DBERROR; Can't save player values! Error: " + _e);
            }
        }
    }

    /// <summary>
    /// Get player data transform,health,hunger,thirst
    /// </summary>
    /// <returns>Playerstats class</returns>
    public PlayerStats LoadPlayer()
    {

        PlayerStats playerstats = new PlayerStats();
        if (m_loggedIn && m_userID != 0)
        {
            // read data record from database and fill playerstat class with given values
            string sql = "SELECT * FROM playerstats WHERE playerID='" + m_userID + "';";
            MySqlCommand cmd = new MySqlCommand(sql, m_connection);
            try
            {
                MySqlDataReader sqlReader = cmd.ExecuteReader();
                while (sqlReader.Read())
                {
                    playerstats.m_trans.x = sqlReader.GetInt32(2);
                    playerstats.m_trans.y = sqlReader.GetInt32(3);
                    playerstats.m_trans.z = sqlReader.GetInt32(4);
                    playerstats.m_currenthealth = sqlReader.GetInt32(5);
                    playerstats.m_currenthunger = sqlReader.GetInt32(6);
                    playerstats.m_currentthirst = sqlReader.GetInt32(7);
                }
                sqlReader.Close();
            }
            catch (Exception _e)
            {
                Debug.LogWarning($"DBERROR; Can't load playerstats! Error: {_e}.");
            }
        }

        return playerstats;
    }

    /// <summary>
    /// clear inventory on database
    /// </summary>
    public void ResetInventory()
    {
        // set sql string to reset inventory on database
        Inventory inventory = InventoryManager.Instance.Inventory;
        if (m_loggedIn && m_userID != 0)
        {
            for (int i = 0; i < inventory.Items.Length; i++)
            {
                string sql = "UPDATE inventoryslots SET itemID=0, amount=0 WHERE slotID=" + inventory.SlotID[i] + ";";
                MySqlCommand cmd = new MySqlCommand(sql, m_connection);
                cmd.ExecuteNonQuery();
            }

            //update inventory and inventory UI
            LoadInventory();
            InventoryManager.Instance.UpdateInventoryUI();
        }
    }

    /// <summary>
    /// Load items and update itemlist 
    /// </summary>
    public void LoadItemList()
    {

        if (m_loggedIn && m_userID != 0)
        {
            Item item = new Item(1, "Wood", "Wood halt");
            // read data record from database and fill playerstat class with given values
            string sql = "SELECT * FROM items";
            MySqlCommand cmd = new MySqlCommand(sql, m_connection);
            try
            {
                MySqlDataReader sqlReader = cmd.ExecuteReader();
                while (sqlReader.Read())
                {
                    int _id = sqlReader.GetInt32(0);
                    string _name = sqlReader.GetString(1);
                    string _desc = sqlReader.GetString(2);
                    int? _hungerAmount = null;
                    int? _thirstAmount = null;
                    int? _durability = null;
                    int? _attack = null;
                    if (!sqlReader.IsDBNull(3))
                    {
                        _hungerAmount = sqlReader.GetInt32(3);
                    }
                    if (!sqlReader.IsDBNull(4))
                    {
                        _thirstAmount = sqlReader.GetInt32(4);
                    }
                    if (!sqlReader.IsDBNull(5))
                    {
                        _durability = sqlReader.GetInt32(5);
                    }
                    if (!sqlReader.IsDBNull(6))
                    {
                        _attack = sqlReader.GetInt32(6);
                    }

                    GameManager.Instance.ItemList.AddItem(new Item(
                        _id, _name, _desc, _hungerAmount, _thirstAmount, _durability, _attack));
                }
                sqlReader.Close();
            }
            catch (Exception _e)
            {
                Debug.LogWarning($"DBERROR; Can't load itemlist! Error: {_e}.");
            }
        }
    }

    #endregion

    #region Private functions

    /// <summary>
    /// hashes the password set by Loginmask, compares with database stored password
    /// </summary>
    /// <param name="_password">clear password, set in Loginmask</param>
    /// <returns></returns>
    private string HashPW(string _password)
    {
        byte[] abBytes = ASCIIEncoding.ASCII.GetBytes(_password);
        byte[] abHashedBytes = MD5CryptoServiceProvider.Create().ComputeHash(abBytes);

        string hash = BitConverter.ToString(abHashedBytes).Replace("-", "").ToLower();

        return hash;
    }

    #region ----- obsolete -----

    /// <summary>
    /// Reads item data from database
    /// </summary>
    /// <param name="_type">ENEWITEM</param>
    /// <param name="_id">int</param>
    /// <param name="_go">GameObject</param>
    /// <returns>NewItem class</returns>
    //public NewItem GetItemInfo(ENEWITEM _type, int _id, GameObject _go = null)
    //{
    //    if (_go == null)
    //    {
    //        m_go = new GameObject();
    //        m_go.AddComponent<ItemScript>().Item = new NewItem[1];
    //    }
    //    else
    //    {
    //        m_go = _go;
    //    }

    //    m_go.GetComponent<ItemScript>().Item[0] = new NewItem(_type, _id, 0);
    //    NewItem item = m_go.GetComponent<ItemScript>().Item[0];
    //    string sql;

    //    if (m_loggedIn)
    //    {

    //        switch (_type)
    //        {
    //            case ENEWITEM.NONE:
    //                return null;

    //            case ENEWITEM.RESOURCE:
    //                {
    //                    sql = $"SELECT * FROM resources WHERE resourceID={_id}";
    //                    MySqlCommand cmd = new MySqlCommand(sql, m_connection);
    //                    try
    //                    {
    //                        MySqlDataReader sqlReader = cmd.ExecuteReader();
    //                        while (sqlReader.Read())
    //                        {
    //                            item.ItemName = sqlReader.GetString(1);
    //                            item.Description = sqlReader.GetString(2);
    //                            item.Icon = Resources.Load<Sprite>($"Sprites/Items/Icons/{item.ItemName}");
    //                        }
    //                        sqlReader.Close();
    //                    }
    //                    catch (Exception _e)
    //                    {
    //                        Debug.LogWarning($"Getting item information failed! Error: {_e}.");
    //                    }
    //                }
    //                break;

    //            case ENEWITEM.TOOL:
    //                {
    //                    sql = $"SELECT * FROM tools WHERE toolID={_id}";
    //                    MySqlCommand cmd = new MySqlCommand(sql, m_connection);
    //                    try
    //                    {
    //                        MySqlDataReader sqlReader = cmd.ExecuteReader();
    //                        while (sqlReader.Read())
    //                        {
    //                            item.ItemName = sqlReader.GetString(1);
    //                            item.Description = sqlReader.GetString(2);
    //                            item.Durability = sqlReader.GetInt32(3);
    //                            item.Damage = sqlReader.GetInt32(4);
    //                            item.Icon = Resources.Load<Sprite>($"Sprites/Items/Icons/{item.ItemName}");
    //                        }
    //                        sqlReader.Close();
    //                    }
    //                    catch (Exception _e)
    //                    {
    //                        Debug.LogWarning($"Getting item information failed! Error: {_e}.");
    //                    }
    //                }
    //                break;

    //            case ENEWITEM.CONSUMABLE:
    //                {
    //                    sql = $"SELECT * FROM consumables WHERE consumableID={_id}";
    //                    MySqlCommand cmd = new MySqlCommand(sql, m_connection);
    //                    try
    //                    {
    //                        MySqlDataReader sqlReader = cmd.ExecuteReader();
    //                        while (sqlReader.Read())
    //                        {
    //                            item.ItemName = sqlReader.GetString(1);
    //                            item.Description = sqlReader.GetString(2);
    //                            item.HungerBenefit = sqlReader.GetInt32(3);
    //                            item.Icon = Resources.Load<Sprite>($"Sprites/Items/Icons/{item.ItemName}");
    //                        }
    //                        sqlReader.Close();
    //                    }
    //                    catch (Exception _e)
    //                    {
    //                        Debug.LogWarning($"Getting item information failed! Error: {_e}.");
    //                    }
    //                }
    //                break;

    //            default:
    //                break;
    //        }
    //    }
    //    GameManager.DestroyOnNextUpdate(m_go);
    //    return item;
    //}

    #endregion


    #endregion
}
