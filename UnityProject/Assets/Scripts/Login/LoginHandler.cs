using UnityEngine;
using TMPro;

public class LoginHandler : MonoBehaviour
{
    #region Variables

    [Header("Inputfields for Logindata")]
    [Tooltip("Inputfield for Username")]
    [SerializeField]
    private TMP_InputField m_tbxUsername;

    [Tooltip("Inputfield for Password")]
    [SerializeField]
    private TMP_InputField m_tbxPassword;

    #endregion

    #region Properties

    public static LoginHandler Instance { get; private set; }

    #endregion

    private void Awake()
    {
        if (Instance)
        {
            Destroy(this);
            return;
        }

        Instance = this;
    }

    private void Start()
    {
        m_tbxUsername.Select();
    }

    /// <summary>
    /// Button Event for Loginmask - commits datas to Databasehandler for validation
    /// </summary>
    public void OnClick_Login()
    {
        GameManager.Instance.DbHandler.LoginUser(m_tbxUsername.text, m_tbxPassword.text);
    }

    /// <summary>
    /// Login function called via code not via button
    /// </summary>
    /// <param name="_username"></param>
    /// <param name="_password"></param>
    public void OnClick_Login(TMP_InputField _username, TMP_InputField _password)
    {
        GameManager.Instance.DbHandler.LoginUser(_username.text, _password.text);
    }
}
