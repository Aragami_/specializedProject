using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsumableSource : MonoBehaviour, ILootable
{
    [SerializeField]
    [Tooltip("The item to add.")]
    private ERESOURCETYPE m_item = ERESOURCETYPE.NONE;

    [SerializeField]
    [Tooltip("The amount to get.")]
    private int m_amount = 0;

    public void Loot()
    {
        InventoryManager.Instance.AddItem(m_item, m_amount);
    }
}
