using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Plant))]
public class EditorPlant : Editor
{
    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        Plant myPlant = target as Plant;

        DrawPropertiesExcluding(serializedObject, "m_plantMaxHp", "m_plantType", "m_logPrefab", "m_timberTime", "m_amountOfLogs", "m_bottomPos", "m_topPos", "m_regenLootItems", "m_fullRegenTime", "m_growthMultiplier", "m_shouldGrow", "m_growSpeed", "m_growTime", "m_foodGrowType", "m_foodGrowSpeed", "m_maxFood");

        // Parameters
        EditorGUILayout.LabelField("");
        EditorGUILayout.LabelField("Parameters", EditorStyles.boldLabel);
        myPlant.m_plantMaxHp = EditorGUILayout.IntField(new GUIContent("Plant HP:", "The amount of HP the plant have (needed for chopping)"), myPlant.m_plantMaxHp);
        myPlant.m_plantType = (PlantType)EditorGUILayout.EnumPopup(new GUIContent("Plant Type:", "The type of plant"), myPlant.m_plantType);
        // No disabled group, instead show/hide elements
        switch (myPlant.m_plantType)
        {
            case PlantType.TREE:
                myPlant.m_logPrefab = EditorGUILayout.ObjectField("Log prefab:", myPlant.m_logPrefab, typeof(GameObject), true) as GameObject;
                myPlant.m_timberTime = EditorGUILayout.FloatField(new GUIContent("Timber Time:", "The time in seconds after the tree is chopped and falling until it disappears"), myPlant.m_timberTime);
                myPlant.m_bottomPos = EditorGUILayout.ObjectField("Bottom position of tree:", myPlant.m_bottomPos, typeof(Transform), true) as Transform;
                myPlant.m_topPos = EditorGUILayout.ObjectField("Top position of tree:", myPlant.m_topPos, typeof(Transform), true) as Transform;
                EditorGUILayout.LabelField("");
                break;
            case PlantType.BUSH:
                myPlant.m_regenLootItems = EditorGUILayout.Toggle("Regenerate Loot Items:", myPlant.m_regenLootItems);
                if (myPlant.m_regenLootItems)
                {
                    myPlant.m_fullRegenTime =
                        (int)EditorGUILayout.Slider("Time for full regen:", myPlant.m_fullRegenTime, 1, 300);
                    myPlant.m_normalBerryMaterial = EditorGUILayout.ObjectField("Normal Berry Material:",
                        myPlant.m_normalBerryMaterial, typeof(Material), false) as Material;
                    myPlant.m_flowerBedBerryMaterial = EditorGUILayout.ObjectField("FlowerBed Berry Material:",
                        myPlant.m_flowerBedBerryMaterial, typeof(Material), false) as Material;
                    myPlant.m_growthMultiplier = EditorGUILayout.Slider("Flowerbed growth multipler:",
                        myPlant.m_growthMultiplier, 1f, 20f);
                }
                else
                {
                    EditorGUILayout.LabelField("");
                    EditorGUILayout.LabelField("");
                    EditorGUILayout.LabelField("");
                    EditorGUILayout.LabelField("");
                }
                break;
            default:
                EditorGUILayout.LabelField("");
                EditorGUILayout.LabelField("");
                EditorGUILayout.LabelField("");
                EditorGUILayout.LabelField("");
                EditorGUILayout.LabelField("");
                break;
        }

        // Growth
        EditorGUILayout.LabelField("");
        EditorGUILayout.LabelField("Growth", EditorStyles.boldLabel);
        myPlant.m_shouldGrow = EditorGUILayout.Toggle(new GUIContent("Should Grow:", "Whether this plant starts fully grown or should grow on Awake/Spawn"), myPlant.m_shouldGrow);
        EditorGUI.BeginDisabledGroup(!myPlant.m_shouldGrow);
        {
            myPlant.m_growSpeed = EditorGUILayout.CurveField(
                new GUIContent("Grow Speed:", "The speed of the growth from Awake/ Spawn to fully grown"),
                myPlant.m_growSpeed);
            myPlant.m_growTime = EditorGUILayout.FloatField(new GUIContent("Grow Time:", "The time in seconds the plant takes to be fully grown"), myPlant.m_growTime);
        }
        EditorGUI.EndDisabledGroup();

        // Food
        EditorGUILayout.LabelField("");
        EditorGUILayout.LabelField("Food", EditorStyles.boldLabel);
        myPlant.m_foodGrowType = (FoodGrowType)EditorGUILayout.EnumPopup(new GUIContent("Food Grow Type:", "How food grows:\n -NONE: Plant doesn't have food\n -INSTANT: Food grows instantly after food grow timer is done\n -LINEAR: Food grows gradually over time"), myPlant.m_foodGrowType);
        EditorGUI.BeginDisabledGroup(!myPlant.IsEdible);
        {
            myPlant.m_foodGrowSpeed = EditorGUILayout.FloatField(new GUIContent("Food Grow Speed:", "Grow speed per second"), myPlant.m_foodGrowSpeed);
            myPlant.m_maxFood = EditorGUILayout.IntField(new GUIContent("Max Food:", "The maximum amount of stored food"), myPlant.m_maxFood);
        }
        EditorGUI.EndDisabledGroup();
        if (GUI.changed)
        {
            // writing changes of the testScriptable into Undo
            Undo.RecordObject(myPlant, "Test Scriptable Editor Modify");
            // mark the testScriptable object as "dirty" and save it
            EditorUtility.SetDirty(myPlant);
        }
        serializedObject.ApplyModifiedProperties();
    }
}
