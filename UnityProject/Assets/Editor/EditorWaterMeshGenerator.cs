using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(WaterMeshGenerator))]
public class EditorWaterMeshGenerator : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Generate new Mesh"))
        {
            WaterMeshGenerator myGenerator = target as WaterMeshGenerator;
            myGenerator?.GenerateMesh();
        }
    }
}